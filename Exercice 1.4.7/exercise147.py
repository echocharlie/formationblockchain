# -*- coding: utf8 -*-


"""

    Exercice 1.4.7 : Vérifier la validité d’une transaction Pay-to-pubkey-hash
    https://ecole.alyra.fr/mod/assign/view.php?id=70
    https://forum.alyra.fr/t/exercice-1-4-7-verifier-la-validite-d-une-transaction-pay-to-pubkey-hash/90

"""

from enum import Enum
from Crypto.Hash import SHA256, RIPEMD160


def hash160(number):
    sha256 = SHA256.new()
    ripemd = RIPEMD160.new()

    sha256.update(bytearray.fromhex(number))

    ripemd.update(bytes(sha256.digest()))
    return ripemd.hexdigest()

class OpCode(Enum):
    OP_DUP = 1
    OP_HASH160 = 2
    OP_EQUALVERIFY = 3
    OP_CHECKSIG = 4

def extract_script(script):
    stack = []
    # décodage du script
    tmp_script = script
    bytes_to_remove = 0
    while tmp_script != "":
        opcode = tmp_script[:2]
        if opcode == "76":
            # OP_DUP
            stack.append(OpCode.OP_DUP)
            bytes_to_remove = 1

        elif opcode == "a9":
            # OP_HASH160
            stack.append(OpCode.OP_HASH160)
            bytes_to_remove = 1
        elif opcode == "88":
            # OP_EQUALVERIFY
            stack.append(OpCode.OP_EQUALVERIFY)
            bytes_to_remove = 1
        elif opcode == "ac":
            # OP_CHECKSIG
            stack.append(OpCode.OP_CHECKSIG)
            bytes_to_remove = 1
        elif int(opcode, 16) >= 0x01 and int(opcode, 16) <= 0x4b:
            # opcode = put <opcode> bytes on top of the stack
            stack.append(tmp_script[2:int(opcode,16)*2 + 2])
            bytes_to_remove = int(opcode ,16) + 1

        tmp_script = tmp_script[bytes_to_remove * 2:]

    return stack

def runscript(stack):
    if len(stack) == 0: return None
    runstack = []
    for op in stack:
        if  op == OpCode.OP_DUP:
            runstack.append(runstack[-1])
        elif op == OpCode.OP_CHECKSIG:
            runstack.append(True)
        elif op == OpCode.OP_EQUALVERIFY:
            v1 = runstack.pop()
            v2 = runstack.pop()
            runstack.append(v1 == v2)
        elif op == OpCode.OP_HASH160:
            r = hash160(runstack[-1])
            runstack.append(r)
        else:
            runstack.append(op)

        #print(runstack)

    return runstack[-1]

def verificationP2PKH(ScriptSig, ScriptPubSig):

    stack = []
    if ScriptSig.startswith("0x"):
        ScriptSig = ScriptSig[2:]
    if ScriptPubSig.startswith("0x"):
        ScriptPubSig = ScriptPubSig[2:]

    stack += extract_script(ScriptSig)
    print(stack)
    stack += extract_script(ScriptPubSig)
    print(stack)

    r = runscript(stack)

    return True


if __name__ == '__main__':

    r = verificationP2PKH("0x483045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001210372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c","0x76a9147c3f2e0e3f3ec87981f9f2059537a355db03f9e888ac")
    print(r)
    r = verificationP2PKH("483045022100F3581E1972AE8AC7C7367A7A253BC1135223ADB9A468BB3A59233F45BC578380022059AF01CA17D00E41837A1D58E97AA31BAE584EDEC28D35BD96923690913BAE9A0141049C02BFC97EF236CE6D8FE5D94013C721E915982ACD2B12B65D9B7D59E20A842005F8FC4E02532E873D37B96F09D6D4511ADA8F14042F46614A4C70C0F14BEFF5","76A9141AA0CD1CBEA6E7458A7ABAD512A9D9EA1AFB225E88AC")
    print(r)
    """
    number = "0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c"
    expected_hash = "7c3f2e0e3f3ec87981f9f2059537a355db03f9e8"
    r = hash160(number)
    print(type(r),r)
    print("expected value :",expected_hash)
    """

    i = 0xb3
    j=2
    while i <= 0xb9:
        print("OP_NOP_"+str(j)+" = "+hex(i))
        i+=1
        j+=1