pragma solidity ^0.5.3;
pragma experimental ABIEncoderV2;

contract Cartes {

    string[] private cartes;

    function ajouterCarte(string memory nom) public {
        cartes.push(nom);
    }

    function recupererCarte(uint index) public view returns (string memory) {
        return cartes[index];
    }

    function getCartes() public view returns (string[] memory) {
        return cartes;
    }

}
