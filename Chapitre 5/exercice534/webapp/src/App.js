import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Cartes from './abis/Cartes.json'

import Web3 from 'web3';
const TITLE = 'Collection de cartes';

class CardList extends React.Component {
    render() {
        let self=this;
        let cartes = this.props.cards.map((aCarte, id) => {
        return (
            <div key={id} className="carte">
                {aCarte.id}.&nbsp;{aCarte}
            </div>
        );

        } );

        return (
            <div className="cartesList">
                <h2>Cartes</h2>
                <div>
                    {cartes}
                </div>
            </div>
        );
    }
}

class CardForm extends React.Component {
  state = {
    card: "",
    filename: ""
  }

  onCardNameChange  = (event) => {
    const value = event.target.value;

    this.setState({card: value});
  }

  handleSubmitCard = (event) => {
    event.preventDefault();
    if (this.state.card !== "" && this.state.filename !== "") {

        this.props.onAddCard(this.state.card);
        return true;
    }
    return false;
  }

  onSelectFileChange = (event) => {
    console.log("change "+event);
    let tgt = event.target || window.event.srcElement;
    let files = tgt.files;
    if (FileReader && files && files.length) {
        console.log(files);
        let image = files[0]
        console.log("image : "+image.name);
        const reader = new FileReader();
        reader.onload = function () {
            document.getElementById("imageRecup").src = reader.result;
        }
        reader.readAsDataURL(image);
        this.setState({filename: image.name});
    } else {
        this.setState({filename: ""});
    }

    //document.getElementById("imageRecup").src = "data:image/jpg;base64,"+ resultat.toString('base64')

  }

  render() {
    return (
        <div>
        <h2>Ajouter une nouvelle carte</h2>

        <form onSubmit={this.handleSubmitCard}>
            <label htmlFor="nomCarte">Nom de la carte</label>
          &nbsp;<input type="text" name="nomCarte" id="nomCarte" size="30" required onChange={this.onCardNameChange}/>
          <br/>
            <label htmlFor="fichierImage">Sélectionnez l'image de la carte</label>
          &nbsp;<input type="file" name="fichierImage" id="fichierImage" onChange={this.onSelectFileChange}/>
          <br/>
            <div id="selectedImage">
                <img id="imageRecup" width="200" heeight="200"/>
            </div>
          <button type="submit" value="AddCard" id="addCard">Ajouter une carte</button>
        </form>
      </div>
    );
  }

} // class CardForm

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      address: "",
      connectedToMetamask: false,
      cartes: [],
      contractAddress: ""
    }
    this.addCard = this.addCard.bind(this);
  }

  async addCard(name) {
    console.log("Ajouter une carte "+name);
    let self = this;
    this.state.cartesSC.methods.ajouterCarte(name).send({from: this.state.address})
    .on('transactionHash', function(hash){
        console.log("transactionHash, "+hash);
    })
    .on('receipt', function(receipt){
        console.log("receipt, "+receipt);
        self.loadCartes();
    })
    .on('confirmation', function(confirmationNumber, receipt){
        console.log("confirmation, "+confirmationNumber+" "+receipt);
    })
    .on('error', function(error, receipt) {
        console.log("error, "+error+" "+receipt);
    });
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable();
      console.log("window.ethereum");
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
      console.log("window.web3");
    }
    else {
    }
  }

  async componentDidMount() {
    // Detect Metamask
    const metamaskInstalled = typeof window.web3 !== 'undefined'
    this.setState({ metamaskInstalled })
    if(metamaskInstalled) {
      await this.loadWeb3()
      await this.loadBlockchainData()
          console.log("METAMASK is installed");
          this.setState({connectedToMetamask: true});
    } else {
        console.log("Metamask is not installed.");
          this.setState({connectedToMetamask: false});
    }
  }

  async loadCartes() {
        const _cartes = await this.state.cartesSC.methods.getCartes().call({from: this.state.account});
        if (_cartes && _cartes.length > 0) {
            console.log("cartes "+_cartes.length);

            this.setState({cartes: _cartes});
        }
  }

    async loadBlockchainData() {
        const web3 = window.web3
        // Load account
        const accounts = await web3.eth.getAccounts()
        this.setState({ account: accounts[0] })
        const networkId = await web3.eth.net.getId()
        console.log("Account : "+accounts[0]);
        this.setState({address: accounts[0]});
        // Ganache local pour les tests
        //const networkData = Cartes.networks[networkId]

        // Ropsten
        const networkData = {}
        networkData.address = "0xA31abe7a4c6f0BF2F4c80e7937dE9020c05E188E";

        console.log(networkData);
        if (networkData) {
            console.log("Loading data from SC");
            //const contractAddress = networkData.address;
            console.log(networkData.address);
            const cartesSC = new web3.eth.Contract(Cartes.abi, networkData.address);
            this.setState({ cartesSC });
            this.setState({contractAddress: networkData.address });
            this.loadCartes();
            //console.log("Nb de cartes enregistrées :"+_cartes.length);
            //for (let i=0; i<_cartes.length; i++) {
            //    console.log(_cartes[i]);
            //}
            /*
            try {
                await cartes.methods.getCompanyInfos().call({from: accounts[0]}).then((infos) => {
                    console.log("commpany infos : "+infos+" ["+infos.name+"]");
                    this.setState({address: accounts[0], companyName: infos.name, registered: true});
                 });
            }catch {
                try {

                    const userInfos = await cartes.methods.getIllustratorInfos().call({from: accounts[0]});

                    if (userInfos) {
                        const jrs = await cartes.methods.getOpenJobRequests().call({from: accounts[0]});
                        console.log("Nb de demandes ouvertes :"+jrs.length);
                        for (let i=0; i<jrs.length; i++) {
                            console.log(jrs[i].description);
                        }
                    this.setState({address: accounts[0]});
                    }

                } catch {
                }
            }
            */
            /*

            let r = await marketPlace.methods.illustratorCount().call();
            console.log("Nombre d'illustrateurs:"+r);
            */

            this.setState({ loading: false})
        }
    }

    render() {

          if (! this.state.connectedToMetamask) {
            return (
              <div>
              <h1>{TITLE}</h1>
              <p>Vous devez être connecté avec Metamask sur la blockchain</p>
              </div>
            );
          }

          return (
            <div>
              <h1>{TITLE}</h1>
              <p>Adresse du smart contract: {this.state.contractAddress}</p>
              <p>Votre Adresse : {this.state.address}</p>
              <CardForm onAddCard={this.addCard} />
              <CardList cards={this.state.cartes} />
            </div>
          );

    } // render()
}

export default App;
