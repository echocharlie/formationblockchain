import unittest
from cercle import Cercle
from math import pi,pow


class MyTestCase(unittest.TestCase):

    def test_something(self):
        c1 = Cercle(rayon=10)
        c2 = Cercle(rayon=20)

        print(f"Périmètre de c1 : {c1.perimetre()}")
        print(f"Aire de c1 : {c1.aire()}")
        print(f"Périmètre de c2 : {c2.perimetre()}")
        print(f"Aire de c2 : {c2.aire()}")
        assert (c1.perimetre() == (2 * pi * 10))
        assert (c2.perimetre() == (2 * pi * 20))
        assert (c1.aire() == (pi * pow(10,2)))
        assert (c2.aire() == (pi * pow(20,2)))


if __name__ == '__main__':
    unittest.main()
