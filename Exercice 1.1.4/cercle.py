# -*- coding: utf8 -*-
from math import pi,pow

class Cercle:

    def __init__(self, rayon):
        self._rayon = rayon


    def aire(self):
        # PI x rayon^2
        return pi * (pow(self._rayon ,2))

    def perimetre(self):
        # 2 x PI x rayon
        return 2 * pi * self._rayon

