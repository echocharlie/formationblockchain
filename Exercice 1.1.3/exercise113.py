# -*- coding: utf8 -*-

"""
    Exercice 1.1.3 : Réaliser un vérificateur de palindrome (Python 3)

"""

test_words = [
"A but tuba.",
"A car, a man, a maraca.",
"A dog, a plan, a canal: pagoda.",
"A dog! A panic in a pagoda!",
"A lad named E. Mandala",
"A man, a plan, a canal: Panama.",
"A man, a plan, a cat, a ham, a yak, a yam, a hat, a canal-Panama!",
"A new order began, a more Roman age bred Rowena.",
"A nut for a jar of tuna.",
"A Santa at Nasa.",
"A Santa dog lived as a devil God at NASA.",
"A slut nixes sex in Tulsa.",
"A tin mug for a jar of gum, Nita.",
"A Toyota! Race fast, safe car! A Toyota!",
"A Toyota’s a Toyota.",
"Able was I ere I saw Elba.",
"Acrobats stab orca.",
"Aerate pet area.",
"Ah, Satan sees Natasha!",
"Aibohphobia (fear of palindromes)",
"Air an aria.",
"Al lets Della call Ed Stella.",
"alula",
"Amen icy cinema.",
"Amore, Roma.",
"Amy, must I jujitsu my ma?",
"Ana",
"Animal loots foliated detail of stool lamina.",
"Anna",
"Anne, I vote more cars race Rome to Vienna.",
"Are Mac ‘n’ Oliver ever evil on camera?",
"Are we not drawn onward to new era?",
"Are we not drawn onward, we few, drawn onward to new era?",
"Are we not pure? “No sir!” Panama’s moody Noriega brags. “It is garbage!” Irony dooms a man; a prisoner up to new era.",
"Art, name no tub time. Emit but one mantra.",
"As I pee, sir, I see Pisa!",
"Avid diva.",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie justo lectus, eu viverra dui finibus sit amet. Cras rhoncus ornare consequat. Aliquam a arcu aliquet, sollicitudin metus vitae, pellentesque mauris. Suspendisse dapibus ligula quis leo ultricies, sed vehicula ex ultrices. Donec facilisis eu quam quis rhoncus. Etiam nec est laoreet, dapibus orci non, feugiat dolor. Aliquam id imperdiet purus. Ut et egestas turpis. Nullam lacinia tellus mauris. Ut quis semper magna. Praesent sollicitudin ex vel sapien lacinia ornare. Nam tellus sem, elementum nec iaculis quis, suscipit id nisl. Quisque ut fringilla augue, nec interdum ex. Donec et erat quis felis laoreet dictum et eu felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque hendrerit volutpat venenatis.",
"Maecenas pellentesque in urna ac tristique. Curabitur pellentesque pretium pretium. Fusce nisi lectus, sagittis et lacus sit amet, aliquam sollicitudin erat. Suspendisse fringilla lacinia massa vel scelerisque. Mauris ac justo dapibus, dictum odio imperdiet, fringilla metus. Maecenas quis sem quis ante lobortis tristique. Pellentesque ut ante convallis, sodales lectus ut, gravida dui. Nullam imperdiet, massa et rutrum consequat, eros purus suscipit sapien, ac lacinia risus ligula vitae massa.",
"Nunc vel elit ipsum. Ut id pulvinar lectus, vitae faucibus augue. In at ex at enim mattis dignissim at vel ipsum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc tempus fringilla orci, at suscipit neque aliquet vel. Aenean urna elit, iaculis sit amet purus et, gravida feugiat turpis. Cras quis justo nec massa venenatis ullamcorper quis quis magna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus eget lacus non ex laoreet auctor. Maecenas tellus elit, ultrices ac consectetur vel, consectetur eget velit. Nunc lorem ipsum, elementum eget rhoncus in, pharetra ut velit. Etiam placerat molestie neque. Aliquam tempor et purus ut consequat.",
"Vestibulum scelerisque purus eu nulla lobortis, quis consectetur nisl congue. Maecenas sit amet volutpat nisi. Nam molestie lacinia tempus. Duis nec mi ut nisi cursus viverra. Ut volutpat turpis quis ante ultrices, lobortis fringilla magna ultrices. Donec sit amet risus dignissim, viverra lorem mollis, hendrerit orci. Integer sollicitudin scelerisque congue. Proin bibendum fermentum nisi ac luctus. Nulla congue arcu pretium iaculis ultrices.",
"Praesent varius nunc sit amet quam blandit rhoncus. Quisque aliquet convallis porta. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas nec turpis enim. Aenean sed tristique dolor, non feugiat massa. Aliquam efficitur ante imperdiet posuere sollicitudin. Integer maximus a justo sed pellentesque. Proin euismod, turpis sed ullamcorper ultricies, risus nunc porta erat, id porta purus risus in nibh. Aliquam eu mauris lectus. Ut tincidunt nulla sollicitudin, lacinia mauris a, hendrerit orci. Donec hendrerit, nisi vitae feugiat vehicula, risus nunc rutrum justo, aliquet tempus turpis nisl quis enim. Ut ullamcorper, dolor eu feugiat laoreet, metus lacus tempus est, eu luctus orci ex vitae est.",
]

def clear_text(text):
    """
    :param text: the text to clean
    :return: the text without special characters like " ' , ; . ( ! ? etc...
    """

    if text.isalpha(): return text

    r = ""
    for aLetter in text:
        if aLetter.isalpha():
            r += aLetter
    return r

def estPalindrome(text):
    """return True if word is a palindrome, else False"""
    clean_text = text
    if not text.isalpha():
        r = ""
        for aLetter in text:
            if aLetter.isalpha():
                r += aLetter
        clean_text = r

    clean_text = clean_text.lower()

    return clean_text == clean_text[::-1]

if __name__ == '__main__':

    for aWord in test_words:
        print(f"'{aWord}' est il un plaindrome ?")
        print(f"{estPalindrome(aWord)}")


    ct = clear_text(test_words[0])
    print(ct)