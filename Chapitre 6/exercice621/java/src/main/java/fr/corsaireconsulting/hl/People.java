package fr.corsaireconsulting.hl;

import java.util.Objects;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

import com.owlike.genson.annotation.JsonProperty;

@DataType()
public class People {

    @Property()
    private String name;
	
	public People() {
		this("");
	}
    
    public String getName() {
        return this.name;
    }

   
    public void setName(String name) {
    	this.name = name;
    }

    public People(@JsonProperty("name") String name) {
        setName(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }
		if (obj.hashCode() != this.hashCode()) return false;
        People other = (People) obj;

        return this.getName().equals(other.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + " [name=" + name + "]";
    }
}

