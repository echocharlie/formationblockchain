package fr.corsaireconsulting.hl;

import com.owlike.genson.annotation.JsonProperty;
import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

@DataType()
public class PackageTransfer {
	
	@Property()
	private int id;

	@Property()
	private Package thePackage;

	@Property()
	private Postman postman;
	
	@Property()
	private People from;

	@Property()
	private People to;
	

	public PackageTransfer(@JsonProperty("id") int id, 
	                       @JsonProperty("package") Package thePackage, 
						   @JsonProperty("postman") Postman postman,
						   @JsonProperty("from") People from,
						   @JsonProperty("to") People to) {
		super();
		setPostmaan(postman);
		setThePackage(thePackage);
		setFrom(from);
		setTo(from);
		setId(id);
	}

	public static void main(String[] args) {

	}

	public Package getThePackage() {
		return thePackage;
	}

	public void setThePackage(Package thePackage) {
		this.thePackage = thePackage;
	}

	public Postman getPostman() {
		return postman;
	}

	public void setPostmaan(Postman postman) {
		this.postman = postman;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public People getFrom() {
		return from;
	}

	public void setFrom(People from) {
		this.from = from;
	}

	public People getTo() {
		return to;
	}

	public void setTo(People to) {
		this.to = to;
	}	
}
