package fr.corsaireconsulting.hl;

import java.util.ArrayList;
import java.util.List;

import org.hyperledger.fabric.contract.Context;
import org.hyperledger.fabric.contract.ContractInterface;
import org.hyperledger.fabric.contract.annotation.Contact;
import org.hyperledger.fabric.contract.annotation.Contract;
import org.hyperledger.fabric.contract.annotation.Default;
import org.hyperledger.fabric.contract.annotation.Info;
import org.hyperledger.fabric.contract.annotation.License;
import org.hyperledger.fabric.contract.annotation.Transaction;
import org.hyperledger.fabric.shim.ChaincodeException;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ledger.KeyValue;
import org.hyperledger.fabric.shim.ledger.QueryResultsIterator;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Alyra - Exercice 6.2.1 : Réaliser un réseau Hyperledger pour suivre des colis postaux
 */
@Contract(
        name = "PostalPackagesTracker",
        info = @Info(
                title = "PostalPackagesTracker contract",
                description = "Suivi des colis postaux",
                version = "0.0.1-SNAPSHOT",
                license = @License(
                        name = "Apache 2.0 License",
                        url = "http://www.apache.org/licenses/LICENSE-2.0.html"),
                contact = @Contact(
                        email = "f.carr@example.com",
                        name = "EC",
                        url = "https://hyperledger.example.com")))
@Default
public final class PostalPackagesTracker implements ContractInterface {

    private final Genson genson = new GensonBuilder().useConstructorWithArguments(true).create();
	
	private static Log _logger = LogFactory.getLog(PostalPackagesTracker.class);

    /**
     * Creates some initial Cars on the ledger.
     *
     * @param ctx the transaction context
     */
    @Transaction()
    public void initLedger(final Context ctx) {
		 _logger.info("initLedger()");
        ChaincodeStub stub = ctx.getStub();
		
        PostOffice[] postOffices = {
                new PostOffice("Paris"),
				new PostOffice("Grenoble"),
				new PostOffice("Lyon"),
				new PostOffice("New-York"),
				new PostOffice("Mexico")};

        for (int i = 0; i < postOffices.length; i++) {
            String key = String.format("POSTOFFICE%03d", i);

            String postOfficeState = genson.serialize(postOffices[i]);
			_logger.info("json :"+postOfficeState);
            stub.putStringState(key, postOfficeState);
			_logger.info("Added postman "+key+" "+postOfficeState);
        }

        Postman[] postmen = {
                new Postman("Pierre"),
				new Postman("Paul") };

        for (int i = 0; i < postmen.length; i++) {
            String key = String.format("POSTMAN%03d", i);

            //Postman aPostman = genson.deserialize(postmen[i], Postman.class);
            String aPostmanState = genson.serialize(postmen[i]);
			_logger.info("json :"+aPostmanState);
            stub.putStringState(key, aPostmanState);
			_logger.info("Added postman "+key+" "+aPostmanState);
        }
		
        Customer[] customers = {
                new Customer("Emmanuel",new Address("Paris")),
                new Customer("Juliette", new Address("Mexico")),
                new Customer("Alexandre", new Address("Grenoble"))};

        for (int i = 0; i < customers.length; i++) {
            String key = String.format("CUST%03d", i);

            //Customer customer = genson.deserialize(customers[i], Customer.class);
            String customerState = genson.serialize(customers[i]);
			_logger.info("json :"+customerState);
            stub.putStringState(key, customerState);
			_logger.info("Added customer "+key+" "+customerState);
        }

    }
	

    /**
     * Retrieves every postmen from the ledger.
     *
     * @param ctx the transaction context
     * @return array of Postman found on the ledger
     */
    @Transaction()
    public Postman[] queryAllPostmen(final Context ctx) {
		 _logger.info("queryAllPostmen()");
        ChaincodeStub stub = ctx.getStub();

        final String startKey = "POSTMAN0";
        final String endKey = "POSTMAN999";
        List<Postman> postmen = new ArrayList<Postman>(0);

        QueryResultsIterator<KeyValue> results = stub.getStateByRange(startKey, endKey);

        for (KeyValue result: results) {
			_logger.info("result.getStringValue():"+result.getStringValue());
            Postman postman = genson.deserialize(result.getStringValue(), Postman.class);
			_logger.info("postman:"+postman.toString());
            postmen.add(postman);
        }

        Postman[] response = postmen.toArray(new Postman[postmen.size()]);

        return response;
    }
	
    /**
     * Retrieves every customers from the ledger.
     *
     * @param ctx the transaction context
     * @return array of Customer found on the ledger
     */
    @Transaction()
    public Customer[] queryAllCustomers(final Context ctx) {
		 _logger.info("queryAllCustomers()");
        ChaincodeStub stub = ctx.getStub();

        final String startKey = "CUST0";
        final String endKey = "CUST999";
        List<Customer> customers = new ArrayList<Customer>(0);

        QueryResultsIterator<KeyValue> results = stub.getStateByRange(startKey, endKey);

        for (KeyValue result: results) {
			_logger.info("result.getStringValue():"+result.getStringValue());
            Customer customer = genson.deserialize(result.getStringValue(), Customer.class);
			_logger.info("customer:"+customer.toString());
            customers.add(customer);
        }

        Customer[] response = customers.toArray(new Customer[customers.size()]);

        return response;
    }	
	
    /**
     * Create a package in the ledger.
     *
     * @param ctx the transaction context
     * @param senderKey 
     * @param recipientKey
     * @param label 
     * @return the created package
     */
    @Transaction()
    public Package createPackage(final Context ctx, final String senderKey, final String recipientKey, final String label) {
		 _logger.info("createPackage()");
        ChaincodeStub stub = ctx.getStub();
		
		String senderState = stub.getStringState(senderKey);

        if (senderState.isEmpty()) {
            String errorMessage = String.format("Customer with key %s does not exist", senderKey);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
        }

		String recipientState = stub.getStringState(recipientKey);

        if (recipientState.isEmpty()) {
            String errorMessage = String.format("Customer with key %s does not exist", recipientKey);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
        }
		
		Customer sender = genson.deserialize(senderState, Customer.class);
		_logger.info("sender : "+sender);
		Customer recipient = genson.deserialize(recipientState, Customer.class);
		_logger.info("recipient : "+sender);
		
		Package newPackage = PackageManager.getInstance().createPackage(label, sender, recipient);
		_logger.info("newPackage : "+newPackage);

		String packageState = genson.serialize(newPackage);
		_logger.info("json :"+packageState);
		String key = String.format("PACKAGE%03d", newPackage.getId());
		stub.putStringState(key, packageState);

        return newPackage;
    }		
	
    /**
     * Retrieves every packages from the ledger.
     *
     * @param ctx the transaction context
     * @return array of Package found on the ledger
     */
    @Transaction()
    public Package[] queryAllPackages(final Context ctx) {
		 _logger.info("queryAllPackages()");
        ChaincodeStub stub = ctx.getStub();

        final String startKey = "PACKAGE0";
        final String endKey = "PACKAGE999";
        List<Package> packages = new ArrayList<Package>(0);

        QueryResultsIterator<KeyValue> results = stub.getStateByRange(startKey, endKey);

        for (KeyValue result: results) {
			_logger.info("result.getStringValue():"+result.getStringValue());
            Package aPackage = genson.deserialize(result.getStringValue(), Package.class);
			_logger.info("aPackage:"+aPackage.toString());
            packages.add(aPackage);
        }

        Package[] response = packages.toArray(new Package[packages.size()]);

        return response;
    }	
	
	 /**
     * a customer sends its package to a postoffice
     *
     * @param ctx the transaction context
     * @param packageKey
     * @param postOfficeKey
     */
    @Transaction()
    public PackageTransfer postPackage(final Context ctx, final String packageKey, final String postOfficeKey) {
		 _logger.info("postPackage()");
        ChaincodeStub stub = ctx.getStub();
		
		String packageState = stub.getStringState(packageKey);

        if (packageState.isEmpty()) {
            String errorMessage = String.format("Package with key %s does not exist", packageState);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
        }

		String postOfficeState = stub.getStringState(postOfficeKey);

        if (postOfficeState.isEmpty()) {
            String errorMessage = String.format("PostOffice with key %s does not exist", postOfficeKey);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
        }
		
		Package thePackage = genson.deserialize(packageState, Package.class);
		_logger.info("package : "+thePackage);
		PostOffice postOffice = genson.deserialize(postOfficeState, PostOffice.class);
		_logger.info("postOffice : "+postOffice);
		
		PackageTransfer pt = PackageManager.getInstance().postPackage(thePackage, postOffice);
		_logger.info("pt : "+pt);

		// Création du transfert
		String ptState = genson.serialize(pt);
		_logger.info("json :"+ptState);
		String key = String.format("PACKAGETX%03d", pt.getId());
		stub.putStringState(key, ptState);
		
		// Mise à jour du package
		packageState = genson.serialize(thePackage);
		_logger.info("json :"+packageState);
		key = String.format("PACKAGE%03d", thePackage.getId());
		stub.putStringState(key, packageState);		

        return pt;
    }	
		
    /**
     * Send a package from a post office to anothoer post office
     *
     * @param ctx the transaction context
     * @param packageKey
     * @param postOfficeKey
     * @return PackageTransfer
     */
    @Transaction()
    public PackageTransfer sendPackage(final Context ctx, final String packageKey, final String postOfficeKey) {
		 _logger.info("sendPackage()");
        ChaincodeStub stub = ctx.getStub();
		
		String packageState = stub.getStringState(packageKey);

        if (packageState.isEmpty()) {
            String errorMessage = String.format("Package with key %s does not exist", packageState);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
        }

		String postOfficeState = stub.getStringState(postOfficeKey);

        if (postOfficeState.isEmpty()) {
            String errorMessage = String.format("Post Office with key %s does not exist", postOfficeKey);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
        }
		
		Package thePackage = genson.deserialize(packageState, Package.class);
		_logger.info("package : "+thePackage);
		PostOffice postOffice = genson.deserialize(postOfficeState, PostOffice.class);
		_logger.info("postOffice : "+postOffice);
		
		if (thePackage.getCurrentLocation().equals(postOffice)) {
            String errorMessage = String.format("Package with key %s already sent to destination", packageKey);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
		}
		
		PackageTransfer pt = PackageManager.getInstance().sendPackage(thePackage, postOffice);
		_logger.info("pt : "+pt);

		String ptState = genson.serialize(pt);
		_logger.info("json :"+ptState);
		String key = String.format("PACKAGETX%03d", pt.getId());
		stub.putStringState(key, ptState);
		
		// Mise à jour du package
		packageState = genson.serialize(thePackage);
		_logger.info("json :"+packageState);
		key = String.format("PACKAGE%03d", thePackage.getId());
		stub.putStringState(key, packageState);				

        return pt;
    }

    /**
     * Distribute a package from a post office to the recipient
     *
     * @param ctx the transaction context
     * @param packageKey
     * @param postmanKey
     * @return PackageTransfer
     */
    @Transaction()
    public PackageTransfer distributePackage(final Context ctx, final String packageKey, final String postmanKey) {
		 _logger.info("sendPackage()");
        ChaincodeStub stub = ctx.getStub();
		
		String packageState = stub.getStringState(packageKey);

        if (packageState.isEmpty()) {
            String errorMessage = String.format("Package with key %s does not exist", packageState);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
        }

		String postmanState = stub.getStringState(postmanKey);

        if (postmanState.isEmpty()) {
            String errorMessage = String.format("Postman with key %s does not exist", postmanKey);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
        }
		
		Package thePackage = genson.deserialize(packageState, Package.class);
		_logger.info("package : "+thePackage);
		
		Postman postman = genson.deserialize(postmanState, Postman.class);
		_logger.info("postman : "+postman);
		
		if (thePackage.getCurrentLocation().equals(thePackage.getRecipient())) {
            String errorMessage = String.format("Package with key %s already sent to destination", packageKey);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
		}
		
		PackageTransfer pt = PackageManager.getInstance().distributePackage(thePackage, postman);
		_logger.info("pt : "+pt);

		String ptState = genson.serialize(pt);
		_logger.info("json :"+ptState);
		String key = String.format("PACKAGETX%03d", pt.getId());
		stub.putStringState(key, ptState);
		
		// Mise à jour du package
		packageState = genson.serialize(thePackage);
		_logger.info("json :"+packageState);
		key = String.format("PACKAGE%03d", thePackage.getId());
		stub.putStringState(key, packageState);		

		// Déclencher un évènement
		stub.setEvent("Distribution", packageState.getBytes());

        return pt;
    }

    /**
     * Retrieves every package transfers from the ledger.
     *
     * @param ctx the transaction context
     * @return array of PackageTransfer found on the ledger
     */
    @Transaction()
    public PackageTransfer[] queryAllPackageTransfers(final Context ctx) {
		 _logger.info("queryAllPackageTransfers()");
        ChaincodeStub stub = ctx.getStub();

        final String startKey = "PACKAGETX0";
        final String endKey = "PACKAGETX999";
        List<PackageTransfer> packages = new ArrayList<PackageTransfer>(0);

        QueryResultsIterator<KeyValue> results = stub.getStateByRange(startKey, endKey);

        for (KeyValue result: results) {
			_logger.info("result.getStringValue():"+result.getStringValue());
            PackageTransfer aPackage = genson.deserialize(result.getStringValue(), PackageTransfer.class);
			_logger.info("aPackage:"+aPackage.toString());
            packages.add(aPackage);
        }

        PackageTransfer[] response = packages.toArray(new PackageTransfer[packages.size()]);

        return response;
    }	

    @Transaction()
    private Package updatePackageState(final Context ctx, final String packageKey, final Package.PackageState state) {
		 _logger.info("updatePackageState()");
        ChaincodeStub stub = ctx.getStub();
		
		String packageState = stub.getStringState(packageKey);

        if (packageState.isEmpty()) {
            String errorMessage = String.format("Package with key %s does not exist", packageKey);
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);
        }
		
		// Lecture du package
		Package thePackage = genson.deserialize(packageState, Package.class);
		_logger.info("thePackage : "+thePackage);

		// Vérifier les transitions de l'état du colis
		// GOOD --> DAMAGED : OK
		// GOOD --> DESTROYED : OK
		// DAMAGED --> DESTROYED: OK
		// Toutes les autres transitions sont KO
		boolean ok = (thePackage.getState().equals(Package.PackageState.GOOD) && state.equals(Package.PackageState.DAMAGED));
		ok = (ok || (thePackage.getState().equals(Package.PackageState.GOOD) && state.equals(Package.PackageState.DESTROYED)));
		ok = (ok || (thePackage.getState().equals(Package.PackageState.DAMAGED) && state.equals(Package.PackageState.DESTROYED)));
		
		if (! ok) {
            String errorMessage = String.format("State transition from "+thePackage.getState().toString()+" to "+state.toString()+" is not allowed");
            System.out.println(errorMessage);
			_logger.error(errorMessage);
            throw new ChaincodeException(errorMessage, errorMessage);			
		}
								
		// Modification de son état
		thePackage.setState(state);
		
		// Sauvegarde du package
		packageState = genson.serialize(thePackage);
		_logger.info("json :"+packageState);
		String key = String.format("PACKAGE%03d", thePackage.getId());
		stub.putStringState(key, packageState);

        return thePackage;
    }		
	
    @Transaction()
    public Package updatePackageStateAsGood(final Context ctx, final String packageKey) {
		return this.updatePackageState(ctx, packageKey, Package.PackageState.GOOD);
	}

    @Transaction()
    public Package updatePackageStateAsDamaged(final Context ctx, final String packageKey) {
		return this.updatePackageState(ctx, packageKey, Package.PackageState.DAMAGED);
	}

    @Transaction()
    public Package updatePackageStateAsDestroyed(final Context ctx, final String packageKey) {
		return this.updatePackageState(ctx, packageKey, Package.PackageState.DESTROYED);
	}

}

