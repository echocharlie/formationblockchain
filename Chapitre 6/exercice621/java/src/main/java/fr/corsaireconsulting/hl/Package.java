package fr.corsaireconsulting.hl;

import java.util.Objects;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

import com.owlike.genson.annotation.JsonProperty;

@DataType()
public final class Package {
	
	public enum PackageState {
        GOOD,
        DAMAGED,
		DESTROYED
    }
	
	@Property()
	private int id;
	
	@Property()
	private String label;

	@Property()
	private Customer sender;
	
	@Property()
	private Customer recipient;
	
	@Property()
	private People currentLocation;
	
	@Property()
	private PackageState state;
	

	public Package(@JsonProperty("id") int id, @JsonProperty("label") String label, @JsonProperty("sender") Customer sender, @JsonProperty("recipient") Customer recipient) {
		super();
		setId(id);
		setLabel(label);
		setRecipient(recipient);
		setSender(sender);
		
		setCurrentLocation(sender);
		setState(PackageState.GOOD);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Customer getSender() {
		return sender;
	}

	public void setSender(Customer sender) {
		this.sender = sender;
	}

	public Customer getRecipient() {
		return recipient;
	}

	public void setRecipient(Customer recipient) {
		this.recipient = recipient;
	}	

	@Override
	public String toString() {
		return "Package [id="+id+", label=" + label + ", sender=" + sender + ", recipient=" + recipient + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + ((recipient == null) ? 0 : recipient.hashCode());
		result = prime * result + ((sender == null) ? 0 : sender.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Package other = (Package) obj;
		
		return this.id == other.getId();
	}

	public People getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(People currentLocation) {
		this.currentLocation = currentLocation;
	}	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	

	public PackageState getState() {
		return state;
	}

	public void setState(PackageState state) {
		this.state = state;
	}
}