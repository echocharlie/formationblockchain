package fr.corsaireconsulting.hl;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

import com.owlike.genson.annotation.JsonProperty;

@DataType()
public final class Postman extends People {
	
	public Postman() {
		super("");
	}

    public Postman(@JsonProperty("name") final String name) {
		super(name);
    }

}