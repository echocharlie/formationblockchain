package fr.corsaireconsulting.hl;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

import com.owlike.genson.annotation.JsonProperty;

@DataType()
public final class Address {

    @Property()
    private String city;
    
    public String getCity() {
        return this.city;
    }

    public Address(@JsonProperty("city") String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        if (obj.hashCode() != this.hashCode()) return false;
		
		Address other = (Address) obj;
		
        return other.city.equals(this.city);
	}

    @Override
    public int hashCode() {
        return this.city.hashCode();
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "@" + Integer.toHexString(hashCode()) + " [city=" + city + "]";
    }
}