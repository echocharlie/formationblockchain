package fr.corsaireconsulting.hl;

import org.hyperledger.fabric.contract.annotation.DataType;
import org.hyperledger.fabric.contract.annotation.Property;

import com.owlike.genson.annotation.JsonProperty;

@DataType()
public final class Customer extends People {
	
	@Property()
    private Address address;
	
	public Customer() {
		this("",null);
	}

    public Customer(@JsonProperty("name") String name, @JsonProperty("address") Address address) {
		super(name);
		
		setAddress(address);
    }
	
    public void setAddress(Address address) {
    	this.address = address;
    }
    
    @Override
	public String toString() {
		return "Customer "+getName()+" [address=" + address + "]";
	}

	public Address getAddress() {
    	return this.address;
    }

}