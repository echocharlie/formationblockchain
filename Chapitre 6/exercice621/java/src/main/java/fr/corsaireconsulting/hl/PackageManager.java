package fr.corsaireconsulting.hl;

import com.owlike.genson.Genson;

public class PackageManager {
	
	private int nextPackageId = 0;	
	private int nextPackageTransferId = 0;	
	
	private static class SingletonHelper{

		private static final PackageManager INSTANCE = new PackageManager();

	}
	public static PackageManager getInstance() {
		return SingletonHelper.INSTANCE;
	}	
	

	public PackageManager() {
	}
	
	public int getNextPackageId() {
		return this.nextPackageId;
		
	}
	
	public int getNextPackageTransferId() {
		return this.nextPackageTransferId;
	}	
	
	public Package createPackage(String label, Customer sender, Customer recipient) {
		Package pack = new Package(getNextPackageId(), label, sender, recipient);
		this.nextPackageId++;
		return pack;
	}
	
	// Sender to post office
	public PackageTransfer postPackage(Package thePackage, PostOffice postOffice) {
		System.out.println("Post package "+thePackage+" to post office "+postOffice);
		
		thePackage.setCurrentLocation(postOffice);
		PackageTransfer pt = new PackageTransfer(getNextPackageTransferId(), thePackage, null, thePackage.getSender(), postOffice);
		this.nextPackageTransferId++;
		return pt;
	}

	// post office to post office
	public PackageTransfer sendPackage(Package thePackage, PostOffice postOffice) {
		System.out.println("Send package "+thePackage+" from "+thePackage.getCurrentLocation()+" to "+postOffice);
		
		thePackage.setCurrentLocation(postOffice);
		PackageTransfer pt = new PackageTransfer(getNextPackageTransferId(), thePackage, null, thePackage.getCurrentLocation(), postOffice);
		this.nextPackageTransferId++;
		return pt;
	}

	// post office to Recipient
	public PackageTransfer distributePackage(Package thePackage, Postman postman) {
		System.out.println("Distribute package "+thePackage+" by "+postman+" to "+thePackage.getRecipient());
		
		thePackage.setCurrentLocation(thePackage.getRecipient());
		PackageTransfer pt = new PackageTransfer(getNextPackageTransferId(), thePackage, postman, thePackage.getCurrentLocation(), thePackage.getRecipient());
		this.nextPackageTransferId++;
		return pt;
	}

	public Package updatePackageStatee(Package thePackage, Package.PackageState state) {
		System.out.println("Update package "+thePackage+" to state "+state.toString());
		
		thePackage.setState(state);
		return thePackage;
	}


	public static void main(String[] args) {
		final Genson genson = new Genson();

		PackageManager manager = PackageManager.getInstance();
		
        PostOffice[] postOffices = {
                new PostOffice("Paris"),
				new PostOffice("Briançon"),
				new PostOffice("Lyon"),
				new PostOffice("New-York"),
				new PostOffice("Mexico")};		
		
		Customer[] customers = {
				new Customer("Emmanuel",new Address("Paris")),
				new Customer("Juliette", new Address("Mexico")),
				new Customer("François", new Address("Briançon")) };

		Postman[] postmen = {
				new Postman("Pierre"),
				new Postman("Paul") };

		Package p1 = manager.createPackage("Colis 1", customers[0], customers[1]);

		manager.postPackage(p1, postOffices[0]);
		
		
	}

}
