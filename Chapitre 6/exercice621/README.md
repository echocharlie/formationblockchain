# Alyra - dev blockchain

TP Hyperledger

Exercices :
* 6.2.1
* 6.2.2
* 6.2.3

Les 3 exercices sont regroupés dans cet unique projet.
___

Langage : JAVA
Projet dans le répertoire java

Classe du chaincode : `fr.corsaireconsulting.hl.PostalPackagesTracker`
___
# Variable d'environnement
La variable d'environnement TESTPATH doit pointer sur le répertoire `test-network` de l'installation Hyperledger. Exemple : 
`TESTPATH=/home/hyper/fabric-samples/test-network`

# Lancement du réseau de test Hyperledger
`cd ./fabric-samples/test-network`
`./network.sh up createChannel`

# Arrêt du réseau de test Hyperledger
`cd ./fabric-samples/test-network`
`sudo ./network.sh up down`

# Compilation et déploiement

Exécuter `compile.sh`

Le script gère toutes les étapes ainsi que les mises à jour du code.

# Exécution des scénarios

## Consultation des logs de l'application

`docker logs -f &#96;docker ps --latest --format {{'.Names'}}&#96;`

## Exécution des commandes

### Formatage json des sorties
Ajouter les commandes suivantes pour formater la sortie des requêtes : 
`2>&1 | grep -Po "payload:(.*)" | grep -Po "\"(.*)" | cut -c2- | rev | cut -c2- | cut -c2- | rev | sed 's/\\"/"/g' | jq`

### initialisation du chaincode
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt --isInit -c '{"function":"initLedger","Args":[]}' `

### Mise à jour des données de test
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"initLedger","Args":[]}'` 

### liste des customers
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"queryAllCustomers","Args":[]}'`


### liste des postmen
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"queryAllPostmen","Args":[]}'`

### création d'un colis
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"createPackage","Args":["CUST001","CUST002","Colis 1"]}'`

`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"createPackage","Args":["CUST000","CUST001","Colis 2"]}'`

`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"createPackage","Args":["CUST002","CUST001","Colis 3"]}'`

### liste de tous les colis
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"queryAllPackages","Args":[]}'`

### Poster un colis
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"postPackage","Args":["PACKAGE001","POSTOFFICE001"]}'`

### Transférer un colis entre 2 bureaux de poste
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"sendPackage","Args":["PACKAGE001","POSTOFFICE002"]}'`

### Distribution du colis au destinataire
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"distributePackage","Args":["PACKAGE001","POSTMAN001"]}'`

### liste de tous les transferts de colis
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"queryAllPackageTransfers","Args":[]}'`

### Changer l'état d'un colis
GOOD --> DAMAGED : OK
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"updatePackageStateAsDamaged","Args":["PACKAGE000"]}'`

DAMAGED --> DESTROYED: OK
`peer chaincode invoke -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem -C mychannel -n exercice621 --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c '{"function":"updatePackageStateAsDestroyed","Args":["PACKAGE001"]}'`



