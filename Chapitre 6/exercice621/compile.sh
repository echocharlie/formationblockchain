#!/bin/bash


# compilation du code
echo "Compilation..."
cd /vagrant/exercice621/java 
./gradlew installDist

JAVA_COMPILE_RESULT=$?

if [ "$JAVA_COMPILE_RESULT" != "0" ]
then
	echo -e "\nErreur de compilation"
	exit
fi
	

# pachage
echo "Création du package..."
rm exercice621.tar.gz
peer lifecycle chaincode package exercice621.tar.gz --path /vagrant/exercice621/java/build/install/exercice621 --lang java --label exercice621_1

if [ : -e exercice621.tar.gz ]
then
	echo -e "\nErreur lors de la création du package."
	exit
fi

# installation
echo "Installation du package sur Org1MSP"
export CORE_PEER_LOCALMSPID="Org1MSP" 
export CORE_PEER_TLS_ROOTCERT_FILE=${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt 
export CORE_PEER_MSPCONFIGPATH=${TESTPATH}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp 
export CORE_PEER_ADDRESS=localhost:7051 

TEST=`peer lifecycle chaincode install exercice621.tar.gz 2>&1 | tail -n1`
CC_PACKAGE_ID=${TEST#*"identifier: "}
echo -e "\nCC_PACKAGE_ID : "$CC_PACKAGE_ID"\n"
# peer lifecycle chaincode install exercice621.tar.gz

echo "Installation du package sur Org2MSP"

export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${TESTPATH}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
export CORE_PEER_ADDRESS=localhost:9051

peer lifecycle chaincode install exercice621.tar.gz

echo "Détermination du numéro de séquence..."
NB_LINES=`peer lifecycle chaincode queryinstalled | wc -l`
NEW_SEQ=$((NB_LINES-1))
echo -e "Séquence : "$NEW_SEQ"\n"

echo "Approbation du package sur Org2MSP"
export CORE_PEER_LOCALMSPID="Org2MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${TESTPATH}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
export CORE_PEER_ADDRESS=localhost:9051
peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name exercice621 --version 1.0 --init-required --package-id $CC_PACKAGE_ID --sequence $NEW_SEQ --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

echo "Approbation du package sur Org1MSP"
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${TESTPATH}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_ADDRESS=localhost:7051
peer lifecycle chaincode approveformyorg -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name exercice621 --version 1.0 --init-required --package-id $CC_PACKAGE_ID --sequence $NEW_SEQ --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

echo "Commit..."
peer lifecycle chaincode checkcommitreadiness --channelID mychannel --name exercice621 --version 1.0 --init-required --sequence $NEW_SEQ --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --output json

peer lifecycle chaincode commit -o localhost:7050 --ordererTLSHostnameOverride orderer.example.com --channelID mychannel --name exercice621 --version 1.0 --sequence $NEW_SEQ --init-required --tls true --cafile ${TESTPATH}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem --peerAddresses localhost:7051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt --peerAddresses localhost:9051 --tlsRootCertFiles ${TESTPATH}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
