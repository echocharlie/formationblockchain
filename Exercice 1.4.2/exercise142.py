# -*- coding: utf8 -*-


"""

    Exercice 1.4.2 : Convertir un nombre en little endian
    https://ecole.alyra.fr/mod/assign/view.php?id=65
    https://forum.alyra.fr/t/exercice-1-4-2-convertir-un-nombre-en-little-endian/85

"""
import math

def to_binary(number, bigendian=True):
    # combien faut il de bits pour représenter number ? (dans la limite de 1024 bits)
    for i in range(1024):
        if math.pow(2, i) > number:
            break
    result = [0] * (math.ceil(i / 8) * 8)

    last = -1
    index = len(result) - 1
    while number > 0:
        #print ("1",index, math.pow(2,index))
        if  math.pow(2, index) > number:
            last = math.pow(2, index)
            #print("last",last)
        else:
            #print("2",last, number)
            number -= math.pow(2, index)
            result[len(result)-1-index] = 1
        index-=1

    if not bigendian:
        result = result[16:20] + result[20:24] + result[8:12] + result[12:16] + result[0:4] + result[4:8]

    return result

HEXA = "0123456789ABCDEF"

def to_hex(binary_number):
    result = ""
    index = len(binary_number) - 1
    bit_n = 0
    sum = 0
    while index >= 0:
        if bit_n > 3:
            bit_n = 0
            result = HEXA[sum] + result
            sum = 0

        sum += int(math.pow(2, bit_n)) * binary_number[index]

        index -= 1
        bit_n += 1
    result = HEXA[sum] + result

    return result


def conversion(number):
    print(f"{number} --> 0x{to_hex(to_binary(number, bigendian=True))} (big endian)")
    print(f"--> 0x{to_hex(to_binary(number, bigendian=False))} (little endian)")

    hex_number = to_hex(to_binary(number, bigendian=False))

    prefix = ""
    # nombre d'octets utilisés pour représenter le nombre
    bytes_count = int(len(hex_number) / 2)

    # nombre d'octets sur lequel il faut représenter le nombre
    bytes_format = 1

    print ("bytes count = ",bytes_count)
    if number > 253:
        prefix = "FD"
        bytes_format = 2
        if bytes_count > 2 and bytes_count <= 4:
            prefix = "FE"
            bytes_format = 4
        elif len(hex_number) > 4:
            prefix = "FF"
            bytes_format = 8


    # compléter pour obtenir le nombre d'octets attendus
    hex_number += "00"*(bytes_format - bytes_count)
    print(f"--> 0x{prefix}{hex_number} (notation variable)")


if __name__ == '__main__':

    print(to_binary(365))
    print(to_binary(365,bigendian=False))

    print(to_hex(to_binary(365)))
    print(to_hex(to_binary(365,bigendian=False)))

    print(to_hex(to_binary(131)))
    print(to_hex(to_binary(131,bigendian=False)))
    print(to_hex(to_binary(255)))
    print(to_hex(to_binary(255,bigendian=False)))
    print(to_hex(to_binary(643)))
    print(to_hex(to_binary(643,bigendian=False)))

    print(to_binary(466321))
    print(to_binary(466321,bigendian=False))
    print(to_hex(to_binary(466321)))
    print(to_hex(to_binary(466321,bigendian=False)))

    # vérification
    print((365).to_bytes(2, byteorder='big').hex())
    print((365).to_bytes(2, byteorder='little').hex())

    conversion(131)
    conversion(255)
    conversion(643)
    conversion(466321)
