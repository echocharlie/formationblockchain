# -*- coding: utf8 -*-


"""
    Exercice 1.2.8 : Créer un arbre de Merkle

"""


import hashlib


def get_hash(w):
    return hashlib.sha256(w).hexdigest()


def build_merkle_tree(data):
    merkle_tree = []
    leaves = []
    for w in data:
        leaves.append(get_hash(w.encode()))
    merkle_tree.append(leaves)
    work = leaves
    orphan = None  # si il y a un nombre impair d'éléments il va en rester 1 qu'on traitera avec la prochaine itération avec un nombre impair d'éléments
    while len(work) > 1:
        new_hash = []
        i = 0
        while i < len(work):
            if i + 1 < len(work):
                s = work[i] + work[i + 1]
                new_hash.append(get_hash(s.encode()))
                i += 2
            else:
                orphan = work[-1]
                print("reste ", orphan)
                i += 1

        work = new_hash
        merkle_tree.append(new_hash)

        # ajouter l'orphelin à la prochaine itération
        if orphan and (len(work) % 2 != 0):
            work.append(orphan)
            orphan = None

    return merkle_tree

if __name__ == '__main__':
    print(hashlib.algorithms_available)

    print(hashlib.sha256(b"welcome").hexdigest())
    print(hashlib.sha256(b"welcome").hexdigest())
    print(hashlib.sha256(b"welcome").hexdigest())
    print(hashlib.sha256(b"lapin").hexdigest())

    """
    words = []
    while True:
        i = input("Entrez une chaîne de caractères. (châine vide pour arrêter) > ")
        if not i:
            break

        words.append(i)
    """
    words = ["Lorem","ipsum","dolor","sit","amet","consectetur","adipiscing","elit","Vestibulum","a"]
    if len(words) > 0:

        print(f"ArbreMerkle {' '.join(words)}")

        # création des hash

        #print(leaves)
        merkle_tree = build_merkle_tree(words)

        for a in merkle_tree:
            print(a)





