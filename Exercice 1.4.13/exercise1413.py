# -*- coding: utf8 -*-


"""

    Exercice 1.4.13 : Commencer à adapter sa preuve de travail au niveau de difficulté
    https://ecole.alyra.fr/mod/assign/view.php?id=77
    https://forum.alyra.fr/t/exercice-1-4-13-commencer-a-adapter-sa-preuve-de-travail-au-niveau-de-difficulte/97

"""

import math
import binascii

def target_int2bits(target):
    # comprehensive explanation here: bitcoin.stackexchange.com/a/2926/2116

    # get in base 256 as a hex string
    target_hex = int2hex(target)

    bits = "00" if (hex2int(target_hex[: 2]) > 127) else ""
    bits += target_hex # append
    bits = hex2bin(bits)
    length = int2bin(len(bits), 1)

    # the bits value could be zero (0x00) so make sure it is at least 3 bytes
    bits += hex2bin("0000")

    # the bits value could be bigger than 3 bytes, so cut it down to size
    bits = bits[: 3]

    return length + bits

def bits2target_int(bits_bytes):
    exp = bin2int(bits_bytes[: 1]) # exponent is the first byte
    print("exp ",exp)
    mult = bin2int(bits_bytes[1:]) # multiplier is all but the first byte
    print("mult : ",mult, hex(mult))
    return mult * (2 ** (8 * (exp - 3)))

def int2hex(intval):
    hex_str = hex(intval)[2:]
    if hex_str[-1] == "L":
        hex_str = hex_str[: -1]
    if len(hex_str) % 2:
        hex_str = "0" + hex_str
    return hex_str

def hex2int(hex_str):
    return int(hex_str, 16)

def hex2bin(hex_str):
    return binascii.a2b_hex(hex_str)

def int2bin(val, pad_length = False):
    hexval = int2hex(val)
    if pad_length: # specified in bytes
        hexval = hexval.zfill(2 * pad_length)
    return hex2bin(hexval)

def bin2hex(binary):
    # convert raw binary data to a hex string. also accepts ascii chars (0 - 255)
    return binascii.b2a_hex(binary)

def bin2int(binary):
    return hex2int(bin2hex(binary))


def cibleAtteinte(coef, exp, hash):

    # Cible = cœfficient * 2 ** (8*(exposant-3)).

    target = int(coef, 16) * 2 ** (8 * (exp-3))

    return int(hash,16) < target

if __name__ == '__main__':

    # Bloc #557098
    # hash : 00000000000000000019b2634066a100e56ed58a0ae40ca5a4e2d1dba6a4be22
    Bits =389159077
    # Difficulté     5 618 595 848 853,28
    # Nonce 3 337 784 065

    hexa = hex(Bits)
    bits_bytes = bytearray.fromhex(hexa[2:])
    print("---> ", bits2target_int(bits_bytes), hex(bits2target_int(bits_bytes)))

    r = cibleAtteinte("3218a5", 23, "00000000000000000019b2634066a100e56ed58a0ae40ca5a4e2d1dba6a4be22")

    print(r)
