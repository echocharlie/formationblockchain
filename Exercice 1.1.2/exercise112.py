# -*- coding: utf8 -*-

"""
    Trouver un nombre saisi par l'utilisateur efficacement

"""

import random


def compare(a, b):
    if a == b:
        return 0
    elif a > b:
        return 1
    else:
        return -1

if __name__ == '__main__':

    input_ok = False
    while not input_ok:
        input_number = input("Veuillez saisir un nombre entre 1 (compris) et 100 (compris)  > ")
        try:
            solution = int(input_number)
            input_ok = solution >= 1 and solution <= 100
        except ValueError as te:
            print("Merci de saisir un nombre")

    print(f"rechercherParOrdi {solution}")

    found = False
    range_min = 1
    range_max = 100
    while not found:
        estimation = random.randint(range_min, range_max)
        print (f"Ma proposition est {estimation}")
        r = compare(solution, estimation)
        if r == 0:
            found = True
        elif r > 0:
            range_min = estimation + 1
        else:
            range_max = estimation - 1

    print("Exact !")