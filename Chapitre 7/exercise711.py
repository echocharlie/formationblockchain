# -*- coding: utf8 -*-


"""

    Exercice 7.1.1 : Comparer les livres d’ordres (Python 3)
    https://ecole.alyra.fr/mod/assign/view.php?id=251
    https://forum.alyra.fr/t/exercice-7-1-1-comparer-les-livres-d-ordres/204

"""


import requests
import json
from datetime import datetime

MOCK_DATA_BITMEX = [{'symbol': 'XBTUSD', 'id': 8799268650, 'side': 'Sell', 'size': 58591, 'price': 7313.5}, {'symbol': 'XBTUSD', 'id': 8799268700, 'side': 'Sell', 'size': 11588, 'price': 7313}, {'symbol': 'XBTUSD', 'id': 8799268750, 'side': 'Sell', 'size': 25065, 'price': 7312.5}, {'symbol': 'XBTUSD', 'id': 8799268800, 'side': 'Sell', 'size': 35865, 'price': 7312}, {'symbol': 'XBTUSD', 'id': 8799268850, 'side': 'Sell', 'size': 3582, 'price': 7311.5}, {'symbol': 'XBTUSD', 'id': 8799268900, 'side': 'Sell', 'size': 34766, 'price': 7311}, {'symbol': 'XBTUSD', 'id': 8799268950, 'side': 'Sell', 'size': 9545, 'price': 7310.5}, {'symbol': 'XBTUSD', 'id': 8799269000, 'side': 'Sell', 'size': 181298, 'price': 7310}, {'symbol': 'XBTUSD', 'id': 8799269050, 'side': 'Sell', 'size': 10825, 'price': 7309.5}, {'symbol': 'XBTUSD', 'id': 8799269100, 'side': 'Sell', 'size': 43241, 'price': 7309}, {'symbol': 'XBTUSD', 'id': 8799269150, 'side': 'Buy', 'size': 508403, 'price': 7308.5}, {'symbol': 'XBTUSD', 'id': 8799269200, 'side': 'Buy', 'size': 3346727, 'price': 7308}, {'symbol': 'XBTUSD', 'id': 8799269250, 'side': 'Buy', 'size': 960500, 'price': 7307.5}, {'symbol': 'XBTUSD', 'id': 8799269300, 'side': 'Buy', 'size': 1247224, 'price': 7307}, {'symbol': 'XBTUSD', 'id': 8799269350, 'side': 'Buy', 'size': 1173805, 'price': 7306.5}, {'symbol': 'XBTUSD', 'id': 8799269400, 'side': 'Buy', 'size': 1319868, 'price': 7306}, {'symbol': 'XBTUSD', 'id': 8799269450, 'side': 'Buy', 'size': 2440584, 'price': 7305.5}, {'symbol': 'XBTUSD', 'id': 8799269500, 'side': 'Buy', 'size': 659271, 'price': 7305}, {'symbol': 'XBTUSD', 'id': 8799269550, 'side': 'Buy', 'size': 13754, 'price': 7304.5}, {'symbol': 'XBTUSD', 'id': 8799269600, 'side': 'Buy', 'size': 29064, 'price': 7304}]

MOCK_DATA_BITFINEX = {'bids': [{'price': '7318.1', 'amount': '1.99594096', 'timestamp': '1586437116.0'}, {'price': '7316.5', 'amount': '0.00502619', 'timestamp': '1586437116.0'}, {'price': '7316.4', 'amount': '1.17598668', 'timestamp': '1586437116.0'}, {'price': '7316.1', 'amount': '1.48026776', 'timestamp': '1586437116.0'}, {'price': '7316', 'amount': '1.09415175', 'timestamp': '1586437116.0'}, {'price': '7315.8', 'amount': '0.02281846', 'timestamp': '1586437116.0'}, {'price': '7314.6', 'amount': '0.2', 'timestamp': '1586437116.0'}, {'price': '7314.5', 'amount': '0.3', 'timestamp': '1586437116.0'}, {'price': '7314.4', 'amount': '0.2', 'timestamp': '1586437116.0'}, {'price': '7314.1', 'amount': '0.5335', 'timestamp': '1586437116.0'}], 'asks': [{'price': '7319.9', 'amount': '0.70000001', 'timestamp': '1586437116.0'}, {'price': '7320', 'amount': '38.9641025', 'timestamp': '1586437116.0'}, {'price': '7320.1', 'amount': '0.015', 'timestamp': '1586437116.0'}, {'price': '7321', 'amount': '0.0006', 'timestamp': '1586437116.0'}, {'price': '7321.2', 'amount': '1.18151', 'timestamp': '1586437116.0'}, {'price': '7321.7', 'amount': '0.84', 'timestamp': '1586437116.0'}, {'price': '7322', 'amount': '0.3666', 'timestamp': '1586437116.0'}, {'price': '7322.9', 'amount': '0.10885634', 'timestamp': '1586437116.0'}, {'price': '7323', 'amount': '0.0006', 'timestamp': '1586437116.0'}, {'price': '7323.4', 'amount': '0.34146', 'timestamp': '1586437116.0'}]}

MOCK_DATA_AMOUNT = 0.0001 # BTCK

URL_BITFINEX = "https://api.bitfinex.com/v1/book/btcusd"
URL_BITMEX = "https://www.bitmex.com/api/v1/orderBook/L2"


def callAPI(url):
    response = requests.request("GET", url)
    #_jsonDoc = json.loads(response.text)
    if response.status_code == requests.codes.ok:
        return response.json()

    return None
    # return _jsonDoc


if __name__ == '__main__':

    # affichage des 10 dernières offres

    limit = 10
    print(f"{limit} dernières offres achat/vente chez Bitfinex")
    # bitfinex
    __json = MOCK_DATA_BITFINEX
    __json = callAPI(f"{URL_BITFINEX}?limit_bids={limit}&limit_asks={limit}")
    #print(__json)
    print("Ventes :")
    for data in __json["asks"]:
        _datetime = datetime.fromtimestamp(int(float(data['timestamp'])))
        print(f'\t{_datetime.strftime("%A %d %b %Y, %H:%M:%S")}\tQté : {data["amount"]}\tprix : {data["price"]}')

    print("Achats :")
    for data in __json["bids"]:
        _datetime = datetime.fromtimestamp(int(float(data['timestamp'])))
        print(f'\t{_datetime.strftime("%A %d %b %Y, %H:%M:%S")}\tQté : {data["amount"]}\tprix : {data["price"]}')

    print("10 dernières offres achat/vente chez bitMEX")
    # bitMEX
    __json = MOCK_DATA_BITMEX
    __json = callAPI(f"{URL_BITMEX}?symbol=XBT&depth={limit}")
    #print(__json)

    print("Ventes :")
    for data in __json:
        if data['side'] == 'Sell':
            qty = float(data['price']) / float(data["size"])
            print(f'\tprix : {data["price"]}\tQté : {str(qty)}')
            #print(str(data))

    print("Achats :")
    for data in __json:
        if data['side'] == 'Buy':
            qty = float(data['price']) / float(data["size"])
            print(f'\tprix : {data["price"]}\tQté : {str(qty)}')
            #print(str(data))

    MOCK_DATA_AMOUNT = 5.0

    print(f"Nombre de Bitcoin à acheter : {MOCK_DATA_AMOUNT} BTC")

    print("Calcul pour BitFinex :")
    limit=0
    solutionFound = False
    while not solutionFound:
        limit += 5
        __json = callAPI(f"{URL_BITFINEX}?limit_bids={limit}&limit_asks={limit}")
        #print(__json)

        totalPriceBitFinex = 0.0 # Montant total estimé de l'achat
        totalBTC = MOCK_DATA_AMOUNT  # Nb de Bitcoins restant à acheter
        for data in __json["asks"]:

            if float(data["amount"]) > totalBTC:
                totalPriceBitFinex += totalBTC * float(data["price"])
                totalBTC = 0
                break

            totalPriceBitFinex += float(data["amount"]) * float(data["price"])
            totalBTC -= float(data["amount"])
        print(f"Qté restante : {totalBTC}")
        solutionFound = totalBTC == 0



    print("\nCalcul pour bitMEX :")
    limit=0
    solutionFound = False
    while not solutionFound:
        limit += 5

        __json = callAPI(f"{URL_BITMEX}?symbol=XBT&depth={limit}")
        #print(__json)

        totalPriceBitMEX = 0.0 # Montant total estimé de l'achat
        totalBTC = MOCK_DATA_AMOUNT  # Nb de Bitcoins restant à acheter
        dataToSort = []
        for data in __json:
            if data['side'] == 'Sell':
                qty = float(data['price']) / float(data["size"])
                dataToSort.append((float(data["price"]),qty))
                #print(f'\tprix : {data["price"]}\tQté : {str(qty)}')
                #print(str(data))
        # tri des ordres de ventes par prix croissant
        sorteddata = sorted(dataToSort, key=lambda data: data[0])
        for data in sorteddata:
            #print(f'\tprix : {data[0]}\tQté : {data[1]}')
            if float(data[1]) > totalBTC:
                totalPriceBitMEX += totalBTC * float(data[0])
                totalBTC = 0
                break
            totalPriceBitMEX += float(data[1]) * float(data[0])
            totalBTC -= float(data[1])
        print(f"Qté restante : {totalBTC}")
        solutionFound = totalBTC == 0

    print("\nRésultats : ")
    print(f"bitFinex, Qté achetée : {MOCK_DATA_AMOUNT}, montant total : {totalPriceBitFinex} USD")
    print(f"bitMEX, Qté achetée : {MOCK_DATA_AMOUNT}, montant total : {totalPriceBitMEX} USD")


    MOCK_DATA_AMOUNT = 20000 # 20 USD

    print(f"\n\nMontant à dépenser : {MOCK_DATA_AMOUNT} USD")

    print("Calcul pour BitFinex :")
    limit=0
    solutionFound = False
    while not solutionFound:
        limit += 5
        __json = callAPI(f"{URL_BITFINEX}?limit_bids={limit}&limit_asks={limit}")
        #print(__json)

        totalBitcoinBitFinex = 0.0 # Nb de Bitcoins achetés
        totalUSD = MOCK_DATA_AMOUNT  # Nb de USD restant à dépenser
        for data in __json["asks"]:
            qty = float(data["amount"])
            price = float(data["price"])
            amount = qty * price

            if totalUSD <= amount:
                totalBitcoinBitFinex += (totalUSD / price)
                totalUSD = 0
                break

            totalBitcoinBitFinex += qty
            totalUSD -= amount

        print(f"Montant restant : {totalUSD}")
        solutionFound = totalUSD == 0



    print("\nCalcul pour bitMEX :")
    limit=0
    solutionFound = False
    while not solutionFound:
        limit += 5

        __json = callAPI(f"{URL_BITMEX}?symbol=XBT&depth={limit}")

        totalBitcoinBitMEX = 0.0 # Nb de Bitcoins achetés
        totalUSD = MOCK_DATA_AMOUNT  # Nb de USD restant à dépenser
        dataToSort = []
        for data in __json:
            if data['side'] == 'Sell':
                qty = float(data['price']) / float(data["size"])
                dataToSort.append((float(data["price"]),qty))

        # tri des ordres de ventes par prix croissant
        sorteddata = sorted(dataToSort, key=lambda data: data[0])
        for data in sorteddata:
            qty = float(data[1])
            price = float(data[0])
            amount = qty * price

            if totalUSD <= amount:
                totalBitcoinBitMEX += (totalUSD / price)
                totalUSD = 0
                break

            totalBitcoinBitMEX += qty
            totalUSD -= amount

            print(f"Montant restant : {totalUSD}")
        solutionFound = totalUSD == 0

    print("\nRésultats : ")
    print(f"bitFinex, nb de Bitcoins achetés : {totalBitcoinBitFinex}, avec : {MOCK_DATA_AMOUNT} USD")
    print(f"bitMEX, nb de Bitcoins achetés : {totalBitcoinBitMEX}, avec : {MOCK_DATA_AMOUNT} USD")
