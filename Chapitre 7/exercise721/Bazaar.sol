pragma solidity >0.6.0;

import "./MagicItem.sol";

contract Bazaar {

    struct Bid {
         address bestBuyer;
         uint256 bestAuction;
         uint256 timeLimit;
         uint256 tockenId;
         address seller;
    }
    
    Bid[] private bids;
    
    address public owner;
    MagicItem private magicItem;
    
    constructor(address magicItemContract) public {
		owner = msg.sender;
		magicItem = MagicItem(magicItemContract);
    }
    
    function sell(uint _tokenId) public {
        require(magicItem.exists(_tokenId),"Cet objet n'existe pas.");
        // Le SC doit être proopriétaire de l'objet avant toute opération
        require(magicItem.ownerOf(_tokenId) == address(this),"Vous devez me transférer la propriété de l'objet avant de le vendre");
        
        Bid memory bid = Bid(address(0x0),0,now + 1 * (1 days),_tokenId,msg.sender);
        bids.push(bid);
    }
    
}

