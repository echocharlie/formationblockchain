# -*- coding: utf8 -*-


"""

    Exercice 7.1.3 Créer le dashboard d’un jeton (Python 3)
    https://ecole.alyra.fr/mod/assign/view.php?id=253


"""


import requests
from datetime import datetime

URL_MESSARI = "https://data.messari.io/api/v1/assets/{0}/metrics"


def callAPI(url):
    response = requests.request("GET", url)
    if response.status_code == requests.codes.ok:
        return response.json()

    return None


if __name__ == '__main__':
    token = "bat"

    __json = callAPI(URL_MESSARI.format(token))
    #print(__json)

    all_time_high_price = __json["data"]["all_time_high"]["price"]
    price_usd = __json["data"]["market_data"]["price_usd"]

    y_2050 = __json["data"]["supply"]["y_2050"]
    current = __json["data"]["supply"]["circulating"]
    print(f"Prix actuel : {price_usd}, prix max : {all_time_high_price}, nombre total de jetons : {y_2050}, nombre de jetons en circulation : {current} {(current/ y_2050) * 100.0}%")
