const {
  BN,           // Big Number support
  balance,
  time,
  ether,
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');

const { expect } = require('chai');

const MagicItem = artifacts.require('MagicItem');
const Bazaar = artifacts.require('Bazaar');

contract('Bazaar & MagicItem', function (accounts) {
 const _name = 'Bazar';
 const owner = accounts[0];

 async function getBalance(from) {
    if (from.tracker != undefined) {
        const r = await from.tracker.get();
        return r;
    } else {
        return -1;
    }
 }

// cf https://github.com/ethereum/wiki/wiki/JavaScript-API#web3ethgettransactionreceipt
 function dumpTxInfos(result) {
    console.log(result);
 }


   before(async function () {
	 
	 this.MagicItemInstance = await MagicItem.new({from: owner});
     console.log("Created MagicItemInstance at "+this.MagicItemInstance.address);

	 this.BazaarInstance = await Bazaar.new(this.MagicItemInstance.address, {from: owner});
     console.log("Created BazaarInstance at "+this.BazaarInstance.address);
	 
     console.log(Bazaar.defaults());
     console.log(MagicItem.defaults());
	 
	 this.owerOfContracts = Bazaar.defaults().from;
	 
     this.trackerContract = await balance.tracker(this.BazaarInstance.address);
  });

  beforeEach(async function () {
    console.log("\n");
 });

  afterEach(async function () {
 });

  after(async function () {
  console.log("\nTESTS END");
 });

 it('has a name', async function () {
   expect(await this.BazaarInstance.name()).to.equal(_name);
 });
 
 it('has ownership of sample items', async function () {
   expect(await this.MagicItemInstance.ownerOf(1150)).to.equal(this.owerOfContracts);
   expect(await this.MagicItemInstance.ownerOf(2170)).to.equal(this.owerOfContracts);
   expect(await this.MagicItemInstance.ownerOf(3170)).to.equal(this.owerOfContracts);
 });
 
 it('exists item 1150', async function () {
   let exists = await this.MagicItemInstance.exists(1150);
   expect(exists).to.equal(true);
 });

it('does not exist item 7777', async function () {
   let exists = await this.MagicItemInstance.exists(7777);
   expect(exists).to.equal(false);
 });

 it('owns 3 items', async function () {
   let itemCount = await this.MagicItemInstance.balanceOf(this.owerOfContracts);
   expect(itemCount).to.be.bignumber.equal('3');
 });
 
 it('transfers object 1 from contract owner to account 1', async function () {
   const tx = await this.MagicItemInstance.transferFrom(this.owerOfContracts, accounts[1],1150);
   expectEvent(tx, 'Transfer', { _from: this.owerOfContracts, _to: accounts[1], _tokenId: new BN('1150') });
 });
 
 it('transfers object 2 from contract owner to account 2', async function () {
   const tx = await this.MagicItemInstance.transferFrom(this.owerOfContracts, accounts[2],2170);
   expectEvent(tx, 'Transfer', { _from: this.owerOfContracts, _to: accounts[2], _tokenId: new BN('2170') });
 });
 
 it('fails to transfer object 3 from contract owner to account 3', async function () {
   await expectRevert(this.MagicItemInstance.transferFrom(this.owerOfContracts, accounts[3],3170),"Les objet tres rares ne sont pas transferables");
 });

 it('contract owner owns sample item', async function () {
   let ownerAddress = await this.MagicItemInstance.ownerOf(3170);
   expect(ownerAddress).to.equal(this.owerOfContracts);
 });
 
 it('account 4 buys an item with insuficient funds', async function () {
   await expectRevert(this.MagicItemInstance.buyItem({ from: accounts[4], value: '100000000000000' }),"Le prix d'un objet est de 0.1 ETH");  // 0.0001 ETH
 });

 it('account 4 buys an item', async function () {
   await expect(this.MagicItemInstance.buyItem({ from: accounts[4], value: '100000000000000000' })); // 0.1 ETH
 });
 
 it('account 4 owns 1 item', async function () {
   let itemCount = await this.MagicItemInstance.balanceOf(accounts[4]);
   expect(itemCount).to.be.bignumber.equal('1');
 });
 
 
 // VENTE D'UN OBJET
 it('account 4 cannot sell an item that does not exist', async function () {
   await expectRevert(this.BazaarInstance.sell(7777, { from: accounts[4] }),"Cet objet n'existe pas");  
 });

 it('account 2 must transfer item to Bazaar contract before selling it', async function () {
   await expectRevert(this.BazaarInstance.sell(2170, { from: accounts[2] }),"Vous devez me transferer la propriete de l'objet avant de le vendre");  
 });

 it('transfers object 2 from account 2 to the Bazaar', async function () {
   const tx = await this.MagicItemInstance.transferFrom(accounts[2], this.BazaarInstance.address, 2170, {from: accounts[2]});
   expectEvent(tx, 'Transfer', { _from: accounts[2], _to: this.BazaarInstance.address, _tokenId: new BN('2170') });
 });
 
 it('account 2 sells Item 2', async function () {
   await expect(this.BazaarInstance.sell(2170,{ from: accounts[2] }));  
 });
 
 // FAIRE UNE OFFRE
 it('account 6 bids on a nonexistent sell', async function () {
   await expectRevert(this.BazaarInstance.bid(1, { from: accounts[6], value: 0 }),"Cette vente n'existe pas");
 });

 it('account 6 bids on an existent sell with a bad price', async function () {
   await expectRevert(this.BazaarInstance.bid(0, { from: accounts[6], value: 0 }),"Le prix n'est pas assez eleve");
 });
  
 it('account 6 bids on an existent sell with a good price', async function () {
   await expect(this.BazaarInstance.bid(0, { from: accounts[6], value: 100000000000000 })); // 0.0001 ETH
 });

 it('account 7 bids on an existent sell with a good price', async function () {
   await expect(this.BazaarInstance.bid(0, { from: accounts[7], value: 200000000000000 })); // 0.0002 ETH
 });

 it('account 8 bids on an expired sell', async function () {
	await time.increase(time.duration.days(2)); // avancer de 2 jours
    await expectRevert(this.BazaarInstance.bid(0, { from: accounts[7], value: 300000000000000 }),"Cette vente est terminee"); // 0.0003 ETH
 });
 
 it('account 9 tries to get is money back on an expired sell where he has no bid', async function () {
   await expectRevert(this.BazaarInstance.payback(0, { from: accounts[9] }),"Vous n'avez aucun remboursement pour cette vente"); 
 });

 it('account 6 get is money back on an expired sell where he has 1 bid', async function () {
   await expect(this.BazaarInstance.payback(0, { from: accounts[6] })); 
 });

 it('account 9 tries to collect an item on a sell he did not win', async function () {
   await expectRevert(this.BazaarInstance.collectItem(0, { from: accounts[9] }),"Vous n'avez pas gagne cette vente"); 
 });

 it('account 7 collects the winning item', async function () {
   await expect(this.BazaarInstance.collectItem(0, { from: accounts[7] })); 
 });

 it('account 7 owns sample item 2', async function () {
   let ownerAddress = await this.MagicItemInstance.ownerOf(2170);
   expect(ownerAddress).to.equal(accounts[7]);
 }); 

// Vente en mode enchères holandaises
it('transfers item 2 from account 7 to the Bazaar', async function () {
   const tx = await this.MagicItemInstance.transferFrom(accounts[7], this.BazaarInstance.address, 2170, {from: accounts[7]});
   expectEvent(tx, 'Transfer', { _from: accounts[7], _to: this.BazaarInstance.address, _tokenId: new BN('2170') });
 });
 
 it('account 7 sells Item 7 in Dutch Auction Mode', async function () {
   await expect(this.BazaarInstance.sellDutchAuction(2170, ether("1"), { from: accounts[7] }));   // 1 ETH
 });

 it('account 5 bids on Item 7 in Dutch Auction Mode', async function () {
    await time.increase(time.duration.hours(2));

    // Récupérer le prix actuel de l'objet
    let price = await this.BazaarInstance.getCurrentPrice(1, {from : accounts[7]});
    console.log("Current price : "+price);
    await expect(this.BazaarInstance.bid(1, { from: accounts[7], value: price }));

 });

 it('account 8 bids on Item 7 auction fails', async function () {
    await expectRevert(this.BazaarInstance.bid(1, { from: accounts[7], value: 300000000000000 }),"Cette vente est terminee"); // 0.0003 ETH
 });
  
});
