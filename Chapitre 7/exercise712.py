# -*- coding: utf8 -*-


"""

    Exercice Exercice 7.1.2 - Récupérer le prix du dernier échange (Python 3)
    https://ecole.alyra.fr/mod/assign/view.php?id=252


"""


import requests
from datetime import datetime

URL_BITFINEX = "https://api.bitfinex.com/v1/trades/btcusd"


def callAPI(url):
    response = requests.request("GET", url)
    if response.status_code == requests.codes.ok:
        return response.json()

    return None


if __name__ == '__main__':

    __json = callAPI(f"{URL_BITFINEX}")
    #print(__json)
    _datetime = datetime.fromtimestamp(int(float(__json[0]['timestamp'])))
    print (f'Dernière transaction : {_datetime.strftime("%A %d %b %Y, %H:%M:%S")} {__json[0]["type"]} {__json[0]["amount"]} au prix de {__json[0]["price"]} USD')
