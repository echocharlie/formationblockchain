const MagicItem = artifacts.require("./MagicItem");
const Bazaar = artifacts.require("./Bazaar");

module.exports = function(deployer) {
  
  deployer.deploy(MagicItem).then(function() {
    return deployer.deploy(Bazaar,MagicItem.address);
  });
  
};
