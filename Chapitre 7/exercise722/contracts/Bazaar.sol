pragma solidity ^0.5.0;

import "./MagicItem.sol";

contract Bazaar {
	string public name = "Bazar";
    
	struct Bid {
         address bestBuyer;
         uint256 bestAuction;
         uint256 timeLimit;
         uint256 tokenId;
         address seller;
    }
    
    Bid[] private bids;
    
    address public owner;
    MagicItem private magicItem;
    
    // Adresse de l'enchérisseur
    // Index de la vente
    // montant payé
    mapping(address => mapping(uint256 => uint256) ) private payments;
    
    constructor(address magicItemContract) public {
		owner = msg.sender;
		magicItem = MagicItem(magicItemContract);
    }
    
    function sell(uint _tokenId) public {
        require(magicItem.exists(_tokenId),"Cet objet n'existe pas");
        // Le SC doit être proopriétaire de l'objet avant toute opération
        require(magicItem.ownerOf(_tokenId) == address(this),"Vous devez me transferer la propriete de l'objet avant de le vendre");
        
        Bid memory bid = Bid(address(0x0),0,now + (1 * (1 days)),_tokenId,msg.sender);
        bids.push(bid);
    }
    
    function bid(uint bidIndex) public payable {
        require(bids.length > 0,"Aucun objet a vendre");
        require(bidIndex >= 0 && bidIndex < bids.length,"Cette vente n'existe pas");
        require(bids[bidIndex].timeLimit > now,"Cette vente est terminee");
        require(msg.value > bids[bidIndex].bestAuction,"Le prix n'est pas assez eleve");
        
        // Rembourser le précédent meilleur enchérisseur
        payments[bids[bidIndex].bestBuyer][bidIndex] = bids[bidIndex].bestAuction; 

        bids[bidIndex].bestAuction = msg.value;
        bids[bidIndex].bestBuyer = msg.sender;
    }
    
    // Remboursement
    function payback(uint bidIndex) public payable {
        require(bids.length > 0,"Aucun objet a vendre");
		require(bidIndex >= 0 && bidIndex < bids.length,"Cette vente n'existe pas");
        require(bids[bidIndex].timeLimit < now,"Cette vente n'est pas terminee");
        require(payments[msg.sender][bidIndex] > 0,"Vous n'avez aucun remboursement pour cette vente");
        
        (bool success, ) = msg.sender.call.value(payments[msg.sender][bidIndex])("");
	    require(success, "Le remboursement a échoué.");
	    payments[msg.sender][bidIndex] = 0;
    }
    
    // Le gagnat récupère son objet
    function collectItem(uint bidIndex) public {
        require(bids.length > 0,"Aucun objet a vendre");
		require(bidIndex >= 0 && bidIndex < bids.length,"Cette vente n'existe pas");
        require(bids[bidIndex].timeLimit < now,"Cette vente n'est pas terminee");
        require(bids[bidIndex].bestBuyer == msg.sender,"Vous n'avez pas gagne cette vente");
        
        magicItem.transferFrom(address(this), bids[bidIndex].bestBuyer, bids[bidIndex].tokenId);
    }

    
    
}