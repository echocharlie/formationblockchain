pragma solidity >0.6.0;
pragma experimental ABIEncoderV2;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";

/*
    Chaque objet (token) sera représenté par un nombre de quatre chiffres. 
        Le premier représente sa rareté : 
            0 pour un objet courant, 
            1 pour un objet rare, 
            2 pour un objet divin. 
        Le second chiffre représente le type d’objet : 
            0 pour une épée, 
            1 baguette… 
        Le 3ème est la puissance de l'objet de 0 à 9
        le quatrième chiffre représente le modèle
            0 Human
            1 Magician
            
*/

abstract contract ERC721Simple {
    event Transfer(address indexed _from, address indexed _to, uint256 _tokenId);

    function balanceOf(address _owner) public virtual view returns (uint256);
    function ownerOf(uint256 _tokenId) public virtual view returns (address);
    function exists(uint256 _tokenId) public virtual view returns (bool);
    function transferFrom(address _from, address _to, uint256 _tokenId) public virtual;
    
}

contract MagicItem is ERC721Simple {
    
    uint8 constant private RARITY_LOW = 1;
    uint8 constant private RARITY_MEDIUM = 2;
    uint8 constant private RARITY_HIGH = 3;
    
    uint8 constant private ITEM_TYPE_SWORD = 1;
    uint8 constant private ITEM_TYPE_CAPE = 2;
    uint8 constant private ITEM_TYPE_SHIELD = 3;
    uint8 constant private ITEM_TYPE_MAGIC_WAND = 4;

    uint8 constant private ITEM_MODEL_HUMAN = 1;
    uint8 constant private ITEM_MODEL_MAGICIAN = 2;
    
    uint16 constant private ITEM_SAMPLE_1 = 1150; // LOW / SWORD / POWER=5 / HUMAN
    uint16 constant private ITEM_SAMPLE_2 = 2170; // MEDIUM / SWORD / POWER=7 / HUMAN
    uint16 constant private ITEM_SAMPLE_3 = 3170; // HIGH / SWORD / POWER=9 / HUMAN
    
    uint256[] private itemCollection;

    address constant private ADDRESS_ZERO = address(0x0);
    string constant MSG_ERROR_BAD_ADDRESS = "Adresse invalide";
    string constant MSG_ERROR_ITEM_NOT_FOUND = "Cet objet n'existe pas";
    string constant MSG_ERROR_NOT_AUTHORIZED = "Vous n'êtes pas autorisé à appeler cette fonction";
    
    address public owner;
    
    // key : _tokenId
    // value : adresse du propriétaire
    mapping (uint256 => address) private items;
    
    // key : adresse d'un propriétaire
    // value : Nombre d'objets détenus
    mapping (address => uint256) private itemCount; 

	string public name = "Objets Magiques";
	
	constructor() public ERC721Simple() {
		owner = msg.sender;
		
		itemCollection.push(ITEM_SAMPLE_1);
		itemCollection.push(ITEM_SAMPLE_2);
		itemCollection.push(ITEM_SAMPLE_3);
		
		// Les 3 objets sont affecté au propriétaire du SC
        setOwner(owner, ITEM_SAMPLE_1);
        setOwner(owner, ITEM_SAMPLE_2);
        setOwner(owner, ITEM_SAMPLE_3);
	}
	

	function setOwner(address _owner, uint256 _tokenId) private {
	    
	    if (exists(_tokenId)) {
	        items[_tokenId] = _owner;
	        itemCount[_owner]++;
	    }
	    
	}
	
    function balanceOf(address _owner) public override  view returns (uint256) {
        require(_owner != ADDRESS_ZERO,MSG_ERROR_BAD_ADDRESS);
        return itemCount[_owner];
    }
    
    function ownerOf(uint256 _tokenId) public override  view returns (address) {
        require(items[_tokenId] != ADDRESS_ZERO, MSG_ERROR_ITEM_NOT_FOUND);
        return items[_tokenId];
    }
    
    function exists(uint256 _tokenId) public override  view returns (bool) {
        //return items[_tokenId] != ADDRESS_ZERO;
        
        bool found = false;
        uint i = 0;
        while (i < itemCollection.length && ! found) {
            found = (itemCollection[i] == _tokenId);
            i++;
        }
        
        return found;
    }
    
    function transferFrom(address _from, address _to, uint256 _tokenId) public override  {
        require(_from != ADDRESS_ZERO,MSG_ERROR_BAD_ADDRESS);
        require(_to != ADDRESS_ZERO,MSG_ERROR_BAD_ADDRESS);
        require(items[_tokenId] != ADDRESS_ZERO,MSG_ERROR_ITEM_NOT_FOUND);
        require(items[_tokenId] == _from,"Cet objet n'appartient pas à cette adresse");
        require(msg.sender == owner, MSG_ERROR_NOT_AUTHORIZED);
        
        // les objets très rares (HIGH) ne sont pas transférables
        require(_tokenId < 3000,"Les objtes très rares ne sont pas transférables");

        setOwner(_to, _tokenId);
        itemCount[_from]--;
    }
    
    // Creuser()
    function buyItem() public payable returns (uint256) {
        require(msg.value == 0.1 ether,"Le prix d'un objet est de 0.1 ETH");
        
        uint newItem = createItem();
        setOwner(msg.sender, newItem);
        
        
        return newItem;
    }
    
    function toBytes(uint256 x) private pure returns (bytes memory b) {
        b = new bytes(32);
        assembly { mstore(add(b, 32), x) }
    }
    
    function random(uint _max) private view returns (uint256) { 
        uint sum = SafeMath.add(block.timestamp, block.difficulty); 
        sum = SafeMath.mod(sum, 23); 
        bytes memory _sum = toBytes(sum); 
        bytes32 b=keccak256(abi.encodePacked(_sum)); 
        return uint8(uint(b)) % _max; 
    }
    
    function useItem(uint _tokenId) public returns (uint256) {
        require(exists(_tokenId),MSG_ERROR_ITEM_NOT_FOUND);
        require(items[_tokenId] == msg.sender,"Vous ne possedez pas cet objet");
        
        uint r = random(11);
        if (r == 0) {
            items[_tokenId] = ADDRESS_ZERO;
            itemCount[msg.sender]--;
            
            // Suppression de l'objet de la collection
            uint index = 0;
            bool found = false;
            while (! found) {
                if (itemCollection[index] == _tokenId) found = true;
                else index++;
            }
            if (found) {
                // Copie du dernier élément du tableau à la place de clui qu'il faut supprimer
                itemCollection[index] = itemCollection[itemCollection.length-1];
                // Suppression du dernier élément du tableau
                delete itemCollection[itemCollection.length-1];
            }        
        }
        
        return r;
    }
    
    function createItem() private returns (uint256) {
        
        bool itemAdded = false;
        uint256 r;
        while (! itemAdded) {
            uint rarety = random(3) + 1;
            uint typeItem = random(4) + 1;
            uint model = random(2) + 1;
            uint power = random(9) + 1;
            
            r = rarety * 1000 + typeItem * 100 + power * 10 + model;
            if (! exists(r)) {
                // Ajout de l'objet dans la collection
                itemCollection.push(r);

                itemAdded = true;
            }
        }
        
        return r;
    }
}