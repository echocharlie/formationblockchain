# -*- coding: utf8 -*-


"""
    Exercice 1.2.2 : Obtenir les meilleurs pourboires

"""
import math

# taille de la transaction, pourboire
transacs = [(2000,13000),(6000,9000),(800,2000),(700,1500),(1200,3500),(1000,2800),(1300,5000),(600,1500)]
#transacs = [(2000,13000),(6000,9000),(800,2000),(700,1500)]
MAX_BLOC_SIZE = 6000


class Bloc:

    def __init__(self):
        self._size = 0
        self._tip = 0
        self._transacs = []

    def addTransaction(self, taille, pourboire):
        if self._size + taille <= MAX_BLOC_SIZE:
            self._transacs.append((taille, pourboire))
            self._tip += pourboire
            self._size += taille

    def get_size(self):
        return self._size

    def get_tip(self):
        return self._tip

    def get_transacs(self):
        return self._transacs


def brut_force():
    best_bloc=None
    for i in range(1,int(math.pow(2, len(transacs)))):
        combination = "{0:b}".format(i).zfill(8)
        #print(f"Evaluation de la combinaison {combination}")
        bloc = Bloc()
        for i in range(len(combination)):
            if combination[i] == "1":
                bloc.addTransaction(transacs[i][0], transacs[i][1])
            if best_bloc is None or bloc.get_tip() > best_bloc.get_tip():
                best_bloc = bloc
    print(f"Plus gros pourboire trouvé:{best_bloc.get_tip()}")
    print("Transactions du bloc : ")
    size=0
    for t in best_bloc.get_transacs():
        print(f"taille : {t[0]} - pourboire {t[1]}")
        size+=t[0]
        # recherche du pourboire
    print(f"Taille totale du bloc : {size} ")

def ratio_way():
    trasacs_with_ratio=[]
    for t in transacs:
        ratio = t[1] / t[0]
        print(f"{t[0]}:{t[1]}   ratio:{ratio}")
        t = (t[0],t[1],ratio)
        trasacs_with_ratio.append(t)
    trasacs_with_ratio.sort(key=lambda tup: tup[2], reverse=True)
    size=0
    tip = 0
    for r in trasacs_with_ratio:
        print(f"Tentative d'ajout de la transaction {r}")
        if size + r[0] <= MAX_BLOC_SIZE:
            print("\tBloc ajouté")
            size+=r[0]
            tip+=r[1]
        else:
            print("\tImpossible d'ajouter ce bloc, taille maximale dépassée")
    print(f"taille du bloc {size}")
    print(f"Montant total du pourboire {tip}")


if __name__ == '__main__':

    # tri croissant  des transactions par leur porboire
    transacs.sort(key=lambda tup: tup[1])
    for t in transacs:
        print(f"transaction {t[0]}:{t[1]}")

    """
    1ère méthode brutale, toutes les combinaisons sont déterminées, légèrement optimisées pour ensuite
    être parcourues et déterminer la meilleur combinaison
    """
    brut_force()

    """
    2ème aproche en calculant le ratio pourboire / taille de chaque transacton et en déterminer les meilleures
    """
    ratio_way()
