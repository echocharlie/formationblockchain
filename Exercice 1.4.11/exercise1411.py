# -*- coding: utf8 -*-


"""

    Exercice 1.4.11 : Réaliser une preuve de travail (PoW) naïve
    https://ecole.alyra.fr/mod/assign/view.php?id=74
    https://forum.alyra.fr/t/exercice-1-4-11-realiser-une-preuve-de-travail-pow-naive/94

"""

import random
import time

def chaineAlea(length):
    result = ""

    min = ord("A")
    max = ord("Z")
    for i in range(length):
        random_char = chr(random.randint(min,max))
        result += random_char

    return result

def rechercheDebut(goal, length):
    result = ""
    count = 0
    while True:
        count+=1
        random_str = chaineAlea(length)

        if random_str[:len(goal)] == goal:
            result = random_str
            break

    return result,count

if __name__ == '__main__':

    for i in range(10):
        print (chaineAlea(8))

    print(rechercheDebut("AA",8))
    goal = ""
    for i in range(1,20):
        goal = "A"*i
        print("goal ",goal)
        for j in range(i+1,20):
            now = time.time()
            print(rechercheDebut(goal, j), time.time() - now," secs")


