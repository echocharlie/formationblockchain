pragma solidity >0.5.8;
pragma experimental ABIEncoderV2;

contract MarketPlace {
	uint constant public FEE_RATE = 2; // 2% de frais

    address public owner;

	enum State {
		Open,
		Closed,
		InProgress
	}

	struct Illustrator {
		string name;
		address addr;
		uint cred;

		uint totalAmount; // total des gains de l'illustrateur
		uint totalWithdraw; // Montant total retiré par l'lllustrateur sur ses gains
}
	uint public illustratorCount;

	struct Company {
		string name;
		address addr;
	}

	struct JobRequest {
		uint id;
		address company;
		uint amount; // in wei
		uint timeLimit; // Délai en secondes
		string description;
		State state;
		uint credMin; // Crédibilité minimale pour postuler

		address illustrator; // Illustrateur sélectionné
		bytes32 hash; // Hash de l'URL de la livraison
	}

	// Propositions faites par les illustrateurs pour 1 jobRequest
	// Clef: index du jobRequest
	// Value : Liste des illustrateurs ayant proposés de réaliser le job
	mapping(uint => Illustrator[]) offers;

	string public name = "MarketPlace for illustrators";

	// Illustrators
	mapping (address => Illustrator) public illustrators;

	// Companies
	mapping (address => Company) public companies;

	JobRequest[] public jobRequests;

	constructor() public {
		owner = msg.sender;
	}

	uint public  totalPayableAmount;

	event NewJobRequest(address company, uint amount, uint timelimit, string description, State state, uint credMin);

	function submitJobRequest(string memory _description, uint _amount, uint _timeLimit, uint _cred) public payable {
		require(_cred > 0, "La crédibilité doit être supérieure à 0");
		require(_amount > 0 wei,"Le montant doit être suppérieur à 0");
		require(companies[msg.sender].addr != address(0x00),"Vous n'êtes pas enregistré");
		uint expectedValue = _amount * (1 + FEE_RATE / 100);
		require(msg.value >= expectedValue, "Montant payé insuffisant");
		uint newId=jobRequests.length + 1;
		JobRequest memory job = JobRequest({id: newId, company: msg.sender, amount : _amount, timeLimit: _timeLimit, description: _description, state:State.Open, credMin: _cred, illustrator: address(0x0), hash: bytes32("0x0")});
		jobRequests.push(job);

		totalPayableAmount += msg.value;
		emit NewJobRequest(job.company, job.amount, job.timeLimit, job.description, job.state, job.credMin);
	}

	// Postuler à une JobRequest
	function makeOffer(uint jobRequestIndex) public {
		require(illustrators[msg.sender].cred > 0,"Vous n'êtes pas enregistré.");
		require(jobRequestIndex >= 0 && jobRequestIndex < jobRequests.length, "Cette demande n'existe pas");
		require(jobRequests[jobRequestIndex].state == State.Open, "Les candidatures sont closes pour cette demande.");
		require(illustrators[msg.sender].cred >= jobRequests[jobRequestIndex].credMin,"Vous ne disposez pas de la crédibilité suffisante pour postuler.");

		offers[jobRequestIndex].push(illustrators[msg.sender]);
	}

	// Une société accepte une offre
	function acceptOffer(uint jobRequestIndex, address illustrator) public {
		require(companies[msg.sender].addr != address(0x0),"Vous n'êtes pas enregistré.");
		require(illustrators[illustrator].addr != address(0x0),"Illustrateur inconnu");
		require(jobRequestIndex >= 0 && jobRequestIndex < jobRequests.length, "Cette demande n'existe pas");
		require(jobRequests[jobRequestIndex].state == State.Open,"Cette demande est déjà acceptée.");

		Illustrator[] memory ill = offers[jobRequestIndex];
		bool found = false;
		uint i=0;
		while (! found) {
			found = ill[i].addr == illustrator;
			i++;
		}
		require(found, "Cet illustrateur n'a pas fait d'offre pour cette demande.");

		JobRequest storage jr = jobRequests[jobRequestIndex];
		jr.illustrator = illustrator;
		jr.state = State.InProgress;

	}

	// Livraison du job par un illustrateur
	function deliver(uint jobRequestIndex, bytes32 hash) public {
		require(illustrators[msg.sender].addr != address(0x0),"Illustrateur inconnu");
		require(jobRequestIndex >= 0 && jobRequestIndex < jobRequests.length, "Cette demande n'existe pas");
		require(jobRequests[jobRequestIndex].state == State.InProgress,"Cette demande n'est pas en cours de réalisation.");
		require(jobRequests[jobRequestIndex].illustrator == msg.sender, "Vous n'êtes pas le candidat retenu pour cette offre.");

		// Enregistrer le hash
		jobRequests[jobRequestIndex].hash = hash;

		// Clore la demande
		jobRequests[jobRequestIndex].state = State.Closed;

		// Débloquer les fonds au profit de l'illustrateur
		illustrators[msg.sender].totalAmount += jobRequests[jobRequestIndex].amount;

		// Augmenter la crédibilité de l'illustrateur
		illustrators[msg.sender].cred += 1;
	}

	// fonction de retrait de ses gains par un illustrateur
	function withdraw() public payable {
		require(illustrators[msg.sender].addr != address(0x0),"Illustrateur inconnu");

		uint amount = illustrators[msg.sender].totalAmount - illustrators[msg.sender].totalWithdraw;
		require(amount > 0, "Vous n'avez aucun gain à retirer");

		illustrators[msg.sender].totalWithdraw += amount;
		totalPayableAmount -= amount;
		//msg.sender.transfer(illustrators[msg.sender].totalAmount);

		(bool success, ) = msg.sender.call.value(amount)("");
		require(success, "Transfer failed.");
	}

	function getOpenJobRequests() public view returns (JobRequest[] memory) {
		JobRequest[] memory jrs = new JobRequest[](jobRequests.length);
		for (uint i=0; i < jobRequests.length; i++) {
			jrs[i] = jobRequests[i];
		}
		return jrs;
	}

	function getJobRequestInfos(uint index) public view returns (JobRequest memory) {
		require(index >= 0  && index < jobRequests.length,"Ce travail n'existe pas");
		return jobRequests[index];
	}

	// Enregistrement d'un illustrator
	function registerIllustrator(string memory username) public {
		require(illustrators[msg.sender].cred == 0,"Vous êtes déjà enregistré.");

		Illustrator memory newUser = Illustrator(username, msg.sender, 1, 0, 0);
		illustrators[msg.sender] = newUser;
		illustratorCount++;
	}

	// Enregistrement d'un illustrator
	function registerCompany(string memory companyName) public {
		require(companies[msg.sender].addr == address(0x0), "Vous êtes déjà enregistré.");

		Company memory newCompany = Company(companyName, msg.sender);
		companies[msg.sender] = newCompany;
	}

	function getIllustratorInfos() public view returns (Illustrator memory) {
		require(illustrators[msg.sender].cred > 0, "You are not registered.");
		return illustrators[msg.sender];
	}

	function getCompanyInfos() public view returns (Company memory) {
		require(companies[msg.sender].addr != address(0x0), "You are not registered.");
		return companies[msg.sender];
	}

	// fallback
	function () external payable {}
}