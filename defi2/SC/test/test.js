const {
  BN,           // Big Number support
  balance,
  time,
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');

const { expect } = require('chai');

const MarketPlace = artifacts.require('MarketPlace');

contract('MarketPlace', function (accounts) {
 const _name = 'MarketPlace for illustrators';
 const owner = accounts[0];
 const illustrator1 = {
    addr : accounts[1],
    name : "Emmanuel",
    initialBalance: 0,
    gasUsed: 0
 };
 const illustrator2 = {
    addr : accounts[2],
    name : "Pierre",
    initialBalance: 0,
    gasUsed: 0
 };
 const illustrator3 = {
     addr : accounts[3],
     name : "Nathalie",
    initialBalance: 0,
     gasUsed: 0
  };
  const illustrator4 = {
     addr : accounts[4],
     name : "Unregistered User",
    initialBalance: 0,
     gasUsed: 0
  };
const company1 = {
    addr : accounts[5],
    name : "ACME Company",
    initialBalance: 0,
    gasUsed:0,
 };
 const company2 = {
    addr : accounts[6],
    name : "Toon Factory",
    initialBalance: 0,
    gasUsed: 0
 };
 const company3 = {
    addr : accounts[7],
    name : "Dummy",
    initialBalance: 0,
    gasUsed: 0
 };

 let illustrators = [illustrator1, illustrator2, illustrator3, illustrator4];
 let companies = [company1, company2, company3];

 async function getBalance(from) {
    if (from.tracker != undefined) {
        const r = await from.tracker.get();
        return r;
    } else {
        return -1;
    }
 }

// cf https://github.com/ethereum/wiki/wiki/JavaScript-API#web3ethgettransactionreceipt
 function dumpTxInfos(result) {
    console.log(result);
    /*
      console.log("tx:"+result.tx);
      console.log("from:"+result.receipt.from);
      console.log("status:"+result.receipt.status);
      console.log("logs:"+result.logs);
      console.log("gas used:"+result.receipt.gasUsed);
      */
 }


   before(async function () {
     let estimatedGas = await MarketPlace.new.estimateGas();
     console.log("Estimated gas for deployment : "+estimatedGas);
     this.MarketPlaceInstance = await MarketPlace.new({from: owner});

     // Référence : https://github.com/trufflesuite/truffle/tree/master/packages/contract
     // Permet de récupérer certaines valeurs de configuration du contrat dans le réseau. Cf truffle-config.js
     // from, gas et gasPrice

     console.log(MarketPlace.defaults());
     // il est possible de modifier ces valeurs en appelant MarketPlace.defaults({ gas: 100à000 ]);
     console.log("SC loaded at address : "+this.MarketPlaceInstance.address);

     this.trackerContract = await balance.tracker(this.MarketPlaceInstance.address);
  });

  beforeEach(async function () {
    console.log("\n");
    for (i=0; i<illustrators.length;i++) {
        console.log("BEFORE, "+illustrators[i].name+" balance : "+await getBalance(illustrators[i]));
    }
    for (i=0; i < companies.length;i++) {
        console.log("BEFORE, "+companies[i].name+" balance : "+await getBalance(companies[i]));
    }
 });

  afterEach(async function () {
    for (i=0; i<illustrators.length;i++) {
        console.log("AFTER, "+illustrators[i].name+" balance : "+await getBalance(illustrators[i]));
    }
    for (i=0; i < companies.length;i++) {
        console.log("AFTER, "+companies[i].name+" balance : "+await getBalance(companies[i]));
    }
 });

  after(async function () {
  console.log("\nTESTS END");
    for (i=0; i<illustrators.length;i++) {
        let currentBalance = await getBalance(illustrators[i]);
        let deltaBalance = illustrators[i].initialBalance - currentBalance;
        console.log(illustrators[i].name+" inital balance : "+illustrators[i].initialBalance+", balance : "+currentBalance+", delta : "+deltaBalance+", gas used:"+illustrators[i].gasUsed);
    }
    for (i=0; i < companies.length;i++) {
        let currentBalance = await getBalance(companies[i]);
        let deltaBalance = companies[i].initialBalance - currentBalance;
        console.log(companies[i].name+" initial balane : "+companies[i].initialBalance+", balance : "+currentBalance+", delta : "+deltaBalance+", gas used:"+companies[i].gasUsed);
    }
 });

 it('a un nom', async function () {
   expect(await this.MarketPlaceInstance.name()).to.equal(_name);
 });

 it('a un solde nul', async function () {
    const currentBalance = await this.trackerContract.get();
    console.log("SC balance :"+currentBalance);
    expect(currentBalance).to.be.bignumber.equal(new BN('0'));
 });

 it('vérifier l\'enregistrement des illustrateurs', async function () {

     let result = await this.MarketPlaceInstance.registerIllustrator(illustrator1.name, {from: illustrator1.addr});
     illustrator1.gasUsed += result.receipt.gasUsed;
     dumpTxInfos(result);
    // count est un attribut public du SC mais Solidity expose automatiquement une fonction pour y accéder
     let count= await this.MarketPlaceInstance.illustratorCount();
     expect(count).to.be.bignumber.equal(new BN('1'));

     result = await this.MarketPlaceInstance.registerIllustrator(illustrator2.name, {from: illustrator2.addr});
     illustrator2.gasUsed += result.receipt.gasUsed;
     dumpTxInfos(result);
     count= await this.MarketPlaceInstance.illustratorCount();
     expect(count).to.be.bignumber.equal(new BN('2'));

     result = await this.MarketPlaceInstance.registerIllustrator(illustrator3.name, {from: illustrator3.addr});
     illustrator3.gasUsed += result.receipt.gasUsed;
     dumpTxInfos(result);

     count= await this.MarketPlaceInstance.illustratorCount();
     expect(count).to.be.bignumber.equal(new BN('3'));

     let userInfos1 = await this.MarketPlaceInstance.getIllustratorInfos({from: illustrator1.addr});
     console.log(userInfos1);
     let userInfos2 = await this.MarketPlaceInstance.getIllustratorInfos({from: illustrator2.addr});
     console.log(userInfos2);
     let userInfos3 = await this.MarketPlaceInstance.getIllustratorInfos({from: illustrator3.addr});
     console.log(userInfos3);
     expect(userInfos1.cred).to.equal('1');
     expect(userInfos1.name).to.equal(illustrator1.name);
     expect(userInfos1.addr).to.equal(illustrator1.addr);

    // création d'un tracker pour le solde de illustrator1
    for (let i=0; i<illustrators.length; i++) {
        let illustrateur = illustrators[i];
        illustrateur.tracker = await balance.tracker(illustrateur.addr);
        illustrateur.initialBalance = await illustrateur.tracker.get();
    }
  });

   it('Ne pas enregistrer plusieurs fois un illustrateur', async function () {
     await expectRevert(
       this.MarketPlaceInstance.registerIllustrator(illustrator1.name, { from: illustrator1.addr }),
       "Vous êtes déjà enregistré."
     );
   });

   it('illustrator4 n\'est pas enregistré', async function () {
       await expectRevert(
         this.MarketPlaceInstance.getIllustratorInfos({from: illustrator4.addr}),
         "You are not registered."
        );

   });

it('vérifier l\'enregistrement des sociétés', async function () {
    let result = await this.MarketPlaceInstance.registerCompany(company1.name, {from: company1.addr});
    company1.gasUsed += result.receipt.gasUsed;
    dumpTxInfos(result);

    result = await this.MarketPlaceInstance.registerCompany(company2.name, {from: company2.addr});
     company2.gasUsed += result.receipt.gasUsed;
     dumpTxInfos(result);

    let userInfos1 = await this.MarketPlaceInstance.getCompanyInfos({from: company1.addr});
    console.log(userInfos1);
    let userInfos2 = await this.MarketPlaceInstance.getCompanyInfos({from: company2.addr});
    console.log(userInfos2);

    expect(userInfos1.name).to.equal(company1.name);
    expect(userInfos1.addr).to.equal(company1.addr);

    // création d'un tracker pour le solde de company1
    // création d'un tracker pour le solde de illustrator1
    for (let i=0; i<companies.length; i++) {
        let company = companies[i];
        company.tracker = await balance.tracker(company.addr);
        company.initialBalance = await company.tracker.get();
    }

 });

   it('ne pas enregistrer plusieurs fois une société', async function () {
     await expectRevert(
       this.MarketPlaceInstance.registerCompany(company1.name,{ from: company1.addr }),
       "Vous êtes déjà enregistré."
     );
   });

  it('company 3 n\'est pas enregistrée', async function () {
       await expectRevert(
         this.MarketPlaceInstance.getCompanyInfos({from: company3.addr}),
         "You are not registered."
        );
  });

  it('créer une demande avec un montant erroné', async function () {
    await expectRevert(
        this.MarketPlaceInstance.submitJobRequest("Création d'un Logo",0,3600,1,{from: company1.addr}),
        "Le montant doit être suppérieur à 0"
        );
  });

  it('créer une demande avec une mauvaise crédibilité', async function () {
    await expectRevert(
        this.MarketPlaceInstance.submitJobRequest("Création d'un Logo",100,3600,0,{from: company1.addr}),
        "La crédibilité doit être supérieure à 0"
        );
  });

  it('créer une demande par une société non enregistrée', async function () {
    await expectRevert(
        this.MarketPlaceInstance.submitJobRequest("Création d'un Logo",100,3600,1,{from: company3.addr}),
        "Vous n'êtes pas enregistré"
        );
  });

  it('créer une demande avec des fonds insuffisants', async function () {
    await expectRevert(
        this.MarketPlaceInstance.submitJobRequest("Création d'un Logo",1000000000000,3600,1,{from: company1.addr}),
        "Montant payé insuffisant");
  });

  it('créer une demande avec des fonds suffisants', async function () {
    const _amount = 1000000000000;
    const fees = (_amount * 2) / 100; // 2% de frais
    /*await expect(
        this.MarketPlaceInstance.submitJobRequest("Création d'un Logo",_amount,3600,1,{from: company1.addr, value: _amount + fees})
    );
    */
    const sjr = await this.MarketPlaceInstance.submitJobRequest("Création d'un Logo",_amount,3600,1,{from: company1.addr, value: _amount + fees});
    expectEvent(sjr, 'NewJobRequest', { amount: '1000000000000' });
     company1.gasUsed += sjr.receipt.gasUsed;
     console.log(sjr);

  });

  it('Vérifier le montant total payé', async function () {
    let amount= await this.MarketPlaceInstance.totalPayableAmount();
    expect(amount).to.be.bignumber.equal(new BN('1020000000000'));
    console.log("montant total payé : "+amount.toString(10));
  });

 it('Vérifier le solde du SC après la création de la demande', async function () {
     const currentBalance = await this.trackerContract.get();
     console.log("SC balance :"+currentBalance);
     expect(currentBalance).to.be.bignumber.equal(new BN('1020000000000'));
  });

  it('créer une demande avec des fonds suffisants et une crédibilité minimale de 2', async function () {
    const _amount = 1000000000000;
    const fees = (_amount * 2) / 100; // 2% de frais
    let sjr = await this.MarketPlaceInstance.submitJobRequest("Création d'une charte graphique",_amount,3600,2,{from: company2.addr, value: _amount + fees});
    expectEvent(sjr, 'NewJobRequest', { amount: '1000000000000' });

    company2.gasUsed += sjr.receipt.gasUsed;
    console.log(sjr);
  });

  it('un illustrateur non enregistré fait une offre', async function () {
    await expectRevert(
        this.MarketPlaceInstance.makeOffer(0,{from: illustrator4.addr}),
        "Vous n'êtes pas enregistré"
        );
  });

  it('un illustrateur fait une offre pour une demande inexistante', async function () {
    await expectRevert(
        this.MarketPlaceInstance.makeOffer(2,{from: illustrator1.addr}),
        "Cette demande n'existe pas"
        );
  });


  it('un illustrateur fait une offre pour une demande avec une crédibilité insuffisante', async function () {
    await expectRevert(
        this.MarketPlaceInstance.makeOffer(1,{from: illustrator1.addr}),
        "Vous ne disposez pas de la crédibilité suffisante pour postuler."
        );
  });

  it('Les 3 illustrateurs font une offre sur la demande n°1', async function () {
    let result = await this.MarketPlaceInstance.makeOffer(0,{from: illustrator1.addr});
    illustrator1.gasUsed += result.receipt.gasUsed;

    result = await this.MarketPlaceInstance.makeOffer(0,{from: illustrator2.addr});
    illustrator3.gasUsed += result.receipt.gasUsed;

    result = await this.MarketPlaceInstance.makeOffer(0,{from: illustrator3.addr});
    illustrator3.gasUsed += result.receipt.gasUsed;
  });

  it('mémorise 2 demandes', async function () {
    let jrs = await this.MarketPlaceInstance.getOpenJobRequests({from: illustrator1.addr});
    //console.log(jrs+" "+typeof(jrs)+" "+jrs.length);
    assert(jrs != undefined && Array.isArray(jrs) && jrs.length == 2,"2 demandes doivent être crées.");

  });


  it('une société accepte une offre inexistante', async function () {
    await expectRevert(
        this.MarketPlaceInstance.acceptOffer(100,illustrator1.addr,{from: company1.addr}),
        "Cette demande n'existe pas"
        );
  });

  it('une société non enregistrée accepte une offre', async function () {
    await expectRevert(
        this.MarketPlaceInstance.acceptOffer(100,illustrator1.addr,{from: company3.addr}),
        "Vous n'êtes pas enregistré"
        );
  });

  it('une société accepte une offre pour une demande insexistante', async function () {
    await expectRevert(
        this.MarketPlaceInstance.acceptOffer(100,illustrator1.addr,{from: company1.addr}),
        "Cette demande n'existe pas"
        );
  });


  it('une société accepte une offre', async function () {
    let result = await this.MarketPlaceInstance.acceptOffer(0,illustrator1.addr,{from: company1.addr});
     company1.gasUsed += result.receipt.gasUsed;
  });

  it('un illustrateur fait une offre pour une demande en cours ou close', async function () {
    await expectRevert(
        this.MarketPlaceInstance.makeOffer(0,{from: illustrator2.addr}),
        "Les candidatures sont closes pour cette demande."
        );
  });

  it('une société accepte une offre déjà acceptée', async function () {
    await expectRevert(
        this.MarketPlaceInstance.acceptOffer(0,illustrator1.addr,{from: company1.addr}),
        "Cette demande est déjà acceptée."
        );
  });

  it('un illustrateur livre un travail pour une offre inexistante', async function () {
    await expectRevert(
        this.MarketPlaceInstance.deliver(100,"0x0000000000000000000000000000000000000000000000000000000000000000",{from: illustrator1.addr}),
        "Cette demande n'existe pas"
        );
  });

  it('un illustrateur non enregistré livre un travail', async function () {
    await expectRevert(
        this.MarketPlaceInstance.deliver(0,"0x0000000000000000000000000000000000000000000000000000000000000000",{from: illustrator4.addr}),
        "Illustrateur inconnu"
        );
  });
  it('un illustrateur livre un travail pour une offre pas encore acceptée ou fermée', async function () {
    await expectRevert(
        this.MarketPlaceInstance.deliver(1,"0x0000000000000000000000000000000000000000000000000000000000000000",{from: illustrator1.addr}),
        "Cette demande n'est pas en cours de réalisation"
        );
  });
  it('un illustrateur livre un travail pour lequel il n\'a pas été retenu', async function () {
    await expectRevert(
        this.MarketPlaceInstance.deliver(0,"0x0000000000000000000000000000000000000000000000000000000000000000",{from: illustrator2.addr}),
        "Vous n'êtes pas le candidat retenu pour cette offre"
        );
  });

  it('un illustrateur livre un travail.', async function () {
    let result =await
        this.MarketPlaceInstance.deliver(0,"0x0000000000000000000000000000000000000000000000000000000000000000",{from: illustrator1.addr});
    illustrator1.gasUsed += result.receipt.gasUsed;
  });

  it('illustrateur 1 doit avoir sa crédibilité augmentée de 1 point.', async function () {
    let userInfos1 = await this.MarketPlaceInstance.getIllustratorInfos({from: illustrator1.addr});
    expect(userInfos1.cred).to.equal('2');
  });

  it('un illustrateur non enregitré retire ses gains', async function () {
    await expectRevert(
        this.MarketPlaceInstance.withdraw({from: illustrator4.addr}),
        "Illustrateur inconnu"
        );
  });

  it('un illustrateur retire ses gains alors qu\'ils sont nuls', async function () {
    await expectRevert(
        this.MarketPlaceInstance.withdraw({from: illustrator3.addr}),
        "Vous n'avez aucun gain à retirer"
        );
  });

  it('un illustrateur retire ses gains.', async function () {
    const currentBalance = await illustrator1.tracker.get();
    let result = await this.MarketPlaceInstance.withdraw({from: illustrator1.addr});

    await time.advanceBlock();

    illustrator1.gasUsed += result.receipt.gasUsed;


  });

 /*
 it('a un symbole', async function () {
   expect(await this.ERC20Instance.symbol()).to.equal(_symbol);
 });
 it('a une valeur décimal', async function () {
   expect(await this.ERC20Instance.decimals()).to.be.bignumber.equal(_decimals);
 });
 it('vérifie la balance du propriétaire du contrat', async function (){
   let balanceOwner = await this.ERC20Instance.balanceOf(owner);
   let totalSupply = await this.ERC20Instance.totalSupply();
   expect(balanceOwner).to.be.bignumber.equal(totalSupply);
 });
 it('vérifie si un transfer est bien effectué', async function (){
   let balanceOwnerBeforeTransfer = await this.ERC20Instance.balanceOf(owner);
   let balanceRecipientBeforeTransfer = await this.ERC20Instance.balanceOf(recipient);
   let amount = ether("10");
 
   await this.ERC20Instance.transfer(recipient, amount, {from: owner});
 
   let balanceOwnerAfterTransfer = await this.ERC20Instance.balanceOf(owner);
   let balanceRecipientAfterTransfer = await this.ERC20Instance.balanceOf(recipient);
  expect(balanceOwnerAfterTransfer).to.be.bignumber.equal(balanceOwnerBeforeTransfer.sub(amount));
   expect(balanceRecipientAfterTransfer).to.be.bignumber.equal(balanceRecipientBeforeTransfer.add(amount));
 })
 */
});
