import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import MarketPlace from './abis/MarketPlace.json'

import Web3 from 'web3';

const TITLE = 'Place de marché pour illustrateurs';

class CompanyInfo extends React.Component {
  render() {
      return (
        <div className="companyInfo">
          <p>Société : {this.props.name}</p>
          <p>Address : {this.props.address}</p>
        </div>
      );
  }

}

class UserInfo extends React.Component {
  render() {
      return (
        <div className="userinfo">
          <p>Bonjour {this.props.name}</p>
          <p>Address : {this.props.address}</p>
        </div>
      );
  }

}


class JobRequestList extends React.Component {
    render() {
        let self=this;
        let jobRequests = this.props.jrs.map((aJobRequest, id) => {
        return (
            <div key={id} className="jobRequest">
                {aJobRequest.id}.&nbsp;Intitulé de la demande : {aJobRequest.description}&nbsp;
                {/* la syntaxe ()=>... est obligatoire pour indiquer à React que c'est une fonction */}
                <button onClick={()=>self.props.makeOfferFunction(aJobRequest.id)}>Postuler</button>
            </div>
        );

        } );

        return (
            <div className="jobRequestList">
                <h2>Demandes en attente de candidatures</h2>
                <div>
                    {jobRequests}
                </div>
            </div>
        );
    }
}

class JobRequestForm extends React.Component {
  state = {
    description: "",
    amount: 0,
    timeLimit: 0,
    credMin: 0
  }

  updateSubmitButtonState = () => {
    let ok = false;
    ok = this.state.description != "";
    let a = parseInt(this.state.amount,10);
    ok = ok && (a > 0);
    document.getElementById("NewJobRequest").disabled = !ok;
     //submitButton.disabled = (value.length === 0);
  }

  onChangeDescription  = (event) => {
    const value = event.target.value;
    //console.log(value);
    this.setState({description: value});
    this.updateSubmitButtonState();
  }
  onChangeAmount  = (event) => {
    const value = event.target.value;
    //console.log(value);
    if (isNaN(value)) return;
    this.setState({amount: value});
    this.updateSubmitButtonState();
  }

  onChangeTimeLimit  = (event) => {
    const value = event.target.value;
    //console.log(value);
    if (isNaN(value)) return;
    this.setState({timeLimit: value});
    this.updateSubmitButtonState();
  }
  onChangeCredMin  = (event) => {
    const value = event.target.value;
    console.log(value+" "+isNaN(value));
    if (isNaN(value)) return;
    this.setState({credMin: value});
    this.updateSubmitButtonState();
  }

  // le paramètre event peut être nommé comme n veut
  handleSubmitJobRequest = (event) => {
    event.preventDefault();
    console.log("submit "+this.state.description+" "+this.state.amount);

    this.props.onCreateJobRequest(this.state.description, this.state.amount, this.state.timeLimit, this.state.credMin);
    return true;
  }

  render() {
    return (
        <div>
          <p>Création d'une de demande de travail</p>
          <p>2% de frais seront comptés par le service</p>
          <form onSubmit={this.handleSubmitJobRequest}>
            Description : <input name="name" type="text" placeholder="Description du travail" value={this.state.description} required onChange={this.onChangeDescription}/>
            Montant : <input name="name" type="text" placeholder="Montant" value={this.state.amount} required onChange={this.onChangeAmount}/>&nbsp;wei
            Durée : <input name="name" type="text" placeholder="timeLimit" value={this.state.timeLimit} required onChange={this.onChangeTimeLimit}/>&nbsp;minutes
            Crédibilité minimum : <input name="name" type="text" placeholder="credMin" value={this.state.credMin} required onChange={this.onChangeCredMin}/>&nbsp;minutes
            <button disabled type="submit" value="NewJobRequest" id="NewJobRequest">Créer une demande</button>
          </form>
      </div>
    );
  }
}

class RegisterForm extends React.Component {
  state = {
    username: "",
    companyName: ""
  }

  // le paramètre event peut être nommé comme n veut
  handleSubmitIllustrator = (event) => {
    event.preventDefault();
    console.log("submit "+this.state.username);

    this.props.onRegisterIllustrator(this.state.username);
    return true;
  }

    handleSubmitCompany = (event) => {
    event.preventDefault();
    const profile =
    // current référence l'objet HTML, donc le INPUT
    console.log("submit "+this.state.companyName);

    this.props.onRegisterCompany(this.state.companyName);
    return true;
  }

  registerAsCompany = () => {
    console.log("as a company");
    this.props.onRegisterCompany();
  }

  registerAsIllustrator = () => {
    console.log("as an illustrator");
    this.props.onRegisterIllustrator();
  }

  onUsernameChange  = (event) => {
    const value = event.target.value;
    const submitButton = document.getElementById("RegisterAsIllustrator");
    //console.log(value);
    this.setState({username: value});
    submitButton.disabled = (value.length === 0);
  }

  onCompanyNameChange  = (event) => {
    const value = event.target.value;
    const submitButton = document.getElementById("RegisterAsCompany");
    //console.log(value);
    this.setState({companyName: value});
    submitButton.disabled = (value.length === 0);
  }

  render() {
    return (
        <div>
        <p>Merci de vous enregistrer auprès de l'application...</p>

        <form onSubmit={this.handleSubmitIllustrator}>
          <input name="name" type="text" placeholder="Votre nom" value={this.state.username} required onChange={this.onUsernameChange}/>

          &nbsp;
          <button disabled type="submit" value="RegisterAsIllustrator" id="RegisterAsIllustrator">S'enregistrer comme illustrateur</button>
        </form>
      ou

      <form onSubmit={this.handleSubmitCompany}>
          <input name="name" type="text" placeholder="Nom de la société" value={this.state.companyName} required onChange={this.onCompanyNameChange}/>

        &nbsp;
          <button disabled type="submit" value="RegisterAsCompany" id="RegisterAsCompany">S'enregistrer comme société
        </button>
        </form>
      </div>
    );
  }

}

class App extends Component {

    async makeOffer(jobRequestId) {
        console.log("Postuler "+jobRequestId);
    }


    async createJobRequest(desc, mnt, limit, cred) {
        console.log("Creation d'une JobRequest");
        const amountToPay = (mnt * 1.02);
        this.state.marketPlace.methods.submitJobRequest(desc, mnt, limit, cred).send({from: this.state.address, value: amountToPay})
        .on('transactionHash', function(hash){
            console.log("transactionHash, "+hash);
        })
        .on('receipt', function(receipt){
            console.log("receipt, "+receipt);
        })
        .on('confirmation', function(confirmationNumber, receipt){
            console.log("confirmation, "+confirmationNumber+" "+receipt);
        })
        .on('error', function(error, receipt) {
            console.log("error, "+error+" "+receipt);
        });
    }

  async registerCompany(name) {
    console.log("Enregistrement de la société "+name);
    //try {
        //const r = await this.state.marketPlace.methods.registerCompany(name).call().then((r2) => { console.log("Company registered "+r2)});
        this.state.marketPlace.methods.registerCompany(name).send({from: this.state.address})
        .on('transactionHash', function(hash){
            console.log("transactionHash, "+hash);
        })
        .on('receipt', function(receipt){
            console.log("receipt, "+receipt);
            this.setState({registered: true, companyName: name});
        })
        .on('confirmation', function(confirmationNumber, receipt){
            console.log("confirmation, "+confirmationNumber+" "+receipt);
        })
        .on('error', function(error, receipt) {
            console.log("error, "+error+" "+receipt);
        });


  }

  registerIllustrator = (name) => {
    console.log("Enregistrement de l'illustrateur "+ name);

    this.state.marketPlace.methods.registerIllustrator(name).send({from: this.state.address})
    .on('transactionHash', function(hash){
        console.log("transactionHash, "+hash);
    })
    .on('receipt', function(receipt){
        console.log("receipt, "+receipt);
        this.setState({registered: true, username: name});
    })
    .on('confirmation', function(confirmationNumber, receipt){
        console.log("confirmation, "+confirmationNumber+" "+receipt);
    })
    .on('error', function(error, receipt) {
        console.log("error, "+error+" "+receipt);
    });
  }

  async componentDidMount() {
    // Detect Metamask
    const metamaskInstalled = typeof window.web3 !== 'undefined'
    this.setState({ metamaskInstalled })
    if(metamaskInstalled) {
      await this.loadWeb3()
      await this.loadBlockchainData()
          console.log("METAMASK is installed");
    } else {
        console.log("Metamask is not installed.");
    }
  }

    async loadBlockchainData() {
        const web3 = window.web3
        // Load account
        const accounts = await web3.eth.getAccounts()
        this.setState({ account: accounts[0] })
        const networkId = await web3.eth.net.getId()
        console.log("Account : "+accounts[0]);
        this.setState({address: accounts[0]});
        const networkData = MarketPlace.networks[networkId]
        console.log(networkData);
        if (networkData) {
            const marketPlace = new web3.eth.Contract(MarketPlace.abi, networkData.address);
            this.setState({ marketPlace })
            try {
                await marketPlace.methods.getCompanyInfos().call({from: accounts[0]}).then((infos) => {
                    console.log("commpany infos : "+infos+" ["+infos.name+"]");
                    this.setState({address: accounts[0], companyName: infos.name, registered: true});
                 });
            }catch {
                try {

                    const userInfos = await marketPlace.methods.getIllustratorInfos().call({from: accounts[0]});

                    if (userInfos) {
                        const jrs = await marketPlace.methods.getOpenJobRequests().call({from: accounts[0]});
                        console.log("Nb de demandes ouvertes :"+jrs.length);
                        for (let i=0; i<jrs.length; i++) {
                            console.log(jrs[i].description);
                        }
                    this.setState({address: accounts[0], username: userInfos.name, registered: true,openJobRequests: jrs});
                    }

                } catch {
                }
            }
            /*

            let r = await marketPlace.methods.illustratorCount().call();
            console.log("Nombre d'illustrateurs:"+r);
            */

            this.setState({ loading: false})
        }
        /*

        if(networkData) {
          const socialNetwork = web3.eth.Contract(SocialNetwork.abi, networkData.address)
          this.setState({ socialNetwork })
          const postCount = await socialNetwork.methods.postCount().call()
          this.setState({ postCount })
          // Load posts
          for (var i = 1; i <= postCount; i++) {
            const post = await socialNetwork.methods.posts(i).call()
            this.setState({
              posts: [...this.state.posts, post]
            })
          }
          // Sort posts. Show highest tipped posts first
          this.setState({
            posts: this.state.posts.sort((a,b) => b.tipAmount - a.tipAmount)
          })
          this.setState({ loading: false})
        } else {
          window.alert('SocialNetwork contract not deployed to detected network.')
        }
        */
      }

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      address: "",
      registered: false,
      connectedToMetamask: true,
      illustrator: false,
      username: "",
      companyName: ""
    }

    this.registerCompany = this.registerCompany.bind(this)
    this.registerIllustrator = this.registerIllustrator.bind(this)
    this.createJobRequest = this.createJobRequest.bind(this)
    this.makeOffer = this.makeOffer.bind(this)

}

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable();
      console.log("window.ethereum");
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
      console.log("window.web3");
    }
    else {
    }
  }

  render() {

        if (! this.state.connectedToMetamask) {
          return (
            <div>
            <h1>{TITLE}</h1>
            <p>Vous devez être connecté avec Metamask sur la blockchain</p>
            </div>
          );
        }

        if (! this.state.registered) {
          return (
            <div>
              <h1>{TITLE}</h1>
              <p>Adresse : {this.state.address}</p>
              <RegisterForm onRegisterCompany={this.registerCompany} onRegisterIllustrator={this.registerIllustrator}/>
            </div>
          );
        }
    return (
      <div>
        <h1>{TITLE}</h1>

          {this.state.username==="" &&
              <div>
                 <CompanyInfo address={this.state.address} name={this.state.companyName}/>
                 <JobRequestForm onCreateJobRequest={this.createJobRequest} />
                </div>
          }
          {this.state.companyName==="" &&
             <div>
                 <UserInfo name={this.state.username} address={this.state.address}/>
                 <JobRequestList jrs={this.state.openJobRequests} makeOfferFunction={this.makeOffer}/>
             </div>
          }

      </div>

    );
  }
}


export default App;
