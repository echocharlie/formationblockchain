import unittest


from exercise121 import *
from math import factorial

class MyTestCase(unittest.TestCase):
    def test_factorielle(self):
        #self.assertEqual(True, False)
        for i in range(101):
            self.assertEqual(factorielle(i), factorial(i))


    def test_factorielle_recursive(self):
        #self.assertEqual(True, False)

        for i in range(101):
            self.assertEqual(factorielle_recursive(i), factorial(i))


if __name__ == '__main__':
    unittest.main()

