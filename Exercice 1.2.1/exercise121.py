# -*- coding: utf8 -*-

"""
    Exercice 1.2.1: Calculer la factorielle d'un nombre

"""
import math

def factorielle(number):
    """
    :param number:
    :return: La factorielle de number
    """
    if number in (0,1): return 1
    r = 1
    for i in range(1, number+1):
        r = r * i
    return r

def _facto(current, number, result):
    if current > number: return result

    #print(current, number, result)
    result = result * current
    current+=1
    result  = _facto(current, number, result)
    return result

def factorielle_recursive(number):
    """
    :param number:
    :return: La factorielle de number
    """
    if number in (0,1): return 1
    result = 2
    result = _facto(3, number, result)

    return result


if __name__ == '__main__':
    print(math.factorial(0))
    print(math.factorial(1))
    print(factorielle(0))
    print(factorielle(1))
    print(factorielle(2))
    print("\n")
    print(factorielle_recursive(0))
    print(factorielle_recursive(1))
    print("2! =",factorielle_recursive(2))
    #print(math.factorial(2))
    #print(math.factorial(3))
    print("3! =",factorielle_recursive(3))

