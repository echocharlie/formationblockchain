pragma solidity >0.5.8;

contract Epinglage {
    address public owner;

	string public name = "Defi 3 - Epinglage";

	constructor() public {
		owner = msg.sender;
	}

	uint public  totalPayableAmount;

	event Epingler(string hash);

	function payerStockage(string memory hash) public payable {
		require((msg.value >= 100000000000000000 wei), "Montant payé insuffisant (0.1 ETH minimum)");

		totalPayableAmount += msg.value;
		emit Epingler(hash);
	}

}