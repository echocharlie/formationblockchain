const ipfsClient = require('ipfs-http-client');
// connect to ipfs daemon API server
const ipfs = ipfsClient('http://192.168.70.115:5001') 


exports.addFileToIPFS = async function addFileToIPFS(file) {
    console.log('Add file ' + file.name + ' to IPFS');

    if (!fs.existsSync(file.path)) {
        console.error('File not found.');
        return;
    }

    // envoi du fichier vers IPFS
    const files = [];
    files.push({
        path: '/defi3/' + file.name,
        content: fs.readFileSync(file.path),
        mtime: fs.statSync(file.path).mtime
    });

    for await (const result of ipfs.add(files, { pin: false, })) {
        console.log(result);

        await sendPayment(result.cid);
        return result;
    }

    // suppression du fichier du répertoire DATA que l'appel IPFS soit ok ou non
    console.log('Remove file ' + file.path);
    fs.unlink(file.path, function (err) {
        if (err) {
            return console.error(err);
        }
        console.log("File deleted successfully!");
    });

}

exports.pinFileToIPFS = async function pinFileToIPFS(hash) {
    console.log('Pin file with hash ' + hash + ' to IPFS');

    const pinset = await ipfs.pin.add(hash);
    console.log(pinset)

}