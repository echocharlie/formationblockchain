'use strict';
var debug = require('debug')('app');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');

const ethers = require('ethers');
const ipfsClient = require('ipfs-http-client');

const menu = [{ option: "page 1", link: "/" }, { option: "page 2", link: "/" }];
var routes = require('./routes/index')(menu);
var users = require('./routes/users');

var app = express();

var env = process.env.NODE_ENV || 'development';


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); // pug

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('tiny')); // dev combined
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/css', express.static(path.join(__dirname, '/node_modules/bootstrap/dist/css')));
app.use('/js', express.static(path.join(__dirname, '/node_modules/bootstrap/dist/js')));
app.use('/js', express.static(path.join(__dirname, '/node_modules/jquery/dist')));
app.use('/js', express.static(path.join(__dirname, '/node_modules/ipfs-http-client/dist/')));
app.use('/js', express.static(path.join(__dirname, '/node_modules/ethers/dist/')));


app.use('/', routes);
app.use('/users', users);

var IPFS_NODE_URL;
var CONTRACT_ADDRESS;
var ETH_NODE;

console.log('env : '+env)
if (env === 'production') {
    CONTRACT_ADDRESS = '0x8a96c305498acf249f26496d402847e7f8ef9048';
    IPFS_NODE_URL = 'http://192.168.70.115:5001';
    ETH_NODE = 'http://localhost:7545';
}
else {
    IPFS_NODE_URL = 'http://vps809473.ovh.net:5001';
    CONTRACT_ADDRESS = '0x35bbFD53b2091fEBbe26e7733ca2c31DBE37405a';
    ETH_NODE = 'http://vps809473.ovh.net:8501';
    /*IPFS_NODE_URL = 'http://192.168.70.115:5001';
    CONTRACT_ADDRESS = '0x473B847DF136f40C33F2BACe7d7D294039d8d14E';
    ETH_NODE = 'http://192.168.70.115:8545';
    */
}

// connect to ipfs daemon API server
const ipfs = ipfsClient(IPFS_NODE_URL); 

// G�n�ration du fichier apidata.js
// qui contient l'adresse du SC et son ABI.
// ainsi que l'URL du noeud IPFS
app.get('/api/apidata.js', function (req, res) {
    console.log('Sending apidata.js');
    res.setHeader('content-type', 'text/javascript');
    res.write("const IPFS_NODE_URL='" + IPFS_NODE_URL + "';");
    res.write("const SC_ADDRESS='" + CONTRACT_ADDRESS + "';");
    res.write('\n');
    const contractCompleteMetadata = JSON.parse(fs.readFileSync(path.join(__dirname, '/abis/Epinglage.json'), "utf8"));
    const abi = contractCompleteMetadata.abi;
    res.write('const SC_ABI=' + JSON.stringify(abi) + ';');
    res.end();
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});



// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.set('port', process.env.PORT || 3000);



var server = app.listen(app.get('port'), function () {
    debug('Starting the D�fi 3 application...');
    
    debug(`Express server listening on port ${server.address().port}`);

    // IPFS Version.
    ipfs.version().then((version) => {
        console.log(version);
    });

    // Test connexion RPC Ethereum
    const url = ETH_NODE;
    let customHttpProvider = new ethers.providers.JsonRpcProvider(url);
    customHttpProvider.getNetwork().then(network => {
        console.log('network : ' + network.name + ' ' + network.chainId + ' ' + network.ensAddress);
        
        // juste pour le fun 
        customHttpProvider.listAccounts().then(accounts => {
            let address = accounts[0]; // "0x54e236C0D3C87e1700Dcbf2AFCa25F66Ac9dAFEc";
            customHttpProvider.getBalance(address).then((balance) => {

                // balance is a BigNumber (in wei); format is as a sting (in ether)
                let etherString = ethers.utils.formatEther(balance);

                console.log(`Balance of ${address} : ${etherString}`);
            });

        });
        
        // Connexion au SC
        console.log('Loading smart contract ABI...')
        try {
            const contractCompleteMetadata = JSON.parse(fs.readFileSync(path.join(__dirname, '/abis/Epinglage.json'), "utf8"));
            const abi = contractCompleteMetadata.abi;
            console.log('ABI loaded.');
                        
            let contract = new ethers.Contract(CONTRACT_ADDRESS, abi, customHttpProvider);
            contract.name().then((name) => {
                console.log('contract name : ' + name);

                // SC event handler
                contract.on('Epingler', (hash) => {
                    console.log('server side event fired with hash: ' + hash);
                });
            }).catch(error => {
                console.error('Unable to connect to the smart contract, error:' + error);
                process.exit(1);
            });


            
        } catch (error) {
            console.error('Unable to load smart contract ABI ' + error.message);
            process.exit(1);
        }


    }).catch(error => {
        console.error('Unable to connect to Ethereum network');
        process.exit(1);
    });


    
});

