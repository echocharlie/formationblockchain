'use strict';
var express = require('express');
var router = express.Router();

var formidable = require('formidable');
var fs = require('fs');
const path = require("path")

const ethers = require('ethers');

var ipfs = require('../ipfs'); 

const title = 'D\u00E9fi 3';
const subTitle = 'Service d\'\u00E9pinglage de fichiers';
const contractAddress = '0x473B847DF136f40C33F2BACe7d7D294039d8d14E';
const contractCompleteMetadata = JSON.parse(fs.readFileSync(path.join(__dirname, '../abis/Epinglage.json'), "utf8"));
const abi = contractCompleteMetadata.abi;

async function sendPayment(hash) {
    console.log('Sending payment for hash ' + hash);

    // Test connexion RPC Ethereum
    const url = "http://localhost:7545";
    let customHttpProvider = new ethers.providers.JsonRpcProvider(url);
    customHttpProvider.getNetwork().then(network => {
        console.log('network : ' + network.name + ' ' + network.chainId + ' ' + network.ensAddress);

        let address = "0x54e236C0D3C87e1700Dcbf2AFCa25F66Ac9dAFEc";

        // Connexion au SC
        try {
            let contract = new ethers.Contract(contractAddress, abi, customHttpProvider);
            contract.name().then(name => {
                console.log('contract name : ' + name);
            });

        } catch (error) {
            console.error('Unable to connect to the smart contract ' + error.message);
        }
    });
}


function createRouter(menu) {

    /* GET home page. */
    router.get('/', function (req, res) {
        res.render('index', {
            title, subTitle,
            menu,
            message: '',
            errorMessage: ''
        });
    });

    // Upload file
    router.post('/upload', (req, res) => {
        console.log('upload file...');
        let message = '';
        
        var form = new formidable.IncomingForm();
        form.parse(req);
        
        if (form.bytesExpected > 0 && form.bytesExpected < 1000000) {
            
            //console.log(form);
            // démarrage de l'upload, et sauvegarde du fichier dans le répertoire data
            form.on('fileBegin', function (name, file) {
                file.path = __dirname + '/../data/' + file.name;
                console.log('on, ' + name + ' ' + file.path);
            });

            
            // fin du téléchargement
            form.on('file', function (name, file) {
                
                addFileToIPFS(file).then(result => {
                    console.log('--->' + result.path + ' ' + result.cid);
                    message = 'Fichier ' + file.name + ' t\u00E9l\u00E9charg\u00E9 avec succ\u00EAs, HASH : ' + result.cid;
                    console.log('message:' + message);

                    res.render('index', {
                        title, subTitle,
                        menu,
                        message,
                        errorMessage: '',
                        errors: {
                        }
                    });
                });


            });
        } else {
            let errorMessage = '';
            if (form.bytesExpected > 0) errorMessage = 'Fichier trop gros, merci de s\u00E9lectionner un fichier plus petit.';
            res.render('index', {
                title, subTitle,
                menu,
                errorMessage,
                message,
                errors: {
                }
            });
        }
        });


    return router;
}





module.exports = createRouter;
