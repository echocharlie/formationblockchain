var contractEpinglage;
var web3Provider;
const ipfs = window.IpfsHttpClient(IPFS_NODE_URL);

async function loadWeb3() {
    console.log("Loading the provider");
    if (window.ethereum) {
        try {
            window.web3 = new Web3(window.ethereum)
            await window.ethereum.enable();
            console.log("window.ethereum");
            web3Provider = new ethers.providers.Web3Provider(web3.currentProvider);
            //provider = new ethers.providers.Web3Provider(ethereum);
            
            
            $('#userAddress').html(ethereum.selectedAddress);
        } catch (error) {
            console.log("Not connected.");
        }
    }
    else if (window.web3) {
        window.web3 = new Web3(window.web3.currentProvider)
        console.log("window.web3");
        web3.eth.getAccounts().then((accounts) => {
            if (accounts && accounts.length > 0) {
                document.userAddress = accounts[0];
                $('#userAddress').html(accounts[0]);
            }
        });
    }
    else {
    }
}

async function isMetamaskInstalled() {
    // Detect Metamask
    const metamaskInstalled = typeof window.web3 !== 'undefined'
    if (metamaskInstalled) {
        console.log("METAMASK is installed");
        await this.loadWeb3();
        await connectToSC();
        return true;
    } else {
        console.log("Metamask is not installed.");
        return false;
    }
}

async function connectToSC() {
    console.log('Connecting to the SC');
    try {
        contractEpinglage = new ethers.Contract(SC_ADDRESS, SC_ABI, web3Provider.getSigner());
        console.log(connectToSC);
        contractEpinglage.name().then(name => {
            console.log('Connected to SC "' + name+'"');
        });

        // SC Event handler
        let r = await contractEpinglage.on("Epingler", (hash) => {
            console.log('event fired with hash: ' + hash);
            pinFileToIPFS(hash).then(() => {
            }).catch((error) => {
                let errorMessage = 'Une erreur est survenue pendant l\'\u00E9pinglage du fichier sur IPFS. '+error;
                showErrorMessage(errorMessage);
            });
        });
    } catch (err) {
        console.error(err);
    }

}

// A $( document ).ready() block.
$(document).ready(function () {
    console.log("ready!");
    isMetamaskInstalled().then((isInstalled) => {
        console.log("MetaMask is installed : " + isInstalled);
        document.metamaskInstalled = isInstalled;
        
        if (isInstalled) {
            $('#metamaskStatus').html('Metamask est install&eacute;');
            $('#main').show();
        } else $('#metamaskStatus').html('Metamask n\est pas install&eacute;');
    });

    const form = document.getElementById('theForm');
    
    form.addEventListener('submit', sendFile);

    console.log('Smart Contract Address : ' + SC_ADDRESS);
    $('#smartContractAddress').html(SC_ADDRESS);
    
    
});

function loadFile() {
}

async function pinFileToIPFS(hash) {
    console.log('Pin file with hash ' + hash + ' to IPFS');

    const pinset = await ipfs.pin.add(hash);
    console.log(pinset)

}

// envoi du fichier sur IPFS et appel du service de paiement du SC
async function sendFile(event) {
    let fileInput = document.getElementById('theFile');

    console.log(fileInput);
    let file = fileInput.files[0];
    console.log(file);
    if (FileReader && file) {

        const reader = new FileReader();
        reader.onload = async function () {
            //document.getElementById("theFile").src = reader.result;
            //console.log(reader.result);
            console.log('file read');

            // envoi du fichier vers IPFS
            
            const files = [];
            files.push({
                path: file.name,
                content: reader.result
            });

            for await (const result of ipfs.add(files, { pin: false, })) {
                //console.log(result);
                console.log(result.path + ' ' + result.cid.toString());
                console.log('Calling the SC.');
                contractEpinglage.payerStockage(result.cid.toString(), { value: ethers.utils.parseEther('0.1') }).then((scResult) => {
                    console.log(scResult.hash);
                    console.log('SC called to pay the file storage');
                    let message = 'Fichier <strong>' + result.path + '</strong> t\u00E9l\u00E9charg\u00E9 avec succ\u00EAs, HASH : <strong>' + result.cid.toString() + '</strong>';
                    message += '<br/>Transaction : <strong>' + scResult.hash + '</strong>';
                    showMessage(message);
                })
                    .catch((error) => {
                        let errorMessage = 'Le paiement a \u00E9t\u00E9 rejet\u00E9.';
                        showErrorMessage(errorMessage);
                    });
                //return result;
            }
        }
        reader.readAsBinaryString(file);

    } else {

    }

    event.preventDefault();
}

function showMessage(message) {
    $('#message').html(message);
    $('#messagePanel').show();
}

function hideMessage() {
    $('#messagePanel').hide();

}

function showErrorMessage(message) {
    $('#errorMessage').html(message);
    $('#errorMessagePanel').show();

}

function hideErrorMessage() {
    $('#errorMessage').hide();

}