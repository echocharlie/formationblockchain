const {
  BN,           // Big Number support
  balance,
  time,
  ether,
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');

const { expect } = require('chai');

const CryptoBonsai = artifacts.require('CryptoBonsai');

contract('My Crypto Bonsai', function (accounts) {
 const _name = 'CryptoBonsai';
 const _owner = accounts[0];

 async function getBalance(from) {
    if (from.tracker != undefined) {
        const r = await from.tracker.get();
        return r;
    } else {
        return -1;
    }
 }

// cf https://github.com/ethereum/wiki/wiki/JavaScript-API#web3ethgettransactionreceipt
 function dumpTxInfos(result) {
    console.log(result);
 }


   before(async function () {
	 
    this.CryptoBonsaiInstance = await CryptoBonsai.new({from: _owner});
    console.log("Created CryptoBonsaiInstance at "+this.CryptoBonsaiInstance.address);

    console.log(CryptoBonsai.defaults());
    this.owerOfContracts = CryptoBonsai.defaults().from;

    this.trackerContract = await balance.tracker(this.CryptoBonsaiInstance.address);
  });

  beforeEach(async function () {
    console.log("\n");
 });

  afterEach(async function () {
 });

  after(async function () {
  console.log("\nTESTS END");
 });

  it('has a name', async function () {
    expect(await this.CryptoBonsaiInstance.name()).to.equal(_name);
  });

  it("Should make first account an owner", async function () {
    expect(await this.CryptoBonsaiInstance.owner()).to.equal(_owner);
  });

  it("Should fail to call function with address 0", async function () {
    await expectRevert(this.CryptoBonsaiInstance.balanceOf(constants.ZERO_ADDRESS),"ERC721: balance query for the zero address");
  });

  it("Should have at least one category, one sub caegory for each category and one species for each sub category", async function () {
    let cat = await this.CryptoBonsaiInstance.getCategories();
    console.log(cat);
    expect(cat.length).to.greaterThan(0);

    for (let i=0; i<cat.length; i++) {
      let subCat = await this.CryptoBonsaiInstance.getSubCategories(i);
      expect(subCat.length).to.greaterThan(0);

      for (let j=0; j<subCat.length; j++) {
        let species = await this.CryptoBonsaiInstance.getSpecies(i,j);
        expect(species.length).to.greaterThan(0);
      }
  
    }
  });

  it("Should not allow creating a bonsai with invalid name", async function () {
    await expectRevert(this.CryptoBonsaiInstance.createBonsai("",0,0,0),"Bonsai: Name must be at least 4 characters long");
  });
  it("Should not allow creating a bonsai with an invalid category", async function () {
    await expectRevert(this.CryptoBonsaiInstance.createBonsai("My first Bonsai",4,0,0),"Bonsai: Invalid category index");
  });
  it("Should not allow creating a bonsai with an invalid sub-category", async function () {
    await expectRevert(this.CryptoBonsaiInstance.createBonsai("My first Bonsai",0,4,0),"Bonsai: Invalid sub-category index");
  });
  it("Should not allow creating a bonsai with an invalid species", async function () {
    await expectRevert(this.CryptoBonsaiInstance.createBonsai("My first Bonsai",0,0,10),"Bonsai: Invalid species index");
  });

  it("Should allow creating a bonsai with valid name, category, sub-category, species", async function () {
    const tx = await this.CryptoBonsaiInstance.createBonsai("My first Bonsai",0,0,0,{from: accounts[1]});

    expectEvent(tx, 'Transfer', { from: constants.ZERO_ADDRESS, to: accounts[1], tokenId: new BN('1') });
  });

  it("Should now store 1 bonsai", async function () {
    const count = await this.CryptoBonsaiInstance.getBounsaiCount();

    expect(count).to.be.bignumber.equal(new BN('1'));
  });


  it("Should stand that accounts[1] owns 1 token", async function () {
    const balance = await this.CryptoBonsaiInstance.balanceOf(accounts[1]);

    expect(balance).to.be.bignumber.equal(new BN('1'));
  });

  it("Should stand that accounts[1] owns 'My First Bonsai'", async function () {
    expect(await this.CryptoBonsaiInstance.ownerOf(1)).to.equal(accounts[1]);
  });

  it("Should not allow creating a bonsai with an existing name", async function () {
    await expectRevert(this.CryptoBonsaiInstance.createBonsai("My first Bonsai",0,0,1),"Bonsai: Name already exists");
  });

  it("Should not allow to destroy a non-existing bonsai", async function () {
    await expectRevert(this.CryptoBonsaiInstance.destroyBonsai(99),"ERC721: owner query for nonexistent token");
  });

  it("Should not allow to destroy a bonsai you do not own", async function () {
    await expectRevert(this.CryptoBonsaiInstance.destroyBonsai(1, {from: accounts[2]}),"Bonsai: You do not owns this bonsai");
  });

  it("Should allow to destroy an existing bonsai", async function () {
    const tx = await this.CryptoBonsaiInstance.destroyBonsai(1,{from: accounts[1]});

    expectEvent(tx, 'Transfer', { from: accounts[1], to: constants.ZERO_ADDRESS, tokenId: new BN('1') });
  });
  it("Should now store 0 bonsai", async function () {
    const count = await this.CryptoBonsaiInstance.getBounsaiCount();

    expect(count).to.be.bignumber.equal(new BN('0'));
  });

  it("Should stand that accounts[1] does not own any token", async function () {
    const balance = await this.CryptoBonsaiInstance.balanceOf(accounts[1]);

    expect(balance).to.be.bignumber.equal(new BN('0'));
  });
  it("Should allow to create a random bonsai", async function () {
    const tx = await this.CryptoBonsaiInstance.createRandomBonsai("My first Bonsai",{from: accounts[1]});

    expectEvent(tx, 'Transfer', { from: constants.ZERO_ADDRESS, to: accounts[1], tokenId: new BN('2') });
  });

  it("Should allow to watering a bonsai", async function ()  {
    await this.CryptoBonsaiInstance.createRandomBonsai.sendTransaction("My second Bonsai",{from: accounts[3]});
    
    let bonsais = await this.CryptoBonsaiInstance.getBonsais({from: accounts[3]});
    const bonsaiId = bonsais[bonsais.length-1];
    //console.log(bonsais.length+" "+bonsaiId);

    const _exists = await this.CryptoBonsaiInstance.exists(bonsaiId);
    //console.log(bonsaiId+" "+_exists);

    
    await this.CryptoBonsaiInstance.watering(bonsaiId,{from: accounts[3]});
    //console.log(humidityLevel+" "+humidityLevel.toNumber());

    let bonsai = await this.CryptoBonsaiInstance.getBonsai(bonsaiId, {from: accounts[3]});
    console.log(bonsai);
  });

  it("Should grow", async function () {
    let bonsais = await this.CryptoBonsaiInstance.getBonsais({from: accounts[3]});
    const bonsaiId = bonsais[bonsais.length-1];
    let bonsai = await this.CryptoBonsaiInstance.getBonsai(bonsaiId, {from: accounts[3]});
    console.log(bonsai);
    time.advanceBlock(time.latest() + 1);
    let bonsai2 = await this.CryptoBonsaiInstance.getBonsai(bonsaiId, {from: accounts[3]});
    console.log(bonsai2);
    expect(bonsai2.humidityLevel-bonsai.humidityLevel).to.be.equal(-5);
    expect(bonsai2.growth-bonsai.growth).to.be.equal(4);
  });  

  it("Should allow to prune a bonsai", async function ()  {
    let bonsais = await this.CryptoBonsaiInstance.getBonsais({from: accounts[3]});
    const bonsaiId = bonsais[bonsais.length-1];

    await this.CryptoBonsaiInstance.prune(bonsaiId,{from: accounts[3]});

    let bonsai = await this.CryptoBonsaiInstance.getBonsai(bonsaiId, {from: accounts[3]});
    console.log(bonsai);
  });

  it("Should approve accounts[4] to take ownership of a bonsai", async function ()  {
    let bonsais = await this.CryptoBonsaiInstance.getBonsais({from: accounts[3]});
    const bonsaiId = bonsais[bonsais.length-1];

    const tx = await this.CryptoBonsaiInstance.approve(accounts[4], bonsaiId, {from: accounts[3]});
    expectEvent(tx, 'Approval', { owner: accounts[3], approved: accounts[4], tokenId: bonsaiId });
    
  });
  
 });
