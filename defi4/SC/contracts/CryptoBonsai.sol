pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import '@openzeppelin/contracts/ownership/Ownable.sol';

//import "github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";

/// @title A NFT that represents a Bonsai
/// @author Emmanuel Collin
/// @notice Alyra Defi #4
contract CryptoBonsai is ERC721, Ownable {
	using SafeMath for uint256;

	string public constant name = "CryptoBonsai";
	string public constant symbol = "BNS";

	string[] public CATEGORIES = ["Les arbres caducs","Les persistants","Pins et conifères"];

	string[] private SUB_CATEGORY_1 = ["Caducs avec feuilles opposées","Caducs avec feuilles alternées"];
	string[] private SUB_CATEGORY_2 = ["Les persistants avec feuilles opposées","Les persistants avec feuilles alternées"];
	string[] private SUB_CATEGORY_3 = ["Pousse à croissance allongée","Pousse à croissance verticillé","Conifères caduc"];

	string[] private SPECIES_1 = ["Erable jaonais","Erable trident","Grenadier"];
	string[] private SPECIES_2 = ["Orme de Chine","Orme du Japon","Hêtre commun","Hêtre crénelé","Charme","Charme de Corée","Glycine","Magnolia stellata","Pommier"];
	string[] private SPECIES_3 = ["Troène","Buis","Chèvrefeuille","Neige de juin","Olivier","Arbre de Jade","Poivrier de Chine","Durante"];
	string[] private SPECIES_4 = ["Figuier","Arbre à thé","Azalée","Arbre à plume","Bougainville"];
	string[] private SPECIES_5 = ["Genévrier de Chine","Genévrier rigide","Genévrier commun","Epicéa commun","Epicéa de Yedo","If commun","If du Japon"];
	string[] private SPECIES_6 = ["Pin noir du Japon","Pin rouge du Japon","Pin blanc du Japon","Pin sylvestre","Pin de montagne","Pin ponderosa"];
	string[] private SPECIES_7 = ["Mélèze d’Europe","Mélèze du Japon","Mélèze hybride","Cyprès chauve","Séquoia de Chine"];
	
	// Key : Category Index
	mapping(uint => string[]) public SUB_CATEGORIES;

	// Key : (Category Index * 10) + sub category index
	mapping(uint => string[]) public SPECIES;

	struct Bonsai {
		uint256 tokenId;
		string name;
		uint species;
		uint256 creationTime; // timestamp de la création
		uint256 blockId;	  // Numéo de block sur lequel a eu lieu la dernière action sur le Bonsai
		uint256 humidityLevel; // Taux d'humidité
		uint256 growth; // croissance, de 1 à ...
	}

	uint256 private currentTokenId = 0;

	// Array of created bonsais
	Bonsai[] public bonsais;
	mapping(uint256 => Bonsai) private tokenToBonsai;
	mapping(address => uint256[]) private tokensPerOwner;

    /// @author Emmanuel Collin
    /// @notice Contract constructor
	constructor() public {
		SUB_CATEGORIES[0] = SUB_CATEGORY_1;
		SUB_CATEGORIES[1] = SUB_CATEGORY_2;
		SUB_CATEGORIES[2] = SUB_CATEGORY_3;

		SPECIES[0] = SPECIES_1;
		SPECIES[1] = SPECIES_2;
		SPECIES[10] = SPECIES_3;
		SPECIES[11] = SPECIES_4;
		SPECIES[20] = SPECIES_5;
		SPECIES[21] = SPECIES_6;
		SPECIES[22] = SPECIES_7;
	}

    /// @author Emmanuel Collin
    /// @return Bonsais count stored in the contract
	function getBounsaiCount() public view returns (uint256) {
		return bonsais.length;
	}

    /// @author Emmanuel Collin
    /// @notice Create a new Bonsai
    /// @dev
    /// @param _name The name of the bonsai. Must be unique.
    /// @param _categoryIndex Bonsai category index
    /// @param _subCategoryIndex Bonsai sub-category index
    /// @param _speciesIndex Bonsai species index
	function createBonsai(string memory _name, uint _categoryIndex, uint _subCategoryIndex, uint _speciesIndex) public isUnique(_name) {
		require(_categoryIndex >= 0 && _categoryIndex < CATEGORIES.length,"Bonsai: Invalid category index");
		require(_subCategoryIndex >= 0 && _subCategoryIndex < SUB_CATEGORIES[_categoryIndex].length,"Bonsai: Invalid sub-category index");
		require(_speciesIndex >= 0 && _speciesIndex < SPECIES[_categoryIndex * 10 + _subCategoryIndex].length,"Bonsai: Invalid species index");
		uint long = bytes(_name).length;
		require(long > 3, "Bonsai: Name must be at least 4 characters long");

		uint256 _species = _categoryIndex * 100 + _subCategoryIndex * 10 + _speciesIndex;
		currentTokenId = SafeMath.add(currentTokenId,1);

		_mint(msg.sender, currentTokenId);

		Bonsai memory _bonsai = Bonsai({ tokenId: currentTokenId, name: _name, species: _species, creationTime: block.timestamp, blockId: block.number, humidityLevel: 75, growth: 1});
		bonsais.push(_bonsai);
		tokenToBonsai[currentTokenId] = _bonsai;
		tokensPerOwner[msg.sender].push(currentTokenId);
	}

    /// @author Emmanuel Collin
    /// @notice Create a new Bonsai with random category, sub-category and species
    /// @dev
    /// @param _name The name of the bonsai. Must be unique.
	function createRandomBonsai(string memory _name) public isUnique(_name) {
		uint long = bytes(_name).length;
		require(long > 3,"Bonsai: Name must be at least 4 characters long");

		uint256 category = random(CATEGORIES.length);
		uint256 subCategory = random(SUB_CATEGORIES[category].length);
		uint256 species = random(SPECIES[category * 10 + subCategory].length );

		createBonsai(_name, category, subCategory, species);
	}

    /// @author Emmanuel Collin
    /// @notice Find a stored bonsai
    /// @dev
    /// @param _tokenId Bonsai Id
	/// @return Index of the bonsai
	/// @return true if the Bonsai exists, otherwise false
	function findToken(uint256 _tokenId) private view returns (uint256, bool) {
		for (uint i = 0; i < bonsais.length; i++) {
			Bonsai memory _bonsai = bonsais[i];
			if (_bonsai.tokenId == _tokenId) return (i, true);
		}
		return (0, false);
	}

    /// @return true if the Bonsai exists in the contract
	function exists(uint256 _tokenId) public view returns (bool) {
		(uint256 index, bool found) = findToken(_tokenId);
		return found;
	}

    /// @author Emmanuel Collin
    /// @notice Destroy a bonsai
    /// @dev
    /// @param tokenId Bonsai Id
	function destroyBonsai(uint tokenId) public isOwnerOfBonsai(tokenId) {
		(uint256 index, bool found) = findToken(tokenId);
		require(found, "Bonsai: Token not found");

		_burn(tokenId);

		if (bonsais.length > 1) {
			bonsais[index] = bonsais[bonsais.length-1];
			delete tokenToBonsai[bonsais[index].tokenId];
			delete bonsais[bonsais.length-1];
		} else {
			delete bonsais;
		}

		if (tokensPerOwner[msg.sender].length > 1) {
			uint256 tokenIndex = 0;
			found = false;
			while (! found) {
				found = tokensPerOwner [msg.sender][tokenIndex] == tokenId;
				if (! found) tokenIndex++;
			}
			if (found) {
				tokensPerOwner [msg.sender][tokenIndex] = tokensPerOwner [msg.sender][tokensPerOwner [msg.sender].length-1];
				delete tokensPerOwner[msg.sender][tokensPerOwner[msg.sender].length-1];
			}
		} else {
			delete tokensPerOwner[msg.sender];
		}
	}
	
    /// @author Emmanuel Collin
	/// @return an array of all bonsai categories
	function getCategories() public view returns (string[] memory) {
		return CATEGORIES;
	}

    /// @author Emmanuel Collin
	/// @param _categoryIndex Index of the category
	/// @return an array of all sub-categories
	function getSubCategories(uint _categoryIndex) public view returns (string[] memory) {
		require(_categoryIndex >= 0 && _categoryIndex < CATEGORIES.length,"Bonsai: Invalid category index");
		return SUB_CATEGORIES[_categoryIndex];
	}

    /// @author Emmanuel Collin
	/// @param _categoryIndex Index of the category
	/// @param _subCategoryIndex Index of the sub-category
	/// @return an array of all species for the specified category and sub-category
	function getSpecies(uint _categoryIndex, uint _subCategoryIndex) public view returns (string[] memory) {
		require(_categoryIndex >= 0 && _categoryIndex < CATEGORIES.length,"Bonsai: Invalid category index");
		require(_subCategoryIndex >= 0 && _subCategoryIndex < SUB_CATEGORIES[_categoryIndex].length,"Bonsai: Invalid sub-category index");
		return SPECIES[_categoryIndex * 10 + _subCategoryIndex];
	}

    /// @author Emmanuel Collin
	/// @notice A modifier to check that a bonsai name is unique in the contract
	/// @param _name the name to check
	modifier isUnique(string memory _name) {
        bool result = true;
        for (uint256 i = 0; i < bonsais.length; i++) {
            if (keccak256(abi.encodePacked(bonsais[i].name)) == keccak256(abi.encodePacked(_name)) ) {
                result = false;
            }
        }
        require(result, "Bonsai: Name already exists");
        _;
    }

    /// @author Emmanuel Collin
	/// @notice A modifier to check that the caller owns the token
	/// @param _tokenId the token to check
	modifier isOwnerOfBonsai(uint256 _tokenId) {
        bool result = ownerOf(_tokenId) == msg.sender;
        require(result, "Bonsai: You do not owns this bonsai");
        _;
    }

    /// @author Emmanuel Collin
    function toBytes(uint256 x) private pure returns (bytes memory b) {
        b = new bytes(32);
        assembly { mstore(add(b, 32), x) }
    }

    /// @author Emmanuel Collin
	/// @notice Random number generator between 0 and _max
	/// @param _max the upper limit of the generated number
	function random(uint _max) private view returns (uint256) {
		uint sum = SafeMath.add(block.timestamp, block.difficulty);
		sum = SafeMath.mod(sum, 23);
		bytes memory _sum = toBytes(sum);
		bytes32 b = keccak256(abi.encodePacked(_sum));
		return uint8(uint(b)) % _max;
	}

    /// @author Emmanuel Collin
	/// @notice Action method on this NFT. Used to watering the bonsai.
	/// @param _tokenId Then token Id
	function watering(uint256 _tokenId) public isOwnerOfBonsai(_tokenId) {
		updateBonsai(_tokenId);
		Bonsai storage _bonsai = tokenToBonsai[_tokenId];
		_bonsai.humidityLevel += 5;
		if (_bonsai.humidityLevel > 100) _bonsai.humidityLevel = 100;

		_bonsai.blockId = block.number;
	}

    /// @author Emmanuel Collin
	/// @notice Action method on this NFT. Used to prune the bonsai.
	/// @param _tokenId Then token Id
	function prune(uint256 _tokenId) public isOwnerOfBonsai(_tokenId) {
		updateBonsai(_tokenId);
		Bonsai storage _bonsai = tokenToBonsai[_tokenId];
		_bonsai.growth += 10;
		_bonsai.blockId = block.number;
	}

    /// @author Emmanuel Collin
	/// @notice get all token ids for a user
	/// @return an array of all caller's tioken id
	function getBonsais() public view returns (uint256[] memory) {
		return tokensPerOwner[msg.sender];
	}

    /// @author Emmanuel Collin
	/// @param tokenId The token Id
	/// @return A Bonsai structure 
	function getBonsai(uint256 tokenId) public view returns (Bonsai memory) {
		Bonsai memory _bonsai = tokenToBonsai[tokenId];
		(_bonsai.growth , _bonsai.humidityLevel) = computeGrowthAndHumidity(_bonsai);

		return _bonsai;
	}

    /// @author Emmanuel Collin
	/// @param _bonsai The bonsai
	/// @return the computed growth of the bonsai 
	/// @return the computed humidity level of the bonsai 
	function computeGrowthAndHumidity(Bonsai memory _bonsai) private view returns (uint256 growth, uint256 humidity) {
		// Calcul de la croissance du bonsai en fonction de l'humidité résiduelle et du temps qui passe
		// Croissance : 
		//  +1 pour chaque bloc
		//  +x en fonction de l'humidité. 
		uint256 _growth = 0;
		uint256 _humidity = _bonsai.humidityLevel;
		for (uint256 blockNumber = _bonsai.blockId; blockNumber <= block.number; blockNumber++) {
			_growth++;
			// Perdre 5 points d'humidité par block
			_humidity -= 5;
			if (_humidity <= 0) _humidity = 0;
			if (_humidity >= 25 && _humidity < 50) {
				_growth += 2;
			}
			if (_humidity >= 50 && _humidity <= 75) {
				_growth += 3;
			}
			if (_humidity > 75) {
				_growth += 2;
			}

		}
		return (_growth, _humidity);
	}

    /// @author Emmanuel Collin
	/// @notice computes new values for humidity and growth
	/// @param tokenId The token id
	function updateBonsai(uint256 tokenId) private {
		Bonsai storage _bonsai = tokenToBonsai[tokenId];

		(_bonsai.growth , _bonsai.humidityLevel) = computeGrowthAndHumidity(_bonsai);

		_bonsai.blockId = block.number;
	}


}