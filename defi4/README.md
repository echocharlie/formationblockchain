Crypto Bonsaï
=============
Alyra - Formation dev blockchain - Défi #4
------------------------------------------

Création d'un smart contract représentant un NFT : Un bonsaï

![Un bonsaï](./bonsai.jpg)

L'onjectif est de créer et entretenir un ou plusieurs bonsaïs.

Un bonsaï peut être
* créé en lui donnant un nom et en choisissant sa catégorie, sous-catégorie et espèce
* créé en lui donnant un nom et en laissant faire le hasard pour choisi sa catégorie, sous-catégorie et espèce
* arrosé
* taillé
* détruit :o(

La croissance du bonsaï est influencée par le taux d'humidité et les tailles qui lui sont appliquées.

# Dépendances
* truffle
* ganache-cli
* OpenZeppelin. "Don't reinvent the wheel"

# Installation
    npm install -g truffle 
    git clone https://gitlab.com/echocharlie/formationblockchain.git && cd formationblockchain && git filter-branch --subdirectory-filter defi4
    cd formationblockchain
    cd SC
    npm install --python="<Chemin complet vers une installation Python 3>"

# Tests
## Lancer Ganache
    ganache-cli -i 5777 -l 10000000 -g 40000000000

## Compilation
    truffle compile

## Déploiement
    truffle migrate

## Lancement des tests
    truflle test

 
