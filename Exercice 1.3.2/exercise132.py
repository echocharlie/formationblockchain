# -*- coding: utf8 -*-


"""
    Exercice 1.3.2 : Trouver la fréquence d'une lettre dans un texte

"""

def frequences(text):

    if not text: return []

    count={}
    count["a"]=1
    for c in text:
        try:
            n = count[c]
            count[c]+=1

        except KeyError as e:
            count[c] = 1
    return count

if __name__ == '__main__':


    text = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus tincidunt est, dictum vehicula nisl suscipit vitae. Praesent lectus arcu, semper ac nisl in, cursus viverra risus. Sed non nunc sit amet augue vestibulum posuere. Etiam a turpis orci. Aliquam augue justo, pulvinar vel posuere sed, fringilla id enim. Suspendisse dignissim ornare felis, vel iaculis metus feugiat consequat. Praesent feugiat, odio eu dapibus venenatis, nisi ex ullamcorper diam, ut accumsan diam lacus in tortor. Phasellus vel venenatis dui, vel ultricies metus. Proin pretium enim in rutrum laoreet.
Duis pellentesque dolor felis, eu lacinia lectus porta eu. Duis feugiat aliquam diam, ac molestie est suscipit sed. Quisque a nunc ut lacus commodo elementum porta non dolor. Curabitur sollicitudin dignissim congue. Praesent at velit condimentum, rutrum nisl ac, lacinia quam. Ut placerat augue quis leo vestibulum tincidunt. Nulla volutpat lacus eget sagittis gravida. Aliquam vitae venenatis libero. Curabitur aliquam semper purus, et bibendum ex sollicitudin a.
Ut in dapibus dui, quis gravida lorem. Aenean maximus rhoncus erat ut volutpat. Nunc et nulla vel quam maximus pharetra ut vel est. Duis tristique aliquam mauris, a porta orci ultricies eu. Etiam at porta eros. Proin euismod nibh arcu, ac commodo metus viverra viverra. Aenean id lectus nec orci tempus luctus. Nulla facilisi. Nam eleifend malesuada sem ut pretium. Aenean molestie dictum magna non dapibus."""

    print(text)
    r = frequences(text)

    print(r)