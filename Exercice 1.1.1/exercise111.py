# -*- coding: utf8 -*-

"""
    Trouver un nombre aléatoire

"""

import random

if __name__ == '__main__':

    random_number = random.randint(1, 100)
    print("Devinez un nombre entre 1 et 100")

    found = False

    while not found:
        input_number = input("> ")
        try:
            number = int(input_number)

            if number > random_number:
                if number - random_number > 5:
                    print("C'est beaucoup moins")
                else:
                    print("C'est moins")
            elif number < random_number:
                if random_number - number > 5:
                    print("C'est beaucoup plus")
                else:
                    print("C'est plus")
            else:
                found = True
        except ValueError as te:
            print("Merci de saisir un nombre")

    print("Exact !")