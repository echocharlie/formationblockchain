class Noeud:
    def __init__(self, v, parent=None):
        self.valeur = v
        self.parent = parent
        self.gauche = None
        self.droite = None

    # Affiche la valeur du noeud et la valeur de ses deux enfants et de son parent
    def toString(self):
        out = "Noeud " + str(self.valeur) + ":  L";
        if (self.gauche is None):
            out += "-"
        else:
            out += str(self.gauche.valeur)
        out += " R";
        if (self.droite is None):
            out += "-"
        else:
            out += str(self.droite.valeur)
        out += " P";
        if (self.parent is None):
            out += "-"
        else:
            out += str(self.parent.valeur)

        print(out)


class Arbre:
    def __init__(self):
        self.racine = None

    # Méthode pour ajouter un noeud
    def ajouterNoeud(self, val):
        if (self.racine == None):
            self.racine = Noeud(val)
        else:
            self._ajouter(val, self.racine)

    def _ajouter(self, val, nd):
        if (val < nd.valeur):
            if (nd.gauche is not None):
                self._ajouter(val, nd.gauche)
            else:
                nd.gauche = Noeud(val, nd)
        else:
            if (nd.droite is not None):
                self._ajouter(val, nd.droite)
            else:
                nd.droite = Noeud(val, nd)

    # Méthode pour trouver une valeur donnée dans un arbre binaire de recherche
    def trouverNoeud(self, val):
        node = self.racine
        found_node = None
        while found_node is None:
            if node.valeur == val:
                found_node = node
            else:
                if node.gauche is not None and node.valeur > val:
                    node = node.gauche
                elif node.droite is not None and node.valeur < val:
                    node = node.droite
                else:
                    break

        return found_node

    def minimum(self, root):
        if root is None or root.gauche is None:
            return root
        return self.minimum(root.gauche)

    def maximum(self, root):
        if root is None or root.droite is None:
            return root
        return self.maximum(root.droite)

    def successor(self, node):
        if node.droite is not None:
            return self.minimum(node.droite)
        succ = None
        root = self.racine
        while root is not None and root.valeur != node.valeur:
            if root.valeur > node.valeur:
                succ = root
                root = root.gauche
            else:
                root = root.droite
        return succ

    def predecessor(self, node):
        if node.gauche is not None:
            return self.maximum(node.gauche)
        else:
            pred = None
            root = self.racine
            while root is not None and root.valeur != node.valeur:
                if root.valeur > node.valeur:
                    root = root.gauche
                else:
                    pred = root
                    root = root.droite
            return pred

    def remove_node(self, node):
        if not node:
            return

        if node.gauche is not None and node.droite is not None:
            # noeud avec 2 enfants
            # Trouver le successeur
            succ = self.successor(node)
            # swapper la valeur du successeur avec celle du noeud à suppimer
            tmp = node.valeur
            node.valeur = succ.valeur
            succ.valeur = tmp
            # supprimer le successeur
            self.remove_node(succ)
        else:
            if node.gauche is None and node.droite is None:
                # c'est une feuille
                if node.parent is not None:
                    if node.parent.gauche == node:
                        node.parent.gauche = None
                    else:
                        node.parent.droite = None
                    del node
            elif node.gauche is not None:
                # 1 seul enfant à gauche
                if node.parent is not None:
                    if node.parent.gauche == node:
                        node.parent.gauche = node.gauche
                    else:
                        node.parent.droite = node.gauche
                    del node
            else:
                # 1 seul enfant à droite
                if node.parent is not None:
                    if node.parent.gauche == node:
                        node.parent.gauche = node.droite
                    else:
                        node.parent.droite = node.droite
                    del node

    # Méthode pour supprimer un noeud
    def supprimerNoeud(self, val):
        # recherche de la valeur dans l'arbre
        node = self.trouverNoeud(val)

        if node is None:
            return

        self.remove_node(node)

    def _infixe(self, node, r):
        if node is None:
            return
        if node.gauche:
            self._infixe(node.gauche, r)
        r.append(str(node.valeur))
        if node.droite:
            self._infixe(node.droite, r)
        return node.valeur

    # Méthode pour afficher l’arbre selon un parcours infixe
    # Cette méthode doit retournée un tableau contenant la valeur des noeuds
    def infixe(self):
        r = []
        self._infixe(self.racine, r)
        return r

    # Méthode pour afficher la valeur d'un noeud à partir de sa valeur
    def printNoeud(self, val):
        noeud = self.trouverNoeud(val)
        if noeud is not None:
            noeud.toString()

if __name__ == '__main__':

    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    node = a.trouverNoeud(24)
    if node is None:
        print("La valeur 24 n'appartient à l'arbre.")
    else:
        print("La valeur " + str(node.valeur) + " appartient à l'arbre.")

    #print(a.infixe())

    print(f"succ de 24 ? {a.successor(a.trouverNoeud(24))}")
    print(f"succ de 32 ? {a.successor(a.trouverNoeud(32))}")
    print(f"pred de 35 ? {a.predecessor(a.trouverNoeud(35))}")
    print(f"pred de 18 ? {a.predecessor(a.trouverNoeud(18))}")

    #a.dump()
    r = a.infixe()
    print(r)

    print(f"12 ? {a.trouverNoeud(12) is not None}")
    print(f"99 ? {a.trouverNoeud(99) is not None}")

    a.supprimerNoeud(11)
    r = a.infixe()
    print(r)
    a.supprimerNoeud(30)
    r = a.infixe()
    print(r)
