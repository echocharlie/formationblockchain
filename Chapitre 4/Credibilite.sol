pragma solidity ^0.6.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";

contract Credibilite {

   using SafeMath for uint256;

   mapping (address => uint256) public cred;
   bytes32[] private devoirs;

   function produireHash(string memory url) public pure returns (bytes32) {
       bytes32 b=keccak256(abi.encodePacked(url));
       return b;
   }

   function remettre(bytes32 dev) public returns (uint) {

       bool found=false;
       uint i=0;
       while (! found && i < devoirs.length) {
           found = devoirs[i] == dev;
           i++;
       }

       require(! found,"Ce devoir a déjà été rendu.");

       uint pos = devoirs.length;
       if (pos == 0) {
           cred[msg.sender] = SafeMath.add(cred[msg.sender], 30);
       } else {
           cred[msg.sender] = SafeMath.add(cred[msg.sender], 10);
       }

       devoirs.push(dev);
       return pos+1;

   }

   function transfer(address destinataire, uint256 valeur) public {
       require(destinataire != msg.sender,"Vous ne pouvez pas vous envoyer de creds");
       require(cred[msg.sender] > valeur,"Vous n'avez pas assez de creds");
       require(valeur > 0,"Montant de cred insufisant");
       require(destinataire != address(0x0));
       require(cred[destinataire] > 0,"Impossible d'envoyer vos creds à ce destinataire");

       cred[msg.sender] = SafeMath.sub(cred[msg.sender], valeur);
       cred[destinataire] = SafeMath.add(cred[destinataire], valeur);
   }

}