﻿async function createMetaMaskDapp() {
    try {
        // Demande à MetaMask l'autorisation de se connecter
        const addresses = await ethereum.enable();
        const address = addresses[0]
        // Connection au noeud fourni par l'objet web3
        const provider = new ethers.providers.Web3Provider(ethereum);
        dapp = { address, provider };
        console.log(dapp)
        return true;
    } catch (err) {
        // Gestion des erreurs
        console.error(err);
        return false;
    }
}

async function balance() {
    console.log("begin balance");
    dapp.provider.getBalance(dapp.address).then((balance) => {
        let etherString = ethers.utils.formatEther(balance);
        console.log("Balance: " + etherString);
        return etherString;
    });
    console.log("end balance");
}

async function lastBlock() {
    dapp.provider.getBlockNumber(dapp.address).then((blockNumber) => {
        
        console.log("Block Number : " + blockNumber);
        return lastBlock;
    });
}

async function gasPrice() {
    dapp.provider.getGasPrice(dapp.address).then((gasPrice) => {
        let priceString = ethers.utils.formatEther(gasPrice);
        console.log("Gas Price : " + priceString);
        return priceString;
    });
}

async function connectAndRefreshData() {
    let connected = await createMetaMaskDapp();
    if (connected) {
        const balance = await dapp.provider.getBalance(dapp.address);
        let etherString = ethers.utils.formatEther(balance);

        const blockNumber = await dapp.provider.getBlockNumber(dapp.address);

        const gasPrice = await dapp.provider.getGasPrice(dapp.address);
        let priceString = ethers.utils.formatEther(gasPrice);

        document.getElementById("balance").innerHTML = etherString;
        document.getElementById("gasprice").innerHTML = priceString;
        document.getElementById("blocknumber").innerHTML = blockNumber;
        document.getElementById("infos").style = "display:block;";
    } else {
        document.getElementById("infos").style = "display:none;";
    }

}