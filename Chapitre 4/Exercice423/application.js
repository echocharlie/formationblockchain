﻿const abi = 
	[
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"internalType": "address",
					"name": "etudiant",
					"type": "address"
				},
				{
					"indexed": false,
					"internalType": "bytes32",
					"name": "hash",
					"type": "bytes32"
				}
			],
			"name": "DevoirRemis",
			"type": "event"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"internalType": "address",
					"name": "student",
					"type": "address"
				},
				{
					"indexed": false,
					"internalType": "bytes32",
					"name": "hash",
					"type": "bytes32"
				}
			],
			"name": "HomeworkReceived",
			"type": "event"
		},
		{
			"constant": false,
			"inputs": [
				{
					"internalType": "bytes32",
					"name": "dev",
					"type": "bytes32"
				}
			],
			"name": "remettre",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"payable": false,
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"constant": false,
			"inputs": [
				{
					"internalType": "address",
					"name": "destinataire",
					"type": "address"
				},
				{
					"internalType": "uint256",
					"name": "valeur",
					"type": "uint256"
				}
			],
			"name": "transfer",
			"outputs": [],
			"payable": false,
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"constant": true,
			"inputs": [
				{
					"internalType": "address",
					"name": "",
					"type": "address"
				}
			],
			"name": "cred",
			"outputs": [
				{
					"internalType": "uint256",
					"name": "",
					"type": "uint256"
				}
			],
			"payable": false,
			"stateMutability": "view",
			"type": "function"
		},
		{
			"constant": true,
			"inputs": [
				{
					"internalType": "string",
					"name": "url",
					"type": "string"
				}
			],
			"name": "produireHash",
			"outputs": [
				{
					"internalType": "bytes32",
					"name": "",
					"type": "bytes32"
				}
			],
			"payable": false,
			"stateMutability": "pure",
			"type": "function"
		}
	]
	;

homeworks = [];

function refreshHomeworkList() {
	let listElem = document.getElementById("homeworks");
	let hw = "";
	for (let i = 0; i < homeworks.length; i++) {
		hw += "<li>Student : " + homeworks[i].student + "&nbsp;&nbsp;Hash :"+homeworks[i].hash+"</li>";
	}
	listElem.innerHTML = "<ul>" + hw + "</ul>";
}

async function createMetaMaskDapp() {
    try {
        // Demande à MetaMask l'autorisation de se connecter
        const addresses = await ethereum.enable();
        const address = addresses[0]
        // Connection au noeud fourni par l'objet web3
        const provider = new ethers.providers.Web3Provider(ethereum);
        dapp = { address, provider };
        console.log(dapp)
        return true;
    } catch (err) {
        // Gestion des erreurs
        console.error(err);
        return false;
    }
}

async function balance() {
    console.log("begin balance");
    dapp.provider.getBalance(dapp.address).then((balance) => {
        let etherString = ethers.utils.formatEther(balance);
        console.log("Balance: " + etherString);
        return etherString;
    });
    console.log("end balance");
}

async function lastBlock() {
    dapp.provider.getBlockNumber(dapp.address).then((blockNumber) => {
        
        console.log("Block Number : " + blockNumber);
        return lastBlock;
    });
}

async function gasPrice() {
    dapp.provider.getGasPrice(dapp.address).then((gasPrice) => {
        let priceString = ethers.utils.formatEther(gasPrice);
        console.log("Gas Price : " + priceString);
        return priceString;
    });
}

async function connectToCredibilite() {
	// Adresse du contrat 
	const address = "0x64486089d919e2d22d87fdc761728d26e1b1a7c3";

	try {
		contratCredibilite = new ethers.Contract(address, abi, dapp.provider.getSigner());
		contratCredibilite.on('HomeworkReceived', (_student, _homeWorkHash) => {
			console.log("homework with hash " + _homeWorkHash + " received from student at " + _student);
			let homework = {
				student: _student,
				hash: _homeWorkHash
			}
			homeworks.push(homework);
			refreshHomeworkList();
		});
	} catch (err) {
		console.error(err);
	}

}

async function getMyCred() {
	if (contratCredibilite != undefined) {
		let maCredibilite = await contratCredibilite.cred(dapp.address);
		console.log("ma cred : " + maCredibilite);
		return maCredibilite;
	}
	return -1;
}

async function sendHomework(homeworkUrl) {
	if (contratCredibilite != undefined) {
		contratCredibilite.produireHash(homeworkUrl).then((homework_hash) => {
			console.log("homework url hash : " + homework_hash);
			contratCredibilite.remettre(homework_hash).then((rank) => {
				console.log("devoir remis");
				document.getElementById("resultMessage").innerHTML = "Homework succesfully sent to BC";
			}).catch(error => {
				console.log('erreur : ' + error.message);
				document.getElementById("resultMessage").innerHTML = "An error occured while submiting your homework : " + error.message;
			});

		});
	}

}

async function refreshCred() {
	let cr = await getMyCred();
	document.getElementById("mycred").innerHTML = cr;
}

async function connectAndRefreshData() {
    let connected = await createMetaMaskDapp();
    if (connected) {
        const balance = await dapp.provider.getBalance(dapp.address);
        let etherString = ethers.utils.formatEther(balance);

        const blockNumber = await dapp.provider.getBlockNumber(dapp.address);

        const gasPrice = await dapp.provider.getGasPrice(dapp.address);
        let priceString = ethers.utils.formatEther(gasPrice);

        document.getElementById("balance").innerHTML = etherString;
        document.getElementById("gasprice").innerHTML = priceString;
        document.getElementById("blocknumber").innerHTML = blockNumber;
        

		await connectToCredibilite();
		await refreshCred();
		document.getElementById("infos").style = "display:block;";

    } else {
        document.getElementById("infos").style = "display:none;";
    }

}

async function handleSubmitHomework(event) {
	console.log("submit " + typeof (event) + " " + event);
	event.preventDefault();
	let url = document.getElementById("inputURL");
	console.log("url:" + url.value);
	await sendHomework(url.value);
	await refreshCred();
}