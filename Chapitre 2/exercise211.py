# -*- coding: utf8 -*-


"""

    Exercice 2.1.1 : Convertir la cible en difficulté (Python 3)
    https://ecole.alyra.fr/mod/quiz/view.php?id=248&forceview=1
    https://forum.alyra.fr/t/exercice-2-1-1-convertir-la-cible-en-difficulte/100

"""
MAX_DIFFICULTY  = (2**16 -1) * 2**208 # 2.7 * 10**67
def calculerDifficulte(target):
    return MAX_DIFFICULTY / target

if __name__ == '__main__':

    # La difficulté est une représentation de la cible.
    # La difficulté est calculée en divisant la cible la plus grande possible ( (2¹⁶ - 1) * 2²⁰⁸ ≈ 2,7 * 10⁶⁷)  avec la cible actuelle.
    # Au lancement de Bitcoin, la difficulté était de 1.
    diff = calculerDifficulte(1147152896345386682952518188670047452875537662186691235300769792000)

    expected_value = 23.5

    print("Difficulté : ",diff)
    print("Valeur attendue : ",expected_value)