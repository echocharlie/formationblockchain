# -*- coding: utf8 -*-


"""

    Exercice 2.1.5 : Calculer la récompense associée à un bloc (Python 3)
    https://ecole.alyra.fr/mod/quiz/view.php?id=222
    https://forum.alyra.fr/t/exercice-2-1-5-calculer-la-recompense-associee-a-un-bloc/104

    Un autre ajustement prévu au lancement du protocole est la diminution de la récompense des mineurs.
    Cette récompense, qui est incluse dans la transaction coinbase avec les frais collectés, est en effet divisée par
    deux (arrondi à l’entier inférieur en Satoshis) tous les 210 000 blocs.
    Elle était initialement de 50 bitcoins et de 12,5 bitcoins aujourd’hui.
    Elle atteindra sa valeur la plus basse de 1 Satoshi avant de valoir indéfiniment 0

"""
import math

BASE_FEE = 50 # BTC
BLOCKS_COUNT = 210000 # Fees àre divided by 2 every 210000 blocs*

SATOCHI=1/100000000

def recompenseBloc(hauteurBloc):
    index = math.floor(hauteurBloc / BLOCKS_COUNT)
    #print("index ",index, " power:",(2 ** index))

    r = BASE_FEE / (2 ** index)
    if r < SATOCHI: return 0.0
    else:
        return round(r,8)

if __name__ == '__main__':
    expected_value = 0.04882812
    r = recompenseBloc(2100001)
    #2100001 -->  0.04882812.

    print("Récompense : ",r)
    print("Valeur attendue : ",expected_value)

    r = recompenseBloc(5880000)
    print("Récompense : ",r)

    print("0 : ",recompenseBloc(0))
    print("210000 : ",recompenseBloc(210000))
    print("2100001 : ",recompenseBloc(2100001))
    print(recompenseBloc(2100001)) # 04882812
    print(recompenseBloc(7000000))  # 0

