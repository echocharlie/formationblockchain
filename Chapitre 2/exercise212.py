# -*- coding: utf8 -*-


"""

    Exercice 2.1.2 : Identifier le niveau de difficulté d'un bloc (Python 3)
    https://ecole.alyra.fr/mod/quiz/view.php?id=245&forceview=1
    https://forum.alyra.fr/t/exercice-2-1-2-identifier-le-niveau-de-difficulte-dun-bloc/101

"""
import binascii

def bin2hex(binary):
    # convert raw binary data to a hex string. also accepts ascii chars (0 - 255)
    return binascii.b2a_hex(binary)

def hex2int(hex_str):
    return int(hex_str, 16)

def bin2int(binary):
    return hex2int(bin2hex(binary))

def bits2target_int(bits_bytes):
    exp = bin2int(bits_bytes[: 1]) # exponent is the first byte
    mult = bin2int(bits_bytes[1:]) # multiplier is all but the first byte
    return mult * (2 ** (8 * (exp - 3)))

MAX_DIFFICULTY  = (2**16 -1) * 2**208 # 2.7 * 10**67
def calculerDifficulte(bits):
    bits_bytes = bytearray.fromhex(bits[2:])

    target = bits2target_int(bits_bytes)

    return MAX_DIFFICULTY / target


if __name__ == '__main__':

    expeted_value = 23, 5
    difficulty = calculerDifficulte("0x1c0ae493")

    print("Difficulté : ",difficulty)
    print("Expected value : ",expeted_value)