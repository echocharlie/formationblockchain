# -*- coding: utf8 -*-


"""

    Exercice 2.1.6 : Calculer le nombre total de Bitcoin à un instant t (Python 3)
    https://ecole.alyra.fr/mod/quiz/view.php?id=213
    https://forum.alyra.fr/t/exercice-2-1-6-calculer-le-nombre-total-de-bitcoins-a-un-instant-t/105

"""
import math

BASE_FEE = 50 * 10**8# BTC
BLOCKS_COUNT = 210000 # Fees àre divided by 2 every 210000 blocs*
SATOCHI=1/100000000

def recompenseBloc(hauteurBloc):
    index = math.floor(hauteurBloc / BLOCKS_COUNT)
    #print("index ",index, " power:",(2 ** index))

    r = BASE_FEE / (2 ** index)
    return round(r,8)

def bitcoinsEnCirculation2(hauteurBloc):
    """
    version non optimisée, mais qui fonctionne
    :param hauteurBloc:
    :return:
    """
    count = BASE_FEE # genesis bloc

    if count > 0:
        for i in range(1,hauteurBloc+1):
            count += recompenseBloc(i)
    return count

def bitcoinsEnCirculation(hauteurBloc):
    count = 0
    current_fees = BASE_FEE

    remaining_blocks = hauteurBloc + 1
    while remaining_blocks > 0:

        if remaining_blocks > BLOCKS_COUNT:
            count = count + current_fees * BLOCKS_COUNT
        else:
            count = count + remaining_blocks * current_fees

        remaining_blocks -= BLOCKS_COUNT
        if current_fees >> 1 < SATOCHI:
            current_fees = 0
        else:
            current_fees = current_fees >> 1

    return round(count * SATOCHI, 8)

if __name__ == '__main__':
    #print(recompenseBloc(210000))

    print("0 -> 50 : ",bitcoinsEnCirculation(0))       # 50
    print("1 -> 100 : ",bitcoinsEnCirculation(1))     # 100
    print("1000000 -> 20187503.125 : ",bitcoinsEnCirculation(1000000))      # 20187503.125
    print("210000 -> 10500025.0 : ",bitcoinsEnCirculation(210000))  #   10500025.0     210000*50 + 1*25
    print("2100001 -> 20979492.28515624 : ",bitcoinsEnCirculation(2100001)) #20979492.28515624

    print("6929998 -> 20999999.97689999 : ", bitcoinsEnCirculation(6929998))
    print("7000000 ->  20999999.9769 : ", bitcoinsEnCirculation(7000000))


