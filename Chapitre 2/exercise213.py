# -*- coding: utf8 -*-


"""

    Exercice 2.1.3 : Vérifier si la difficulté a été ajustée (Python 3)
    https://ecole.alyra.fr/mod/quiz/view.php?id=225
    https://forum.alyra.fr/t/exercice-2-1-3-verifier-si-la-difficulte-a-ete-ajustee/102

"""

def blocReajustement(hauteurBloc):
    return hauteurBloc % 2016 == 0

if __name__ == '__main__':
    expected_value = True

    r = blocReajustement(556416)

    print("Difficulté réajustée : ",r)
    print("Valeur attendue : ",expected_value)