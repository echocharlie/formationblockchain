pragma solidity ^0.6.0;

contract Pulsation {

    uint private battement;
    string private message;

    constructor(string memory _message) public {
        battement = 0;
        message = _message;
    }

    function ajouterBattement() public returns (string memory) {
        battement++;
        return message;
    }
}