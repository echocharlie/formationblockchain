pragma solidity ^0.6.0;
import "./Pulsation.sol";

contract Pendule {

 string[] public balancier;
 Pulsation tac;
 Pulsation tic;

 Pulsation private contratTic = new Pulsation("tic");
 Pulsation private contratTac = new Pulsation("tac");

 function ajouterTacTic(Pulsation _tic, Pulsation _tac)public{
     tic = _tic;
     tac = _tac;
 }

 function mouvementsBalancier(uint nombreBalanciers) public {
     for (uint i=0; i<nombreBalanciers; i++) {
         balancier.push(contratTic.ajouterBattement());
         balancier.push(contratTac.ajouterBattement());
     }
 }
}
