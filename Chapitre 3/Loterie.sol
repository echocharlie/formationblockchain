pragma solidity ^0.6.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";


contract Loterie {

    using SafeMath for uint256;

    // Nombre de blocs à miner avant le tirage au sort
    uint8 private constant BLOCK_THRESHOLD = 10;

    // Durée de la loterie : 5 jours
    uint8 private constant LOTTERY_LENGTH = 5;

    bool tirageEffectue = false;
    uint8[LOTTERY_LENGTH] tirages;

    uint private montantTotalParis = 0;
    uint private blockNumber;

    address payable private owner;
    uint private ticketsGagnants = 0;

    function getNombreTicketsGagnants() public view returns (uint) {
        return ticketsGagnants;
    }

    function getBlockNumber() public view returns (uint) {
        return blockNumber;
    }

    function getCurrentBlockNumber() public view returns (uint) {
        return block.number;
    }


    constructor() public {
        blockNumber = block.number;
        owner = msg.sender;
    }

    // Tickets achetés par les joueurs
    // Clef 2 : Jour (de 1 à 5)
    // Valeur 2 : nombre choisi par le joueur
    // Un joueur peut acheter 1 seul ticket par jour
    mapping(address => mapping(uint8 => uint8)) private tickets;
    address[] private joueurs;

    // Gains de chaque joueur
    mapping(address => uint) private gains;

    // Une lotterie est démarrée tous les 5 jours

    // Tirage au sort réalisé à la fin de la loterie
    // Titage d'un nombre aléatoire pour chacun des 5 jour que dure la loterie
    // Les joueurs peuvent parier à une date tant que le tirage au sort n'a pas eu lieu pour cette date

    // Acheter un billet à 100 finney en choisissant un nombre uint8 pour une date donnée

    // les gains sont partagés entre tous les gagnants. Les gagnats doivent venir récupérer leurs gains, ils ne sont pas envoyés automatiquement
    // à tous les gagnants pour limiter la conso de gas.


    function parier(uint8 jour, uint8 nombre) payable public returns (bool success) {
        require(! tirageEffectue,"Loterie terminée, le tirage a été effectué");
        require(block.number < (blockNumber + BLOCK_THRESHOLD),"La loterie est terminée.");
        require(msg.value == 100 finney , "Le prix du ticket de lotterie est de 100 finneys");
        require(jour >= 1 && jour <= LOTTERY_LENGTH, "Le jour doit être compris entre 1 et 5.");
        require(nombre >= 1 && nombre <= 255, "Le jour doit être >= 1 et <= 255.");

        jour--;
        require(tickets[msg.sender][jour] == 0,"Vous avez déjà parié pour cette journée");

        tickets[msg.sender][jour] = nombre;

        montantTotalParis += msg.value;

        ajouterJoueur(msg.sender);
        return true;
    }

    function calculerGains() private {
        // Calculer le nombre de tickets gagnants

        for (uint8 jour=0; jour<LOTTERY_LENGTH; jour++) {

            for (uint j=0; j<joueurs.length; j++) {
                if (tirages[jour] == tickets[joueurs[j]][jour]) {
                    // le joueur a gagné
                    ticketsGagnants++;
                }
            }
        }

        if (ticketsGagnants > 0) {
            uint gainParTicketGagnant = SafeMath.div(montantTotalParis, ticketsGagnants);

            // Calculer les gains de chaque gagnant
            for (uint8 jour=0; jour<LOTTERY_LENGTH; jour++) {
                for (uint j=0; j<joueurs.length; j++) {
                    if (tirages[jour] == tickets[joueurs[j]][jour]) {
                        // le joueur a gagné
                        gains[joueurs[j]] = SafeMath.add(gains[joueurs[j]], gainParTicketGagnant);
                    }
                }
            }

        }

        if (ticketsGagnants == 0) {
            //Aucun gagnant, on renvoie tous les gains à l'organisateur
            selfdestruct(owner);
        }
    }

    function retirerGain() public payable {
        require(tirageEffectue,"Le tirage au sort n'a pas été réalisé.");
        require(gains[msg.sender] > 0,"Vous n'avez aucune gain à retirer");

        msg.sender.transfer(gains[msg.sender]);

        // RAZ des gains du joueur pour qu'il ne puisse pas faire plusieurs fois le retrait
        gains[msg.sender] = 0;
    }

    function ajouterJoueur(address adresse) private {
        bool found;
        for (uint i=0; i<joueurs.length; i++) {
            found = joueurs[i] == adresse;
            if (found) break;
        }

        if (! found) joueurs.push(adresse);
    }

    function tirage() public  {
        require(tirageEffectue == false,"Tirage au sort déjà effectué.");
        require(block.number >= (blockNumber + BLOCK_THRESHOLD),"La loterie n'est pas terminée.");

        for (uint8 i=0; i<LOTTERY_LENGTH; i++) {
            tirages[i] = random();
        }
        tirageEffectue = true;

        calculerGains();
    }

    function toBytes(uint256 x) private pure returns (bytes memory b) {
        b = new bytes(32);
        assembly { mstore(add(b, 32), x) }
    }


    function random() private view returns (uint8) {

        uint sum = SafeMath.add(block.timestamp, block.difficulty);
        sum = SafeMath.mod(sum, 23);
        bytes memory _sum = toBytes(sum);

        bytes32 b=keccak256(abi.encodePacked(_sum));

        return uint8(uint(b));
    }

    function getMontantTotalParis() public view returns (uint) {
        return montantTotalParis;
    }

}