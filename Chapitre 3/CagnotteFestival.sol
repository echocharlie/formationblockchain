pragma solidity ^0.6.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";



contract CagnotteFestival{

    using SafeMath for uint256;

    mapping (address => uint) private organisateurs;
    address[] private _organisateurs;

    mapping (address => bool) private festivaliers;
    mapping (address => string) private sponsors;

    uint private placesRestantes=500;
    uint private depensesTotales;
    uint private montantSponsorsTotal;

    uint private montantDepensesJour;
    uint private jourDerniereDepense;

    uint private constant DEPENSE_MAX_PAR_JOUR = 20 ether;

    uint private dateFestival;
    uint private dateLiquidation;


 constructor(uint date) public {
   organisateurs[msg.sender] = 100;
   _organisateurs.push(msg.sender);

   dateFestival = date;
   dateLiquidation = dateFestival + 2 weeks;
 }

 function day(uint ts) private view returns (uint) {
     return SafeMath.div(ts, 1 days);
 }

 function getDateLiquidation() public view returns (uint) {
     return dateLiquidation;
 }

 function getDateFestival() public view returns (uint) {
     return dateFestival;
 }

 function retraitOrga() public {
     require(estOrga(msg.sender),"Vous n'êtes pas organisateur de ce festival.");
     require(now >= dateLiquidation,"Vous ne pouvez pas vous retirez avant la fin du festival");

     // Est-ce le dernier organisateur ?
     uint count = 0;
     for (uint i=0;i<_organisateurs.length;i++) {
         if (organisateurs[_organisateurs[i]] > 0) count++;
     }

     if (count == 1) {
         // Solder le compte
         selfdestruct(msg.sender);
     } else {

         // Distribuer à l'organisateur sa part des fonds qu'il placesRestantes
         // uint montant = (montantSponsorsTotal - depensesTotales) * (organisateurs[msg.sender] / 100);

         uint montant = SafeMath.sub(montantSponsorsTotal,depensesTotales);
         montant = SafeMath.mul(montant, SafeMath.div(organisateurs[msg.sender], 100));
         payer(msg.sender, montant);

         organisateurs[msg.sender] = 0;
     }
 }

 function transfererOrga(address orga, uint parts) public {
     require(parts > 0, "Le nombre de parts doit être suppérieur à 0");
     require(organisateurs[msg.sender] - parts >= 0, "Il n'y a pas assez de parts à distribuer");
     require(organisateurs[orga] == 0,"Cet organisateur a djà reçu ses parts.");

     organisateurs[orga] = parts;
     _organisateurs.push(orga);

     organisateurs[msg.sender] -= parts;
 }

 function estOrga(address orga) public view returns (bool){
     return organisateurs[orga] > 0;
 }

 // Achat d'un billet par un festivalier
 function acheterTicket() public payable {
   require(msg.value == 500 finney,"Place à 0.5 Ethers");
   require(placesRestantes > 0,"Plus de places disponibles");
   require(now <= dateLiquidation,"Le festival est terminé");
   festivaliers[msg.sender]=true;
 }

 // Pour qu'un organisateur puisse dépenser l'argent collecté
 function payer(address payable destinataire, uint montant) public payable {
    require(estOrga(msg.sender));
    require(destinataire != address(0));
    require(montant > 0);
    require(SafeMath.sub(montantSponsorsTotal, depensesTotales) >= montant,"Il n'y a pas assez de fonds pour cette dépense");
    // Vérifier que la dépense ne dépasse pas le seuil journalier
    require(controlerDepensesJournalieres(montant),"Dépense refusée, le seuil maximum quotidien est dépassé");

    montantDepensesJour = SafeMath.add(montantDepensesJour, montant);
    jourDerniereDepense = day(now);

    destinataire.transfer(montant);
    comptabiliserDepense(montant);
 }

 function comptabiliserDepense(uint montant) private view {

   depensesTotales.add(montant);
 }

 function sponsoriser(string memory nom) public payable {
     require(msg.value >= 30 ether,"Le minimum est de 30 Ethers");
     require(dateLiquidation > now,"Le festivale est terminé");
     sponsors[msg.sender] = nom;

     montantSponsorsTotal = SafeMath.add(montantSponsorsTotal, msg.value);
 }

 function getMontantSponsorsTotal() public view returns (uint) {
     return montantSponsorsTotal;
 }

 function getPlacesRestantes() public view returns (uint) {
     return placesRestantes;
 }

 function getDepensesTotal() public view returns (uint) {
     return depensesTotales;
 }

 function controlerDepensesJournalieres(uint depense) private returns (bool) {
     // RAZ du montant dépensé par jour si changement de date
     if (day(jourDerniereDepense) != day(now)) {
         montantDepensesJour = 0;
     }

     return (SafeMath.add(montantDepensesJour, depense) <= DEPENSE_MAX_PAR_JOUR);
 }

}