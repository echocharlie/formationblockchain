pragma solidity ^0.5.7;

contract Assemblee {

    struct Decision {
        string description;
        uint votesPour;
        uint votesContre;
        mapping (address => bool) aVote;
        bool ouverte;
    }

    struct Admin {
        string nom;
        address adresse;
    }

    // Administrateurs
    Admin[] admins;

    Decision[] decisions;

 // Membres
 address[] membres;

 constructor(string memory nom) public {
   Admin memory superAdmin;
   superAdmin.nom = nom;
   superAdmin.adresse = msg.sender;

   admins.push(superAdmin);
 }

     function nommerAdmin(string memory nom, address adresse) public {
        require(estSuperAdmin(msg.sender), "Vous n'avez pas le droit de nommer un nouvel administrateur");
        require(! estAdmin(adresse), "Cette personne est déjà administrateur");

        Admin memory newAdmin;
        newAdmin.nom = nom;
        newAdmin.adresse = adresse;

        admins.push(newAdmin);
     }

     function demissionner() public {
         require(estAdmin(msg.sender), "Vous n'êtes pas administrateur");
         require(! estSuperAdmin(msg.sender), "Le super administrateur ne peut pas démissionner.");

         for (uint i = 0; i < admins.length ; i++) {
             if (admins[i].adresse == msg.sender) {
                 delete(admins[i]);
                 break;
             }
         }
     }

     function estAdmin(address adresse) public view returns (bool) {
         bool found = false;
         uint i = 0;
         while (! found && i <admins.length) {
             if (admins[i].adresse == adresse) found = true;
             i++;
         }
         return found;
     }

     function estSuperAdmin(address adresse) public view returns (bool) {
         return admins[0].adresse == adresse;
     }

    function rejoindre() public {
       membres.push(msg.sender);
       membres.push(0xEce1184668C8cF638f27DC1B147FaB25AB6486a6);
       membres.push(0xE85B44288A26Db31c0e1Faa6974748f6Fb8B98b5);
     }

    // Adresse de test : 0xe6054409320b611ea90AcC6c7Aa8841007A23F68
    function estMembre(address utilisateur) public view returns (bool) {
        bool found;
        uint i=0;

        while (!found && i < membres.length) {
            found = (membres[i] == utilisateur);
            i++;
        }
        return found;
    }

    function proposerDecision(string memory description) public {
        require(estMembre(msg.sender), "Il faut être membre!");
        Decision memory decision;
        decision.description = description;
        decision.votesPour = 0;
        decision.votesContre = 0;
        decision.ouverte = true;

        decisions.push(decision);
    }

    function voter(uint indice, bool value) public {
        require(estMembre(msg.sender), "Il faut être membre!");
        require((decisions.length > 0 && (indice >= 0 && indice < decisions.length)), "Cette décision n'existe pas");
        require(decisions[indice].aVote[msg.sender] == false, "Vous avez déjà voté pour cette décision.");
        require(decisions[indice].ouverte, "La décision est fermée, il n'est plus possible de voter");

        if (value) {
            decisions[indice].votesPour++;
        } else {
            decisions[indice].votesContre++;
        }

        decisions[indice].aVote[msg.sender] = true;
    }

    function comptabiliser(uint indice) public view returns (int){
        if (decisions.length > 0) {
            return int(decisions[indice].votesPour) - int(decisions[indice].votesContre);
        }
        return -1;
    }

    function fermerDecision(uint indice) public {
        require(estAdmin(msg.sender), "Il faut être adlinistrateur pour pouvoir fermer une décision !");
        require((decisions.length > 0 && (indice >= 0 && indice < decisions.length)), "Cette décision n'existe pas");

        decisions[indice].ouverte = false;
    }

}