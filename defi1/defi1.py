# -*- coding: utf8 -*-

"""
    Défi #1 : Outil d’analyse bitcoin
    https://ecole.alyra.fr/mod/assign/view.php?id=111
    https://forum.alyra.fr/t/defi-1-outils-d-analyse-bitcoin/46



"""

from flask_bootstrap import Bootstrap
from flask import Flask, flash, redirect, render_template, request, session, abort, url_for
from markupsafe import Markup
import json
import time
from datetime import timedelta
from toolbox import *
from bitcoinclient import *

BLOCKS_PER_PAGE=20


class BlockchainInfo:
    def __init__(self):
        pass

    def tojson(self):
        json.dumps(self, default=lambda o: o.__dict__)

app = Flask(__name__)
app.secret_key = "6d3cad95ff2b8e395f58d3bd9e4375c16f7aeb595f57f965bb9b6f31057dc40f"
bootstrap = Bootstrap(app)
app.permanent_session_lifetime = timedelta(minutes=5)

@app.route("/")
def welcome():
    return render_template('index.html')

@app.route("/block_detail/<string:hash>",methods=["GET"])
def block_detail(hash):
    con = None
    try:
        con = session["con"]
    except KeyError as ke:
        return redirect(url_for('explorateur',err_message="Session expirée"))

    print("hash ",hash)
    try:
        bc = BitcoinClient(con["host"], con["port"], con["username"], con["password"])
        block_info = bc.getblock(hash)
        target = dec2hex(bits2target_int(int(block_info["bits"], 16)))
        block_info["target"] = target
        block_info["bits_dec"] = hex2dec(block_info["bits"])
        block_info["time_fmt"] = time.strftime("%A %d %B %Y %H:%M:%S GMT", time.gmtime(block_info["time"]))
        print(block_info)
        tx_json = {
            "transactions": []
        }

        for tx_hash in block_info["tx"]:
            try:
                transac_info = bc.gettransaction(tx_hash)
                transac_info["time_fmt"] = time.strftime("%A %d %B %Y %H:%M:%S GMT", time.gmtime(transac_info["time"]))
                tx_json["transactions"].append(transac_info)
            except BitcoinClientException as be:
                transac_info = None

        print(tx_json)
        return render_template("block.html", blockinfo=block_info, tx_json=tx_json)
    except BitcoinClientException as bce:
        return render_template("block.html", err_message="Bloc introuvable")


@app.route("/transaction_detail/<string:hash>",methods=["GET"])
def transaction_detail(hash):
    print("hash ",hash)
    try:
        con = session["con"]
    except KeyError as ke:
        return redirect(url_for('explorateur',err_message="Session expirée"))

    try:
        bc = BitcoinClient(con["host"], con["port"], con["username"], con["password"])

        transaction_info = bc.gettransaction(hash)
        transaction_info["time_fmt"] = time.strftime("%A %d %B %Y %H:%M:%S GMT", time.gmtime(transaction_info["time"]))
        return render_template("transaction.html", transaction=transaction_info)
    except BitcoinClientException as be:
        return render_template("transaction.html", err_message="Transaction introuvable")

@app.route("/search_block",methods=["POST"])
def search_block():
    try:
        con = session["con"]
    except KeyError as ke:
        return redirect(url_for('explorateur',err_message="Session expirée"))

    hash = request.form["hash"]
    return redirect(url_for("block_detail",hash=hash))

@app.route("/search_transaction",methods=["POST"])
def search_transaction():
    try:
        con = session["con"]
    except KeyError as ke:
        return redirect(url_for('explorateur',err_message="Session expirée"))

    hash = request.form["hash"]
    return redirect(url_for("transaction_detail",hash=hash))

@app.route("/analiseur",methods=["GET","POST"])
def analiseur():
    error_message = request.args.get("err_message")
    app_message = request.args.get("app_message")
    if request.method == "POST":
        rawdata = request.form["transaction_raw"]


        json = parserawtransaction(rawdata)
        return render_template("analiseur.html", app_message="",json=json)
    else:
        pass

    return render_template("analiseur.html", app_message="")

@app.route("/explorateur",methods=["GET","POST"])
def explorateur():

    error_message = request.args.get("err_message")
    app_message = request.args.get("app_message")
    if request.method == "POST":
        try:
            con = {
                "host": request.form["hostname"],
                "port": request.form["port"],
                "username": request.form["username"],
                "password": request.form["password"]
            }
            session["con"] = con

            bc = BitcoinClient(request.form["hostname"], request.form["port"], request.form["username"], request.form["password"])
            bi = {
                "difficulty": str(bc.getdifficulty()),
                "blockcount": str(bc.getblockcount()),
                "balance": str(bc.getbalance()),
                "blocksperpage": BLOCKS_PER_PAGE
            }
            session["bi"] = bi

            blocks = {
                "blocks": bc.getblocks(index=0, max=BLOCKS_PER_PAGE),
                "block_index": "0"
            }

            app_message = "Connexion à la blockchain réussie"
            #return redirect(url_for('explorateur', err_message="bad"))
            return render_template("explorateur.html", app_message=app_message, blocks=blocks)
        except BitcoinClientException as be:
            error_message = "Impossible de récupérer les informations depuis la blockchain, "+be.message
    else:
        current_index = 0
        action = request.args.get('action')
        if action:
            current_index = int(request.args.get('block_index'))
            print("current_index",current_index)
            if action == "next":
                current_index += BLOCKS_PER_PAGE
            else:
                current_index -= BLOCKS_PER_PAGE
            print("current_index",current_index)

        try:
            if session["con"]:

                bc = BitcoinClient(session["con"]["host"], session["con"]["port"], session["con"]["username"],
                                   session["con"]["password"])
                blocks = {
                    "blocks": bc.getblocks(index=current_index, max=BLOCKS_PER_PAGE),
                    "block_index": str(current_index)
                }
                return render_template("explorateur.html", app_message=request.args.get('app_message'), err_message=error_message, blocks=blocks)
        except KeyError as ke:
            pass

    return render_template("explorateur.html", app_message=request.args.get('app_message'), err_message=error_message)

@app.route("/conversion",methods=["GET","POST"])
def conversion():
    error_message = None
    app_message = None

    if request.method == "POST":
        number = request.form["number"]

        conversion_type = request.form["conversion_type"]

        if number:
            try:
                if conversion_type == "1":
                    # hex --> Dec
                    r = hex2dec(number)
                elif conversion_type == "2":
                    # dec --> hex
                    intnumber = int(number)
                    r = dec2hex(intnumber)
                elif conversion_type == "3":
                    # Hexadécimal little endian -> hexadécimal
                    # test : 0000000000000000000000000000000000000000A51832
                    # résultat attendu : 3218A50000000000000000000000000000000000000000
                    r = hex_littleendian2hex(number)
                elif conversion_type == "4":
                    # varInt -> décimal
                    # test 1 : 210372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
                    # résultat : 33
                    # test 2 : 483045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
                    # résultat : 72
                    r = extractVARINT(number)
                elif conversion_type == "5":
                    # Champ Bits -> Cible correspondante
                    # test Bits =389159077
                    # réultat attendu :
                    # decimal : 4798269179035823348880781507454323228379569035237392384
                    # hexa : 0x3218a50000000000000000000000000000000000000000
                    r = bits2target_int(number)

                elif conversion_type == "6":
                    # Cible -> Difficulté
                    # test : 1147152896345386682952518188670047452875537662186691235300769792000
                    # résultat attendu : 23.5
                    r = calculerDifficulte(int(number)) # number = target
                else:
                    error_message = "Merci de sélectionner une coversion dans la liste proposée"
                    return render_template("conversion.html", input_number=request.args.get('input_number'),
                                           app_message=request.args.get('app_message'), err_message = error_message)

                app_message = f"Le résultat est {r}"
                return redirect(url_for('conversion', input_number=number, app_message=app_message))
            except ValueError as ve:
                error_message = "Votre saisie est incorrecte"
        else:
            return render_template("conversion.html", input_number=request.args.get('input_number'),
                                   app_message=request.args.get('app_message'))

    return render_template("conversion.html", input_number=request.args.get('input_number'), app_message=request.args.get('app_message'), err_message=error_message)


if __name__ == '__main__':
    app.run(host= '0.0.0.0')
