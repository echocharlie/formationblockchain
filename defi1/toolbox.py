# -*- coding: utf8 -*-

"""
convertisseur en ligne big endian litlle endian ; http://www.save-editor.com/tools/wse_hex.html
"""

import math

import binascii
from enum import Enum
from Crypto.Hash import SHA256, RIPEMD160

MAX_DIFFICULTY  = (2**16 -1) * 2**208 # 2.7 * 10**67
BASE_FEE = 50 * 10**8# BTC
BLOCKS_COUNT = 210000 # Fees àre divided by 2 every 210000 blocs*
SATOCHI=1/100000000


class OpCode(int, Enum):
    OP_FALSE = 0x00
    OP_PUSHDATA1 = 0x4c
    OP_PUSHDATA2 = 0x4d
    OP_PUSHDATA4 = 0x4e
    OP_1NEGATE = 0x4f
    OP_TRUE = 0x51
    OP_2 = 0x52
    OP_3 = 0x53
    OP_4 = 0x54
    OP_5 = 0x55
    OP_6 = 0x56
    OP_7 = 0x57
    OP_8 = 0x58
    OP_9 = 0x59
    OP_10 = 0x5a
    OP_11 = 0x5b
    OP_12 = 0x5c
    OP_13 = 0x5d
    OP_14 = 0x5e
    OP_15 = 0x5f
    OP_16 = 0x60

    OP_NOP = 0x61
    OP_IF = 0x63
    OP_NOTIF = 0x64
    OP_ELSE = 0x67
    OP_ENDIF = 0x68
    OP_VERIFY = 0x69
    OP_RETURN = 0x6a

    OP_TOALTSTACK = 0x6b
    OP_FROMALTSTACK = 0x6c
    OP_IFDUP = 0x73
    OP_DEPTH = 116
    OP_DROP = 0x75
    OP_DUP = 0x76
    OP_NIP = 0x77
    OP_OVER = 0x78
    OP_PICK = 0x79
    OP_ROLL = 0x7a
    OP_ROT = 0x7b
    OP_SWAP = 0x7c
    OP_TUCK = 0x7d
    OP_2DROP = 0x6d
    OP_2DUP = 0x6e
    OP_3DUP = 0x6f
    OP_2OVER = 0x70
    OP_2ROT = 0x71
    OP_2SWAP = 0x72

    OP_CAT = 0x7e
    OP_SUBSTR = 0x7f
    OP_LEFT = 0x80
    OP_RIGHT = 0x81
    OP_SIZE = 0x82

    OP_INVERT = 0x83
    OP_AND = 0x84
    OP_OR = 0x85
    OP_XOR = 0x86
    OP_EQUAL = 0x87
    OP_EQUALVERIFY = 0x88

    OP_1ADD = 0x8b
    OP_1SUB = 0x8c
    OP_2MUL = 0x8d
    OP_2DIV = 0x8e
    OP_NEGATE = 0x8f
    OP_ABS = 0x90
    OP_NOT = 0x91
    OP_0NOTEQUAL = 0x92
    OP_ADD = 0x93
    OP_SUB = 0x94
    OP_MUL = 0x95
    OP_DIV = 0x96
    OP_MOD = 0x97
    OP_LSHIFT = 0x98
    OP_RSHIFT = 0x99
    OP_BOOLAND = 0x9a
    OP_BOOLOR = 0x9b
    OP_NUMEQUAL = 0x9c
    OP_NUMEQUALVERIFY = 0x9d
    OP_NUMNOTEQUAL = 0x9e
    OP_LESSTHAN = 0x9f
    OP_GREATERTHAN = 0xa0
    OP_LESSTHANOREQUAL = 0xa1
    OP_GREATERTHANOREQUAL = 0xa2
    OP_MIN = 0xa3
    OP_MAX = 0xa4
    OP_WITHIN = 0xa5

    OP_RIPEMD160 = 0xa6
    OP_SHA1 = 0xa7
    OP_SHA256 = 0xa8
    OP_HASH160 = 0xa9
    OP_HASH256 = 0xaa
    OP_CODESEPARATOR = 0xab
    OP_CHECKSIG = 0xac
    OP_CHECKSIGVERIFY = 0xad
    OP_CHECKMULTISIG = 0xae
    OP_CHECKMULTISIGVERIFY = 0xaf

    OP_CHECKLOCKTIMEVERIFY = 0xb1
    OP_CHECKSEQUENCEVERIFY = 0xb2

    OP_PUBKEYHASH = 0xfd
    OP_PUBKEY = 0xfe
    OP_INVALIDOPCODE = 0xff

    OP_RESERVED = 0x50
    OP_VER = 0x62
    OP_VERIF = 0x65
    OP_VERNOTIF = 0x66
    OP_RESERVED1 = 0x89
    OP_RESERVED2 = 0x8a
    OP_NOP1 = 0xb0
    OP_NOP4 = 0xb3
    OP_NOP5 = 0xb4
    OP_NOP6 = 0xb5
    OP_NOP7 = 0xb6
    OP_NOP8 = 0xb7
    OP_NOP9 = 0xb8
    OP_NOP10 = 0xb9


def calculerDifficulte(target):
    # La difficulté est une représentation de la cible.
    # La difficulté est calculée en divisant la cible la plus grande possible ( (2¹⁶ - 1) * 2²⁰⁸ ≈ 2,7 * 10⁶⁷)  avec la cible actuelle.
    # Au lancement de Bitcoin, la difficulté était de 1.
    return MAX_DIFFICULTY / target

def extractVARINT(hexdata):
    """
    extraction d'une donnée au format VARINT depuis une chaine
    :param hexdata: la chaine d'où les données doivent être extraites
    :return: la valeur du varint
    """
    current_index = 0
    # varint output length
    varint = hexdata[current_index:current_index + 1 * 2]
    current_index += 1 * 2
    if varint.upper() == "FD":
        output = hexdata[current_index:current_index + 2 * 2]
        current_index += 2 * 2
    elif varint.upper() == "FE":
        output = hexdata[current_index:current_index + 4 * 2]
        current_index += 4 * 2
    elif varint.upper() == "FF":
        output = hexdata[current_index:current_index + 8 * 2]
        current_index += 8 * 2
    else:
        output = varint
    return int(output,16)


def target_int2bits(target):
    target_hex = int2hex(target)

    bits = "00" if (hex2int(target_hex[: 2]) > 127) else ""
    bits += target_hex # append
    bits = hex2bin(bits)
    length = int2bin(len(bits), 1)

    bits += hex2bin("0000")

    bits = bits[: 3]

    return length + bits

def bits2target_int(intnumber):
    hexa = hex(int(intnumber))
    bits_bytes = bytearray.fromhex(hexa[2:])
    exp = bin2int(bits_bytes[: 1])
    #print("exp ",exp)
    mult = bin2int(bits_bytes[1:])
    #print("mult : ",mult, hex(mult))
    return mult * (2 ** (8 * (exp - 3)))

def int2hex(intval):
    hex_str = hex(intval)[2:]
    if hex_str[-1] == "L":
        hex_str = hex_str[: -1]
    if len(hex_str) % 2:
        hex_str = "0" + hex_str
    return hex_str

def hex2int(hex_str):
    return int(hex_str, 16)

def hex2bin(hex_str):
    return binascii.a2b_hex(hex_str)

def int2bin(val, pad_length = False):
    hexval = int2hex(val)
    if pad_length: # specified in bytes
        hexval = hexval.zfill(2 * pad_length)
    return hex2bin(hexval)

def bin2hex(binary):
    # convert raw binary data to a hex string. also accepts ascii chars (0 - 255)
    return binascii.b2a_hex(binary)

def bin2int(binary):
    return hex2int(bin2hex(binary))



def hex2dec(hexnumber):
    try:
        return int(hexnumber, 16)
    except ValueError as ve:
        return 0


def to_binary(number, bigendian=True):
    # combien faut il de bits pour représenter number ? (dans la limite de 1024 bits)
    for i in range(1024):
        if math.pow(2, i) > number:
            break
    result = [0] * (math.ceil(i / 8) * 8)

    last = -1
    index = len(result) - 1
    while number > 0:
        #print ("1",index, math.pow(2,index))
        if  math.pow(2, index) > number:
            last = math.pow(2, index)
            #print("last",last)
        else:
            #print("2",last, number)
            number -= math.pow(2, index)
            result[len(result)-1-index] = 1
        index-=1

    if not bigendian:
        result = result[16:20] + result[20:24] + result[8:12] + result[12:16] + result[0:4] + result[4:8]

    return result

HEXA = "0123456789ABCDEF"

def to_hex(binary_number):
    result = ""
    index = len(binary_number) - 1
    bit_n = 0
    sum = 0
    while index >= 0:
        if bit_n > 3:
            bit_n = 0
            result = HEXA[sum] + result
            sum = 0

        sum += int(math.pow(2, bit_n)) * binary_number[index]

        index -= 1
        bit_n += 1
    result = HEXA[sum] + result

    return "0x"+result

def dec2hex(number):
    return to_hex(to_binary(number, bigendian=True))


def hex_littleendian2hex(hexnumber):
    r = ""
    i = 0
    while i < len((hexnumber)):
        r = hexnumber[i] + hexnumber[i+1] + r
        i += 2
    return r

def hexStrEndianSwap(theString):

    """Rearranges character-couples in a little endian hex string to
    convert it into a big endian hex string and vice-versa. i.e. 'A3F2'
    is converted to 'F2A3'

    @param theString: The string to swap character-couples in
    @return: A hex string with swapped character-couples. -1 on error."""

    # We can't swap character couples in a string that has an odd number
    # of characters.
    if len(theString) % 2 != 0:
        return -1

    # Swap the couples
    swapList = []
    for i in range(0, len(theString), 2):
        swapList.insert(0, theString[i:i + 2])

    # Combine everything into one string. Don't use a delimeter.
    return ''.join(swapList)

def extractbytesfromstr(string, bytescount):
    """
    :param string: la chaîne d'où extraire les données
    :param bytescount: le nombre d'octets à extraire
    :return: les octets extraits
    """
    return string[:bytescount*2]



def parserawtransaction(rawtransaction):
    """
    Parsing d'une transaction au format RAW.
    Référence : https://en.bitcoin.it/wiki/Protocol_documentation#tx
    :param rawtransaction:
    :return: Document JSON avec les données de la transaction
    """
    print("parsing raw transaction")
    json = {}
    # taille de la transaction
    json["size"] = len(rawtransaction) / 2
    # version sur 4 octets
    json["version"] = extractbytesfromstr(rawtransaction,4)
    rawtransaction = rawtransaction[4*2:]

    # flag optional uint8_t[2] 0 ou 2 octets
    flag = False
    flagStr = extractbytesfromstr(rawtransaction,2)
    if flagStr == "0001":
        flag = True
        json["flag"] = flagStr
        rawtransaction = rawtransaction[2*2:]

    # tx_in count 	var_int
    size=extractVARINT(rawtransaction)
    json["tx_in_count"] = size
    rawtransaction = rawtransaction[1 * 2:]
    json["vin"] = []
    # 41+ 	tx_in 	tx_in[] 	A list of 1 or more transaction inputs or sources for coins
    for i in range(json["tx_in_count"]):
        txin = {}

        json["vin"].append(txin)
        # 36 	previous_output 	outpoint 	The previous output transaction reference, as an OutPoint structure
        # outpoint :
        # hash sur 32 octets
        hash = hexStrEndianSwap(extractbytesfromstr(rawtransaction, 32))
        rawtransaction = rawtransaction[32 * 2:]
        # index sur 4 octets
        index = hexStrEndianSwap(extractbytesfromstr(rawtransaction, 4))
        rawtransaction = rawtransaction[4 * 2:]
        txin["hash"] = hash
        txin["index"] = index

        # 1+ 	script length 	var_int 	The length of the signature script
        varint = extractVARINT(rawtransaction)
        txin["script_length"]=varint # en DECIMAL
        rawtransaction = rawtransaction[1 * 2:]
        # signature script 	uchar[] 	Computational Script for confirming transaction authorization
        txin["scriptSig"] = extractbytesfromstr(rawtransaction,varint)
        rawtransaction = rawtransaction[varint * 2:]

        stack = extract_script(txin["scriptSig"])
        print("\n\nstack'",stack,"'")

        # sequence uint32_t
        txin["sequence"] = int(extractbytesfromstr(rawtransaction, 4),16)
        rawtransaction = rawtransaction[4 * 2:]

    varint = extractVARINT(rawtransaction)
    rawtransaction = rawtransaction[1 * 2:]
    json["tx_out_count"] = varint
    json["vout"] = []
    for i in range(json["tx_out_count"]):
        txout = {}
        json["vout"].append(txout)
        # 8 	value 	int64_t 	Transaction Value
        txout["value"] = int(hexStrEndianSwap(extractbytesfromstr(rawtransaction, 8)),16) * SATOCHI
        rawtransaction = rawtransaction[8 * 2:]

        #pk_script length 	var_int 	Length of the pk_script
        varint = extractVARINT(rawtransaction)
        txout["pl_script_length"]=varint # en DECIMAL
        rawtransaction = rawtransaction[1 * 2:]
        # pk_script 	uchar[] 	Usually contains the public key as a Bitcoin script setting up conditions to claim this output.
        txout["pk_script"] = extractbytesfromstr(rawtransaction,varint)
        rawtransaction = rawtransaction[varint * 2:]

        txout["script_code"] = extract_script(txout["pk_script"])


    if flag:
        # 0+ 	tx_witnesses 	tx_witness[] 	A list of witnesses, one for each input; omitted if flag is omitted above
        witness_count = extractVARINT(rawtransaction)
        rawtransaction = rawtransaction[1 * 2:]
        json["txinwitness"] = []
        for i in range(witness_count):

            varint = extractVARINT(rawtransaction)
            rawtransaction = rawtransaction[1 * 2:]
            witness_raw_data = extractbytesfromstr(rawtransaction, varint)
            json["txinwitness"].append(witness_raw_data)
            rawtransaction = rawtransaction[varint * 2:]

    # lock_time uint32_t sur 4 octets
    json["lock_time"] = extractbytesfromstr(rawtransaction,4)
    rawtransaction = rawtransaction[4 * 2:]

    return json


def hash160(number):
    sha256 = SHA256.new()
    ripemd = RIPEMD160.new()

    sha256.update(bytearray.fromhex(number))

    ripemd.update(bytes(sha256.digest()))
    return ripemd.hexdigest()

def extract_script(script):
    stack = []
    # décodage du script
    tmp_script = script
    bytes_to_remove = 0
    while tmp_script != "":
        opcode = int("0x"+tmp_script[:2],16)

        if opcode >= 0x01 and opcode <= 0x4b:
            # opcode = put <opcode> bytes on top of the stack
            stack.append(tmp_script[2:opcode*2 + 2])
            bytes_to_remove = opcode + 1
        else:
            # recherche de l'opcode dans la liste
            foundopcode = None
            for op in OpCode:
                if op == opcode:
                    foundopcode = op
                    break
            if foundopcode:
                stack.append(foundopcode.name)
                bytes_to_remove = 1

        tmp_script = tmp_script[bytes_to_remove * 2:]

    return stack

def runscript(stack):
    if len(stack) == 0: return None
    runstack = []
    for op in stack:
        if  op == OpCode.OP_DUP:
            runstack.append(runstack[-1])
        elif op == OpCode.OP_CHECKSIG:
            runstack.append(True)
        elif op == OpCode.OP_EQUALVERIFY:
            v1 = runstack.pop()
            v2 = runstack.pop()
            runstack.append(v1 == v2)
        elif op == OpCode.OP_HASH160:
            r = hash160(runstack[-1])
            runstack.append(r)
        else:
            runstack.append(op)

        #print(runstack)

    return runstack[-1]

def verificationP2PKH(ScriptSig, ScriptPubSig):

    stack = []
    if ScriptSig.startswith("0x"):
        ScriptSig = ScriptSig[2:]
    if ScriptPubSig.startswith("0x"):
        ScriptPubSig = ScriptPubSig[2:]

    stack += extract_script(ScriptSig)
    print(stack)
    stack += extract_script(ScriptPubSig)
    print(stack)

    r = runscript(stack)

    return True

if __name__ == '__main__':
    rawtransaction="02000000000101d0696fa4e7eb18130faa62024f8cb3e4a81c12dc07592b547fb4da573b27d18f00000000171600149345d82c8798b233df03fd621acbf79f475c65f6feffffff020065cd1d0000000017a914c97ccae6822d00edac17e6156df748bdff0c2eaa870880380c0100000017a914dc6c1104cd9f91339a014f688728ad22f8902aed8702473044022000ad4b59350cdb8c87b5affeb37c0e23dc499f689569a339bf43b2dd37c7811102202ef845adc31385c3d08c195f5db8712180b0ef36fdd5b48dd7496cf6ac7fe94a012103b8f76bc26d76707cea7fabd5f610773904899b64b9fe44fca1346aff8478896765000000"
    """
    valeurs attendues :
        version : 2 (int signé)
        size : 247
    """
    """
    json = parserawtransaction(rawtransaction)
    print(json)
    """
    rawtransaction="01000000016DBDDB085B1D8AF75184F0BC01FAD58D1266E9B63B50881990E4B40D6AEE3629000000008B483045022100F3581E1972AE8AC7C7367A7A253BC1135223ADB9A468BB3A59233F45BC578380022059AF01CA17D00E41837A1D58E97AA31BAE584EDEC28D35BD96923690913BAE9A0141049C02BFC97EF236CE6D8FE5D94013C721E915982ACD2B12B65D9B7D59E20A842005F8FC4E02532E873D37B96F09D6D4511ADA8F14042F46614A4C70C0F14BEFF5FFFFFFFF02404B4C00000000001976A9141AA0CD1CBEA6E7458A7ABAD512A9D9EA1AFB225E88AC80FAE9C7000000001976A9140EAB5BEA436A0484CFAB12485EFDA0B78B4ECC5288AC00000000"
    json = parserawtransaction(rawtransaction)
    print(json)

    scriptpubsig="483045022100F3581E1972AE8AC7C7367A7A253BC1135223ADB9A468BB3A59233F45BC578380022059AF01CA17D00E41837A1D58E97AA31BAE584EDEC28D35BD96923690913BAE9A0141049C02BFC97EF236CE6D8FE5D94013C721E915982ACD2B12B65D9B7D59E20A842005F8FC4E02532E873D37B96F09D6D4511ADA8F14042F46614A4C70C0F14BEFF5"
    print(extract_script(scriptpubsig))

    scriptkey="76A9141AA0CD1CBEA6E7458A7ABAD512A9D9EA1AFB225E88AC"
    print(extract_script(scriptkey))

    """
    for op in OpCode:
        print(op)
    """