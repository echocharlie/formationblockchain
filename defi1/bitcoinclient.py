# -*- coding: utf8 -*-


from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import logging
import socket


class BitcoinClientException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return f"{self.message}"

    def __repr__(self):
        return '<%s \'%s\'>' % (self.__class__.__name__, self)


class BitcoinClient:

    def __init__(self, host="192.168.0.17", port="16591", username="rpca", password="rpca"):
        self.host = host
        self.port = port
        self.username = username
        self.password = password

        self._connection = None

    def _getconnection(self):
        if not self._connection:
            self._connection = AuthServiceProxy(f"http://{self.username}:{self.password}@{self.host}:{self.port}")
        return self._connection

    def getpeerinfo(self):
        try:
            return self._getconnection().getpeerinfo()
        except JSONRPCException as e:
            raise BitcoinClientException(e.message)
        except socket.timeout as ti:
            raise BitcoinClientException("Unable to connect to the bitcoin node")

    def getbalance(self):
        return self._getconnection().getbalance()

    def getblockcount(self):
        return self._getconnection().getblockcount()

    def getdifficulty(self):
        try:
            return self._getconnection().getdifficulty()

        except JSONRPCException as e:
            raise BitcoinClientException(e.message)

        except OSError as ti:
            raise BitcoinClientException("Unable to connect to the bitcoin node")

    def getblock(self, hash):
        if hash is None: return None
        try:
            return self._getconnection().getblock(hash)
        except JSONRPCException as e:
            raise BitcoinClientException(e.message)
        except socket.timeout as ti:
            raise BitcoinClientException("Unable to connect to the bitcoin node")

    def gettransaction(self, txid, decode=True):
        if hash is None: return None
        try:
            return self._getconnection().gettransaction(txid, False, decode)
        except JSONRPCException as e:
            raise BitcoinClientException(e.message)
        except socket.timeout as ti:
            raise BitcoinClientException("Unable to connect to the bitcoin node")

    def getrawtransaction(self, txid):
        if hash is None: return None
        try:
            return self._getconnection().getrawtransaction(txid)
        except JSONRPCException as e:
            raise BitcoinClientException(e.message)
        except socket.timeout as ti:
            raise BitcoinClientException("Unable to connect to the bitcoin node")

    def getblocks(self, index=0, max=10):
        count = self.getblockcount()
        if not count or count == 0:
            return None

        hashes = []
        # dernier bloc a l'index count-1
        if index + max > count-1:
            _max = count - index
        else:
            _max = max
        print(_max)
        for i in range(_max):
            hashes.append(self._getconnection().getblockhash(index+i))

        return hashes

if __name__ == '__main__':
    logging.basicConfig()
    logging.getLogger("BitcoinRPC").setLevel(logging.DEBUG)

    host = "192.168.70.115"
    port = "16591"
    username = "rpca"
    password = "rpca"

    bc = BitcoinClient(host,port,username, password)

    print(bc.getpeerinfo())

    print(bc.getbalance())
    print(bc.getblockcount())
    print(bc.getdifficulty())

    print("blocs")
    print(bc.getblocks(index=10))

    block_hash = bc.getblocks(index=0)[1]
    block = bc.getblock(block_hash)
    tx_hash = block["tx"][0]
    print("block")
    print(block)
    print("transaction hash : ",tx_hash)
    tx = bc.gettransaction(tx_hash)
    print("transaction")
    print(tx)


    # transactions du bloc 00e7495d4f03f82ff33605c1cc8651dad173d6b97d57b1bc88bfa34fa61b516b
    tx = bc.gettransaction("fa4d3aef42e13aee79ba8c3856c76b9000b89928497d0e5e673d0161d4b0a1b8")
    print(tx)
    tx = bc.getrawtransaction("fa4d3aef42e13aee79ba8c3856c76b9000b89928497d0e5e673d0161d4b0a1b8")
    print(tx)
    """ résultat : 
    020000000001010000000000000000000000000000000000000000000000000000000000000000
    ffffffff0502c4000101ffffffff0200f902950000000017a91476ec36c28fa983f21d294d64d259e6d934f80881870000000000000000266a24aa21a9ede2f61c3f71d1defd3fa999dfa36953755c690689799962b48bebd836974e8cf90120000000000000000000000000000000000000000000000000000000000000000000000000
    """

    for i in range(10):
        print(i)