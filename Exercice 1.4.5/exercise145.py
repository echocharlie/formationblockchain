# -*- coding: utf8 -*-


"""

    Exercice 1.4.5 : Calcultatrice en notation polonaise inversée
    https://ecole.alyra.fr/mod/assign/view.php?id=68
    https://forum.alyra.fr/t/exercice-1-4-5-ecrire-une-calculatrice-en-polonaise-inversee/88/2

"""

class Calc:
    """
     p      Prints the value on the top of the stack, without altering the stack.  A newline is printed after the value.
     f      Prints  the  entire contents of the stack without altering anything.  This is a good command to use if you are lost or want to figure out what the effect
              of some command has been.
     c      Clears the stack, rendering it empty.

    """


    def __init__(self):
        self.stack = []

    def addinput(self, input):
        if input == "p":
            self.displaytopofstack()
        elif input == "f":
            self.displaystack()
        elif input == "c":
            self.stack.clear()
        elif input in ["+","-","*","/"]:
            if len(self.stack) >= 2:
                op2 = self.stack.pop()
                op1 = self.stack.pop()
                r = eval(str(op1)+input+str(op2))
                self.stack.append(r)
                self.displaytopofstack()
            else:
                print("Not enough operands in the stack")

        elif input.isnumeric():
            self.stack.append(input)
        else:
            # ignore other inputs
            pass

    def displaystack(self):
        if len(self.stack) == 0:
            print("Empty stack")
        else:
            for i in self.stack:
                print(i)

    def displaytopofstack(self):
        if len(self.stack) == 0:
            print("stack is empty")
        else:
            print(self.stack[-1])

    def getresult(self):
        if len(self.stack) == 0: return 0

        return self.stack[-1]

if __name__ == '__main__':

    calc = Calc()
    while True:
        user_input = input("> ")
        calc.addinput(user_input)



