# -*- coding: utf8 -*-


"""

    Exercice 1.4.1 : Convertir un nombre décimal en hexadécimal
    https://ecole.alyra.fr/mod/assign/view.php?id=64

"""
import math



def to_binary(number, bigendian=True):
    # combien faut il de bits pour représenter number ? (dans la limite de 1024 bits)
    for i in range(1024):
        if math.pow(2, i) > number:
            break
    result = [0] * (math.ceil(i / 8) * 8)

    last = -1
    index = len(result) - 1
    while number > 0:
        #print ("1",index, math.pow(2,index))
        if  math.pow(2, index) > number:
            last = math.pow(2, index)
            #print("last",last)
        else:
            #print("2",last, number)
            number -= math.pow(2, index)
            result[len(result)-1-index] = 1
        index-=1

    if not bigendian:
        result = result[16:20] + result[20:24] + result[8:12] + result[12:16] + result[0:4] + result[4:8]

    return result

HEXA = "0123456789ABCDEF"

def to_hex(binary_number):
    result = ""
    index = len(binary_number) - 1
    bit_n = 0
    sum = 0
    while index >= 0:
        if bit_n > 3:
            bit_n = 0
            result = HEXA[sum] + result
            sum = 0

        sum += int(math.pow(2, bit_n)) * binary_number[index]

        index -= 1
        bit_n += 1
    result = HEXA[sum] + result

    return "0x"+result


def conversion(number):
    print(f"{number} --> {to_hex(to_binary(number, bigendian=True))} (big endian)")
    print(f"--> {to_hex(to_binary(number, bigendian=False))} (little endian)")


if __name__ == '__main__':

    print(to_binary(365))
    print(to_binary(365,bigendian=False))

    print(to_hex(to_binary(365)))
    print(to_hex(to_binary(365,bigendian=False)))

    print(to_binary(466321))
    print(to_binary(466321,bigendian=False))
    print(to_hex(to_binary(466321)))
    print(to_hex(to_binary(466321,bigendian=False)))

    # vérification
    print((365).to_bytes(2, byteorder='big').hex())
    print((365).to_bytes(2, byteorder='little').hex())

    conversion(466321)
