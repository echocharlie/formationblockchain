pragma solidity ^0.6.0;


contract CanalDePaiement {
  
    string public constant name = "CanalDePaiement";
    address public owner;

    enum EtatCanal {
        VIDE,
        ACTIF,
        ENCOURSFERMETURE,
        FERME
    }

    address public partieA;
    address public partieB;
    uint public montant;
    EtatCanal public etat;
    uint blocFermeture;
    uint dernierNonce;
    uint equilibreA;
    uint equilibreB;


    constructor(address _partieA, address _partieB, uint _montant) public {
        owner = msg.sender;
        require(_partieA != address(0),"CanalDePaiement: adresse invalide pour partieA");
        require(_partieB != address(0),"CanalDePaiement: adresse invalide pour partieB");
        require(_montant > 0,"CanalDePaiement: le montant doit etre superieur à 0");

        partieA = _partieA;
        partieB = _partieB;
        montant = _montant;

        etat = EtatCanal.VIDE;
    }

}