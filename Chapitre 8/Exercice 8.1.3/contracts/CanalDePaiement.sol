pragma solidity ^0.6.0;


contract CanalDePaiement {
  
    string public constant name = "CanalDePaiement";
    address public owner;

    enum EtatCanal {
        VIDE,
        ACTIF,
        ENCOURSFERMETURE,
        FERME
    }

    address public partieA;
    address public partieB;
    uint public montant;
    EtatCanal public etat;
    uint blocFermeture;
    uint dernierNonce;
    uint equilibreA;
    uint equilibreB;


    constructor(address _partieA, address _partieB, uint _montant) public {
        owner = msg.sender;
        require(_partieA != address(0),"CanalDePaiement: adresse invalide pour partieA");
        require(_partieB != address(0),"CanalDePaiement: adresse invalide pour partieB");
        require(_montant > 0,"CanalDePaiement: le montant doit etre superieur à 0");

        partieA = _partieA;
        partieB = _partieB;
        montant = _montant;

        etat = EtatCanal.VIDE;
    }

    function financer() public payable {
        require(msg.sender == partieA || msg.sender == partieB,"CanalDePaiement: Vous n'etes pas autorise sur ce canal de paiement");
        require(msg.value > 0,"CanalDePaiement: Financement incorrect");
        require( ((msg.sender == partieA) && (equilibreA + msg.value <= montant)) || ((msg.sender == partieB) && (equilibreB + msg.value <= montant)),"CanalDePaiement: Financement incorrect");
  
        if (msg.sender == partieA) {
            equilibreA = msg.value;
        } else {
            equilibreB = msg.value;
        }

        if (equilibreA > 0 && equilibreB > 0) {
            etat = EtatCanal.ACTIF;
        }
    }

    function message(uint _a, uint _b, bytes4 _nonce) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(_a, _b, _nonce));
    }

}