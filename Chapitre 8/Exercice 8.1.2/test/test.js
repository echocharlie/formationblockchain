const {
  BN,           // Big Number support
  balance,
  time,
  ether,
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');

const { expect } = require('chai');

const CanalDePaiement = artifacts.require('CanalDePaiement');

contract('Canal de paiement', function (accounts) {
 const _name = 'CanalDePaiement';
 const _owner = accounts[0];

 async function getBalance(from) {
    if (from.tracker != undefined) {
        const r = await from.tracker.get();
        return r;
    } else {
        return -1;
    }
 }

// cf https://github.com/ethereum/wiki/wiki/JavaScript-API#web3ethgettransactionreceipt
 function dumpTxInfos(result) {
    console.log(result);
 }


   before(async function () {
	 
    this.CanalDePaiementInstance = await CanalDePaiement.new(accounts[1], accounts[2], ether('5'),{from: _owner});
    console.log("Created CanalDePaiementInstance at "+this.CanalDePaiementInstance.address);

    console.log(CanalDePaiement.defaults());
    this.owerOfContracts = CanalDePaiement.defaults().from;

    this.trackerContract = await balance.tracker(this.CanalDePaiementInstance.address);
  });

  beforeEach(async function () {
    console.log("\n");
 });

  afterEach(async function () {
 });

  after(async function () {
  console.log("\nTESTS END");
 });

  it('has a name', async function () {
    expect(await this.CanalDePaiementInstance.name()).to.equal(_name);
  });

  it("Should make first account an owner", async function () {
    expect(await this.CanalDePaiementInstance.owner()).to.equal(_owner);
  });

  it("Should not allow to deploy contract with invalid address", async function () {
    await expectRevert(CanalDePaiement.new(constants.ZERO_ADDRESS, accounts[2], ether('5'),{from: _owner}),"CanalDePaiement: adresse invalide pour partieA");
    await expectRevert(CanalDePaiement.new(accounts[1], constants.ZERO_ADDRESS, ether('5'),{from: _owner}),"CanalDePaiement: adresse invalide pour partieB");
  });

  it("Should not allow to deploy contract with insuficient funds", async function () {
    await expectRevert(CanalDePaiement.new(accounts[1], accounts[2], ether('0'),{from: _owner}),"CanalDePaiement: le montant doit etre superieur à 0");
  });

  it("Should be in empty state", async function () {
    const state = await this.CanalDePaiementInstance.etat.call();
    console.log(state);
    expect(state).to.be.bignumber.equal(new BN('0'));
  });
  
  it("Should not allow a non registred user to use the channel", async function () {
    await expectRevert(this.CanalDePaiementInstance.financer({from: accounts[3], value: ether('0')}),"CanalDePaiement: Vous n'etes pas autorise sur ce canal de paiement");
  });
  it("Should not allow to send 0 ethers", async function () {
    await expectRevert(this.CanalDePaiementInstance.financer({from: accounts[1], value: ether('0')}),"CanalDePaiement: Financement incorrect");
  });
  it("Should allow to send correct amount of ethers", async function () {
    await expect(this.CanalDePaiementInstance.financer({from: accounts[1], value: ether('3')}));
    await expect(this.CanalDePaiementInstance.financer({from: accounts[2], value: ether('5')}));
  });
  it("Should be in active state", async function () {
    const state = await this.CanalDePaiementInstance.etat.call();
    console.log(state);
    expect(state).to.be.bignumber.equal(new BN('1'));
  });
  it("Should not allow to send more ethers than expected", async function () {
    await expectRevert(this.CanalDePaiementInstance.financer({from: accounts[1], value: ether('6')}),"CanalDePaiement: Financement incorrect");
  });
 });
