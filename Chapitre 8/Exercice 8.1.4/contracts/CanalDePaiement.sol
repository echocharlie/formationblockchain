pragma solidity ^0.6.0;

import "@openzeppelin/contracts/cryptography/ECDSA.sol";

contract CanalDePaiement {
  
    string public constant name = "CanalDePaiement";
    address public owner;

    enum EtatCanal {
        VIDE,
        ACTIF,
        ENCOURSFERMETURE,
        FERME
    }

    address public partieA;
    address public partieB;
    uint public montant;
    EtatCanal public etat;
    uint blocFermeture;
    uint dernierNonce;
    uint equilibreA;
    uint equilibreB;


    constructor(address _partieA, address _partieB, uint _montant) public {
        owner = msg.sender;
        require(_partieA != address(0),"CanalDePaiement: adresse invalide pour partieA");
        require(_partieB != address(0),"CanalDePaiement: adresse invalide pour partieB");
        require(_montant > 0,"CanalDePaiement: le montant doit etre superieur à 0");

        partieA = _partieA;
        partieB = _partieB;
        montant = _montant;

        etat = EtatCanal.VIDE;
    }

    function financer() public payable {
        require(msg.sender == partieA || msg.sender == partieB,"CanalDePaiement: Vous n'etes pas autorise sur ce canal de paiement");
        require(msg.value > 0,"CanalDePaiement: Financement incorrect");
        require( ((msg.sender == partieA) && (equilibreA + msg.value <= montant)) || ((msg.sender == partieB) && (equilibreB + msg.value <= montant)),"CanalDePaiement: Financement incorrect");
        require(etat == EtatCanal.VIDE,"CanalDePaiement: Ce canal est actif ou clos");
        if (msg.sender == partieA) {
            equilibreA += msg.value;
        } else {
            equilibreB += msg.value;
        }

        if (equilibreA > 0 && equilibreB > 0) {
            etat = EtatCanal.ACTIF;
        }
    }

    function message(uint _a, uint _b, uint _nonce) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(_a, _b, _nonce));
    }

    function fermeture(uint _nonce, uint _equilibreA, uint _equilibreB, bytes memory _signature) public {
        require(msg.sender == partieA || msg.sender == partieB,"CanalDePaiement: Vous n'etes pas autorise sur ce canal de paiement");
        require(_nonce > 0, "CanalDePaiement: Nonce invalide");

        address closer = msg.sender;
        address counterpart = address(0);
        if (closer == partieA) counterpart = partieB;
        else counterpart = partieA;

        bytes32 hash = message(_equilibreA, _equilibreB, _nonce);
        bytes32 signedHash = ECDSA.toEthSignedMessageHash(hash);

        address ad = ECDSA.recover(signedHash, _signature);
        require(ad == counterpart,"CanalDePaiement: Signature non reconnue");

        equilibreA = _equilibreA;
        equilibreB = _equilibreB;
        etat = EtatCanal.FERME;
        blocFermeture = block.number;
    }

}