const {
  BN,           // Big Number support
  balance,
  time,
  ether,
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
} = require('@openzeppelin/test-helpers');

const { expect, assert } = require('chai');

const CanalDePaiement = artifacts.require('CanalDePaiement');

contract('Canal de paiement', function (accounts) {
 const _name = 'CanalDePaiement';
 const _owner = accounts[0];

 async function getBalance(from) {
    if (from.tracker != undefined) {
        const r = await from.tracker.get();
        return r;
    } else {
        return -1;
    }
 }

// cf https://github.com/ethereum/wiki/wiki/JavaScript-API#web3ethgettransactionreceipt
 function dumpTxInfos(result) {
    console.log(result);
 }


   before(async function () {
    console.log("Web3 Version : "+web3.version);
    this.CanalDePaiementInstance = await CanalDePaiement.new(accounts[1], accounts[2], ether('5'),{from: _owner});
    console.log("Created CanalDePaiementInstance at "+this.CanalDePaiementInstance.address);

    console.log(CanalDePaiement.defaults());
    this.owerOfContracts = CanalDePaiement.defaults().from;

    this.trackerContract = await balance.tracker(this.CanalDePaiementInstance.address);
    this.trackerAccount1 = await balance.tracker(accounts[1]);
    this.trackerAccount2 = await balance.tracker(accounts[2]);

    // Check accounts balance and refund if needed
    //let b1 = await web3.eth.getBalance(accounts[1]);
    //let b2 = await web3.eth.getBalance(accounts[2]);

    let b1 = await this.trackerAccount1.get();
    let b2 = await this.trackerAccount2.get();
    
    console.log("Account 1 balance : "+web3.utils.fromWei(b1,"ether"));
    console.log("Account 2 balance : "+web3.utils.fromWei(b2,"ether"));

    if (web3.utils.fromWei(b1,"ether") < 10) {
      await web3.eth.sendTransaction({from:accounts[0], to:accounts[1], value: web3.utils.toWei(new BN('20'), "ether")});
    }
    if (web3.utils.fromWei(b2,"ether") < 10) {
      await web3.eth.sendTransaction({from:accounts[0], to:accounts[2], value: web3.utils.toWei(new BN('20'), "ether")});
    }

    //b1 = await web3.eth.getBalance(accounts[1]);
    //b2 = await web3.eth.getBalance(accounts[2]);
    
    b1 = await this.trackerAccount1.get();
    b2 = await this.trackerAccount2.get();

    console.log("Account 1 balance : "+web3.utils.fromWei(b1,"ether"));
    console.log("Account 2 balance : "+web3.utils.fromWei(b2,"ether"));
  });

  beforeEach(async function () {
    console.log("\nBEFORE");
    let b1 = await this.trackerAccount1.get();
    let b2 = await this.trackerAccount2.get();

    console.log("Account 1 balance : "+web3.utils.fromWei(b1,"ether"));
    console.log("Account 2 balance : "+web3.utils.fromWei(b2,"ether"));
 });

  afterEach(async function () {
    console.log("\nAFTER");
    let b1 = await this.trackerAccount1.get();
    let b2 = await this.trackerAccount2.get();

    console.log("Account 1 balance : "+web3.utils.fromWei(b1,"ether"));
    console.log("Account 2 balance : "+web3.utils.fromWei(b2,"ether"));
 });

  after(async function () {
  console.log("\nTESTS END");
 });

  it('has a name', async function () {
    expect(await this.CanalDePaiementInstance.name()).to.equal(_name);
  });

  it("Should make first account an owner", async function () {
    expect(await this.CanalDePaiementInstance.owner()).to.equal(_owner);
  });

  it("Should not allow to deploy contract with invalid address", async function () {
    await expectRevert(CanalDePaiement.new(constants.ZERO_ADDRESS, accounts[2], ether('5'),{from: _owner}),"CanalDePaiement: adresse invalide pour partieA");
    await expectRevert(CanalDePaiement.new(accounts[1], constants.ZERO_ADDRESS, ether('5'),{from: _owner}),"CanalDePaiement: adresse invalide pour partieB");
  });

  it("Should not allow to deploy contract with insuficient funds", async function () {
    await expectRevert(CanalDePaiement.new(accounts[1], accounts[2], ether('0'),{from: _owner}),"CanalDePaiement: le montant doit etre superieur à 0");
  });

  it("Should be in empty state", async function () {
    const state = await this.CanalDePaiementInstance.etat.call();
    console.log(state);
    expect(state).to.be.bignumber.equal(new BN('0'));
  });
  
  it("Should not allow a non registred user to use the channel", async function () {
    await expectRevert(this.CanalDePaiementInstance.financer({from: accounts[3], value: ether('0')}),"CanalDePaiement: Vous n'etes pas autorise sur ce canal de paiement");
  });
  
  it("Should not allow to send 0 ethers", async function () {
    await expectRevert(this.CanalDePaiementInstance.financer({from: accounts[1], value: ether('0')}),"CanalDePaiement: Financement incorrect");
  });
  
  it("Should allow to send correct amount of ethers", async function () {
    //let receipt = await expect(this.CanalDePaiementInstance.financer({from: accounts[1], value: ether('3')}));
    this.CanalDePaiementInstance.financer({from: accounts[1], value: ether('3')})
    .on("confirmation", function(receipt) {
        console.log(receipt);
    });

    await time.advanceBlock();

    let delta = await this.trackerAccount1.delta();
    expect(delta).to.be.bignumber.below(new BN('-3'));
        
    receipt = await expect(this.CanalDePaiementInstance.financer({from: accounts[2], value: ether('5')}));

    await time.advanceBlock();

    delta = await this.trackerAccount2.delta();
    expect(delta).to.be.bignumber.below(new BN('-5'));
    
  });
  
  it("Should be in active state", async function () {
    const state = await this.CanalDePaiementInstance.etat.call({from: accounts[1]});
    console.log(state);
    expect(state).to.be.bignumber.equal(new BN('1'));
  });
  
  it("Should not allow to send more ethers than expected", async function () {
    await expectRevert(this.CanalDePaiementInstance.financer({from: accounts[1], value: ether('6')}),"CanalDePaiement: Financement incorrect");
  });

  async function signPayment(signer, password, _a, _b, _nonce) {
    const hash = await web3.utils.soliditySha3(_a, _b, _nonce).toString("hex");
    console.log("hash:"+hash);
    
    const sig = await web3.eth.personal.sign(hash, signer, password);
    console.log("sig:"+sig);
    return sig;
}

  it("Should close the channel", async function () {
    let _a = 2;
    let _b = 18;
    let _nonce = 256987566;
    let closer = accounts[1];
    let _signature = await signPayment(accounts[2], "alyra", _a, _b, _nonce);
    
    await this.CanalDePaiementInstance.fermeture(_nonce, _a, _b, _signature, {from: closer});
  });


 });
