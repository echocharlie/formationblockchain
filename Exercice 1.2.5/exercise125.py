# -*- coding: utf8 -*-


"""
    Exercice 1.2.5 : Explorer un arbre (Python3)

"""

import random

class Noeud:
    def __init__(self, v, parent=None):
        self.valeur = v
        self.parent = parent
        self.gauche = None
        self.droite = None

        #print(f"création d'un noeud de valeur {v}")

    # Affiche la valeur du noeud et la valeur de ses deux enfants et de son parent
    def toString(self):
        out = "Noeud " + str(self.valeur) + ":  L";
        if (self.gauche is None):
            out += "-"
        else:
            out += str(self.gauche.valeur)
        out += " R";
        if (self.droite is None):
            out += "-"
        else:
            out += str(self.droite.valeur)
        out += " P";
        if (self.parent is None):
            out += "-"
        else:
            out += str(self.parent.valeur)

        print(out)

    def __repr__(self):
        return str(self.valeur)

class Arbre:
    def __init__(self):
        self.racine = None

    def ajouterNoeud(self, val):
        #print(f"arbre, ajouter {val}")
        if self.racine is None:
            self.racine = Noeud(val)
        else:
            self._ajouter(val, self.racine)

    def _ajouter(self, val, nd):
        #print(f"_ajouter {val} {nd}")
        if val < nd.valeur:
            if nd.gauche is not None:
                self._ajouter(val, nd.gauche)
            else:
                #print(f"\tà gauche")
                nd.gauche = Noeud(val, nd)
        else:
            if nd.droite is not None:
                self._ajouter(val, nd.droite)
            else:
                nd.droite = Noeud(val, nd)
                #print(f"\tà droite")

    def minimum(self, root):
        if root is None or root.gauche is None:
            return root
        return self.minimum(root.gauche)

    def maximum(self, root):
        if root is None or root.droite is None:
            return root
        return self.maximum(root.droite)

    def infixe_recur(self, node):
        if node is None:
            return
        if node.gauche:
            self.infixe_recur(node.gauche)
        #print(node.valeur)
        if node.droite:
            self.infixe_recur(node.droite)

    def _infixe(self, node, r):
        if node is None:
            return
        if node.gauche:
            self._infixe(node.gauche,r)
        r.append(str(node.valeur))
        if node.droite:
            self._infixe(node.droite,r)
        return node.valeur

    def infixe(self):
        r = []
        self._infixe(self.racine, r)
        return r

    def dump(self):
        self.infixe_recur(self.racine)

    def display(self, node=None, level=0, is_left=False):
        if node is None: node = self.racine
        indent=4*level
        #print("-"*indent+str(node))
        if node.gauche is not None:
            level+=1
            self.display(node.gauche, level, True)
            level-=1
        if node.droite is not None:
            level+=1
            self.display(node.droite, level, False)
            level -= 1

    # Méthode pour afficher la valeur d'un noeud à partir de sa valeur
    def printNoeud(self, val):
        noeud = self.trouverNoeud(val)
        if noeud is not None:
            noeud.toString();

    def trouverNoeud(self, val):
        """

        :param val: The value to search in the tree
        :return: The node where the value was found
        """

        if not val: return None

        node = self.racine
        found_node = None
        while found_node is None:
            #print(node.valeur)
            if node.valeur == val:
                found_node = node
            else:
                if node.gauche is not None and node.valeur > val:
                    node = node.gauche
                elif node.droite is not None and node.valeur < val:
                    node = node.droite
                else:
                    break

        return found_node

    def find_value_and_parent(self, value):
        """

        :param value: The value to search in the tree
        :return: The node where the value was found and its parent node or None if it is the root node
        """
        parent_node = None
        node = self.racine
        found_node = None
        while found_node is None:
            #print(node.valeur)
            if node.valeur == value:
                found_node = node
            else:
                if node.gauche is not None and node.valeur > value:
                    parent_node = node
                    node = node.gauche
                elif node.droite is not None and node.valeur < value:
                    parent_node = node
                    node = node.droite
                else:
                    break

        return found_node, parent_node

    def successor(self, node):
        if node.droite is not None:
            return self.minimum(node.droite)
        succ = None
        root = self.racine
        while root is not None and root.valeur != node.valeur:
            if root.valeur > node.valeur:
                succ = root
                root = root.gauche
            else:
                root = root.droite
        return succ

    def predecessor(self, node):
        if node.gauche is not None:
            return self.maximum(node.gauche)
        else:
            pred = None
            root = self.racine
            while root is not None and root.valeur != node.valeur:
                if root.valeur > node.valeur:
                    root = root.gauche
                else:
                    pred = root
                    root = root.droite
            return pred

    def remove_node(self, node):
        if not node:
            return

        #print(f"supprimer, node:{node}, parent:{node.parent}")
        if node.gauche is not None and node.droite is not None:
            # noeud avec 2 enfants
            #print("le noeud a 2 enfants")
            # Trouver le successeur
            succ = self.successor(node)
            #print(f"successor de {node.valeur} : {succ.valeur}")
            # swapper la valeur du successeur avec celle du noeud à suppimer
            tmp = node.valeur
            node.valeur = succ.valeur
            succ.valeur = tmp
            # supprimer le successeur
            self.remove_node(succ)
        else:
            if node.gauche is None and node.droite is None:
                # c'est une feuille
                #print("le noeud est une feuille")
                if node.parent is not None:
                    if node.parent.gauche == node:
                        node.parent.gauche = None
                    else:
                        node.parent.droite = None
                    del node
            elif node.gauche is not None:
                # 1 seul enfant à gauche
                #print("1 seul enfant à gauche")
                if node.parent is not None:
                    if node.parent.gauche == node:
                        node.parent.gauche = node.gauche
                    else:
                        node.parent.droite = node.gauche
                    del node
            else:
                # 1 seul enfant à droite
                #print("1 seul enfant à droite")
                if node.parent is not None:
                    if node.parent.gauche == node:
                        node.parent.gauche = node.droite
                    else:
                        node.parent.droite = node.droite
                    del node

    def supprimerNoeud(self, val):
        # recherche de la valeur dans l'arbre
        node = self.trouverNoeud(val)

        if node is None:
            #print(f"Valeur {val} introuvable dans l'arbre")
            return

        self.remove_node(node)

if __name__ == '__main__':

    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)
    node = a.trouverNoeud(None)
    node = a.trouverNoeud(24)
    if node is None:
        print("La valeur 24 n'appartient à l'arbre.")
    else:
        print("La valeur " + str(node.valeur) + " appartient à l'arbre.")

    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    node = a.trouverNoeud(None)
    if node is None:
        print("La valeur None n'appartient à l'arbre.")
    else:
        print("La valeur " + str(node.valeur) + " appartient à l'arbre.")

    #Test 5
    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    a.supprimerNoeud(14)
    a.printNoeud(13)

    # Test 6
    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    a.supprimerNoeud(32)
    a.printNoeud(31)

    # Noeud 31:  L- R- P33

    # Test 7
    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    a.supprimerNoeud(13)
    a.printNoeud(11)
    a.printNoeud(12)
    a.printNoeud(14)

    """
    Noeud 11:  L10 R14 P18
    Noeud 12:  L- R- P14
    Noeud 14:  L12 R- P11
    """
    # Test 8
    print("\ntest 8")
    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    a.supprimerNoeud(40)
    a.printNoeud(33)
    a.printNoeud(35)
    a.printNoeud(46)

    """
    Noeud 33:  L31 R35 P30
    Noeud 35:  L- R46 P33
    Noeud 46:  L- R- P35
"""

    # test 9
    print("\ntest 9")
    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    a.supprimerNoeud(24)
    a.printNoeud(18)

    #Noeud 18:  L11 R21 P30

    # test 10
    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    a.supprimerNoeud(31)
    a.printNoeud(33)

    """
    Noeud 33:  L32 R40 P30
    """

    # test 11
    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    a.supprimerNoeud(None)
    print(a.infixe())

    # ['10', '11', '12', '13', '14', '18', '21', '24', '30', '31', '32', '33', '35', '40', '46']

    # test 12
    a = Arbre()
    a.ajouterNoeud(30)
    a.ajouterNoeud(18)
    a.ajouterNoeud(24)
    a.ajouterNoeud(11)
    a.ajouterNoeud(33)
    a.ajouterNoeud(13)
    a.ajouterNoeud(40)
    a.ajouterNoeud(46)
    a.ajouterNoeud(14)
    a.ajouterNoeud(21)
    a.ajouterNoeud(12)
    a.ajouterNoeud(10)
    a.ajouterNoeud(31)
    a.ajouterNoeud(35)
    a.ajouterNoeud(32)

    a.supprimerNoeud(20)
    print(a.infixe())

    ['10', '11', '12', '13', '14', '18', '21', '24', '30', '31', '32', '33', '35', '40', '46']



    #Noeud 13:  L12 R- P11
    #print(a.infixe())

    print(f"succ de 24 ? {a.successor(a.trouverNoeud(24))}")
    print(f"succ de 32 ? {a.successor(a.trouverNoeud(32))}")
    print(f"pred de 35 ? {a.predecessor(a.trouverNoeud(35))}")
    print(f"pred de 18 ? {a.predecessor(a.trouverNoeud(18))}")

    #a.dump()
    r = a.infixe()
    print(r)

    print(f"12 ? {a.trouverNoeud(12) is not None}")
    print(f"99 ? {a.trouverNoeud(99) is not None}")

    a.supprimerNoeud(11)
    r = a.infixe()
    print(r)
    a.supprimerNoeud(30)
    r = a.infixe()
    print(r)
    """
    a.supprimerNoeud(21)
    r = a.infixe()
    print(r)
    a.supprimerNoeud(31)
    r = a.infixe()
    print(r)
    """

    """
    numbers=[]

    while len(numbers) < 50:
        random_number = random.randint(1, 500)
        if str(random_number) not in numbers:
            numbers.append(str(random_number))
        
    print(",".join(numbers))
    """

    """
    numbers=[326,451,76,430,442,39,466,483,187,467,490,365,90,459,438,89,27,207,12,362,382,58,496,328,67,453,117,221,202,414,441,439,197,420,421,54,183,33,2,161,329,281,85,426,182,123,140,244,50,234]

    arbre = Arbre()

    for n in numbers:
        arbre.ajouter(n)

    print("Affichage de l'abre")
    arbre.display()
    print("Dump")
    arbre.dump()
    print(f"root node {arbre.racine}")
    print(f"bottom left node (min value) : {arbre.find_bottom_left_node()}")
    print(f"bottom right node (max value) : {arbre.find_bottom_right_node()}")

    print("recherche de la valeur 426")
    node = arbre.find_value(426)
    print(f"résultat : {node}")
    print("recherche de la valeur 999")
    node = arbre.find_value(999)
    print(f"résultat : {node}")

    node,parent = arbre.find_value_and_parent(442)
    print(f"node : {node}, parent : {parent}")

    # suppression du noeud avec la valeur 2
    arbre.supprimer(2)
    arbre.dump()
    arbre.display()
    arbre.supprimer(27)
    arbre.dump()
    arbre.display()
    arbre.supprimer(123)
    arbre.dump()
    arbre.display()
    arbre.supprimer(202)
    arbre.dump()
    arbre.display()

    """