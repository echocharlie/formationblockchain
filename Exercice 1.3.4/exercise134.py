# -*- coding: utf8 -*-


"""
    Exercice 1.3.4 : Représenter une courbe elliptique
    https://ecole.alyra.fr/mod/assign/view.php?id=58

    y²=x³+ax +b

"""
import math

class CourbeElliptique:

    def __init__(self, a, b):
        self.a = a
        self.b = b
        print(f"4*a**3+27*b**2 = {4*a**3+27*b**2}")
        if 4*a**3+27*b**2 == 0:
            raise ValueError(f"({self.a}, {self.b}) n'est pas une courbe valide")

    def __eq__(self, other):
        if not other: return False

        return self.a == other.a and self.b == other.b

    def testPoint(self, x, y):
        return math.pow(y,2) == math.pow(x,3) + (self.a * x) + self.b

    def __repr__(self):
        return {'a': self.a, 'b': self.b}

    def __str__(self):
        return f"CourbeElliptique({self.a},{self.b})"


if __name__ == '__main__':

    c = CourbeElliptique(0,7)
    print(c)

    d = CourbeElliptique(1,7)
    e = CourbeElliptique(2,7)
    f = CourbeElliptique(2,7)

    print(e == f)

