# -*- coding: utf8 -*-


"""

    Exercice 1.4.4 : décomposer une transaction
    https://ecole.alyra.fr/mod/assign/view.php?id=67
    https://forum.alyra.fr/t/exercice-1-4-4-decomposer-une-transaction/87

"""
class BitcoinOutput:

    def __init__(self, raw_amount, public_key):
        self.raw_amount = raw_amount
        self.public_key = public_key

    def getamount(self):
        return int(hexStrEndianSwap(self.raw_amount), 16)

    def gerpublickey(self):
        return self.public_key

class BitcoinTransaction:

    def __init__(self, raw_data):
        self.raw_data = raw_data

        self.outputs = []

        # VERSION
        current_index = 0
        self.version = raw_data[current_index:current_index + 4 * 2]
        current_index += 4 * 2

        # INPUT COUNT
        ic = self.raw_data[current_index:current_index + 1 * 2]
        next_index = 1
        current_index += 1 * 2
        if ic.upper() == "FD":
            self.input_count = self.raw_data[current_index:current_index + 2 * 2]
            current_index += 2 * 2
        elif ic.upper() == "FE":
            self.input_count = self.raw_data[current_index:current_index + 4 * 2]
            current_index += 4 * 2
        elif ic.upper() == "FF":
            self.input_count = self.raw_data[current_index:current_index + 8 * 2]
            current_index += 8 * 2
        else:
            self.input_count = ic

        # TX ID
        self.transaction_id = self.raw_data[current_index:current_index + 32 * 2]
        current_index += 32 * 2

        # OUTPUT INDEX
        self.outputindex = self.raw_data[current_index:current_index + 4 *2]
        current_index += 4 * 2

        # VARINT
        varint = self.raw_data[current_index:current_index + 1 * 2]
        next_index = 1
        current_index += 1 * 2
        if varint.upper() == "FD":
            self.scriptsig_varint = self.raw_data[current_index:current_index + 2 * 2]
            current_index += 2 * 2
        elif varint.upper() == "FE":
            self.scriptsig_varint = self.raw_data[current_index:current_index + 4 * 2]
            current_index += 4 * 2
        elif varint.upper() == "FF":
            self.scriptsig_varint = self.raw_data[current_index:current_index + 8 * 2]
            current_index += 8 * 2
        else:
            self.scriptsig_varint = varint

        # varint signature
        varint = self.raw_data[current_index:current_index + 1 * 2]
        next_index = 1
        current_index += 1 * 2
        if varint.upper() == "FD":
            self.signature_varint = self.raw_data[current_index:current_index + 2 * 2]
            current_index += 2 * 2
        elif varint.upper() == "FE":
            self.signature_varint = self.raw_data[current_index:current_index + 4 * 2]
            current_index += 4 * 2
        elif varint.upper() == "FF":
            self.signature_varint = self.raw_data[current_index:current_index + 8 * 2]
            current_index += 8 * 2
        else:
            self.signature_varint = varint

        self.signature_length = int(self.signature_varint,16)
        # Signature
        self.signature = self.raw_data[current_index:current_index + self.signature_length * 2]
        current_index += self.signature_length * 2

        # varint public key
        varint = self.raw_data[current_index:current_index + 1 * 2]
        next_index = 1
        current_index += 1 * 2
        if varint.upper() == "FD":
            self.pubkey_varint = self.raw_data[current_index:current_index + 2 * 2]
            current_index += 2 * 2
        elif varint.upper() == "FE":
            self.pubkey_varint = self.raw_data[current_index:current_index + 4 * 2]
            current_index += 4 * 2
        elif varint.upper() == "FF":
            self.pubkey_varint = self.raw_data[current_index:current_index + 8 * 2]
            current_index += 8 * 2
        else:
            self.pubkey_varint = varint
        self.pubkey_length = int(self.pubkey_varint, 16)

        # public key
        self.publickey = self.raw_data[current_index:current_index + self.pubkey_length * 2]
        current_index += self.pubkey_length * 2

        # Sequence
        self.sequence = raw_data[current_index:current_index + 4 * 2]
        current_index += 4 * 2

        # OUTPUTS
        # varint output length
        varint = self.raw_data[current_index:current_index + 1 * 2]
        next_index = 1
        current_index += 1 * 2
        if varint.upper() == "FD":
            self.output_count_varint = self.raw_data[current_index:current_index + 2 * 2]
            current_index += 2 * 2
        elif varint.upper() == "FE":
            self.output_count_varint = self.raw_data[current_index:current_index + 4 * 2]
            current_index += 4 * 2
        elif varint.upper() == "FF":
            self.output_count_varint = self.raw_data[current_index:current_index + 8 * 2]
            current_index += 8 * 2
        else:
            self.output_count_varint = varint
        self.output_count = int(self.output_count_varint, 16)

        for output_index in range(self.output_count):
            # Montant
            amount = self.raw_data[current_index:current_index + 8 * 2]
            current_index += 8 * 2
            # varint public key
            varint = self.raw_data[current_index:current_index + 1 * 2]
            next_index = 1
            current_index += 1 * 2
            if varint.upper() == "FD":
                pk_varint= self.raw_data[current_index:current_index + 2 * 2]
                current_index += 2 * 2
            elif varint.upper() == "FE":
                pk_varint = self.raw_data[current_index:current_index + 4 * 2]
                current_index += 4 * 2
            elif varint.upper() == "FF":
                pk_varint = self.raw_data[current_index:current_index + 8 * 2]
                current_index += 8 * 2
            else:
                pk_varint = varint

            pk_length = int(pk_varint, 16)
            # public key
            public_key = self.raw_data[current_index:current_index + pk_length *2]

            output = BitcoinOutput(amount, public_key)
            self.outputs.append(output)

            current_index += pk_length * 2

        # LOCKTIME
        self.locktime = self.raw_data[current_index:current_index + 4 * 2]

    def getinput_count(self):
        return self.input_count

    def getversion(self):
        return self.version

    def gettransactionid(self):
        return self.transaction_id

    def getoutputindex(self):
        return self.outputindex

    def getsignature(self):
        return self.signature

def hexStrEndianSwap(theString):
    """Rearranges character-couples in a little endian hex string to
    convert it into a big endian hex string and vice-versa. i.e. 'A3F2'
    is converted to 'F2A3'

    @param theString: The string to swap character-couples in
    @return: A hex string with swapped character-couples. -1 on error."""

    # We can't swap character couples in a string that has an odd number
    # of characters.
    if len(theString) % 2 != 0:
        return -1

    # Swap the couples
    swapList = []
    for i in range(0, len(theString), 2):
        swapList.insert(0, theString[i:i + 2])

    # Combine everything into one string. Don't use a delimeter.
    return ''.join(swapList)


if __name__ == '__main__':

    input = '''0100000001f129de033c57582efb464e94ad438fff493cc4de4481729b859712368582\
75c2010000006a4730440220155a2ea4a702cadf37052c87bfe46f0bd24809759acff8\
d8a7206979610e46f6022052b688b784fa1dcb1cffeef89e7486344b814b0c578133a7\
b0bce5be978a9208012103915170b588170cbcf6380ef701d19bd18a526611c0c69c62\
d2c29ff6863d501affffffff02ccaec817000000001976a9142527ce7f0300330012d6\
f97672d9acb5130ec4f888ac18411a000000000017a9140b8372dffcb39943c7bfca84\
f9c40763b8fa9a068700000000'''

    #input='''02000000000101d0696fa4e7eb18130faa62024f8cb3e4a81c12dc07592b547fb4da573b27d18f00000000171600149345d82c8798b233df03fd621acbf79f475c65f6feffffff020065cd1d0000000017a914c97ccae6822d00edac17e6156df748bdff0c2eaa870880380c0100000017a914dc6c1104cd9f91339a014f688728ad22f8902aed8702473044022000ad4b59350cdb8c87b5affeb37c0e23dc499f689569a339bf43b2dd37c7811102202ef845adc31385c3d08c195f5db8712180b0ef36fdd5b48dd7496cf6ac7fe94a012103b8f76bc26d76707cea7fabd5f610773904899b64b9fe44fca1346aff8478896765000000'''



    # Le hash de la transaction passée où sont les bitcoins à dépenser (sur 32 octets)
    # L’index de la sortie (output) de cette transaction concernée (sur 4 octets)
    # ScriptSig
    # Séquence (sur 4 octets) --> 0xffffffff champ désactivé

    bt = BitcoinTransaction(input)
    print("version : ",bt.getversion())
    print("input count : ", bt.getinput_count())
    print("transaction id : ",bt.gettransactionid())
    print("output index : ",bt.getoutputindex())
    print("signature length : ",bt.signature_length)
    print("signature : ",bt.getsignature())
    print("public key length : ",bt.pubkey_varint,bt.pubkey_length)
    print("sequence : ",bt.sequence)
    print("output count : ",bt.output_count)
    for o in bt.outputs:
        print("\tamount : ",o.getamount(),", public key : ",o.public_key)

    print("locktime : ",bt.locktime)
    """
    bi = BitcoinInput(input)
    print(f"Transaction ID : {bi.gettransaction_id()}")
    print(f"Output Index : {bi.getoutput_index()}")
    print(f"VARINT : {bi.scriptsig_length}")
    print(f"Signature Length : {bi.signature_length}")
    print(f"Signature : {bi.getsignature()}")
    print(f"Public key length : {bi.pubkey_length}")
    print(f"Public key : {bi.getpublic_key()}")
    print(f"Sequence : {bi.sequence}")
    """