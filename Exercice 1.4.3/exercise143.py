# -*- coding: utf8 -*-


"""

    Exercice 1.4.3 : Identifier les différents champs d'une entrée de transaction
    https://ecole.alyra.fr/mod/assign/view.php?id=66
    https://forum.alyra.fr/t/exercice-1-4-3-identifier-les-differents-champs-dune-transaction/86

"""

class BitcoinInput:


    def __init__(self, raw_data):
        self.raw_data = raw_data

        current_index = 0
        self.tx_id = self.raw_data[current_index:32*2]
        current_index = 32 * 2
        self.outputindex = self.raw_data[current_index:current_index + (4 * 2)]
        current_index += (4 * 2)

        next_index = 1
        self.varint = self.raw_data[current_index:current_index + 2]
        if self.varint.upper() == "FD":
            next_index = 2
            pass
        elif self.varint.upper() == "FE":
            next_index = 4
            pass
        elif self.varint.upper() == "FF":
            next_index = 8
            pass
        else:
            self.scriptsig_length = int(self.varint,16)

        current_index += (next_index * 2)

        self.varint_signature = self.raw_data[current_index:current_index + 2]
        next_index = 1
        if self.varint_signature.upper() == "FD":
            next_index = 1
            current_index += 1
            self.signature_length = int(self.raw_data[current_index:current_index + 2 * 2], 16)
            next_index += 2
        elif self.varint_signature.upper() == "FE":
            next_index = 1
            current_index += 1
            self.signature_length = int(self.raw_data[current_index:current_index + 4 * 2], 16)
            next_index += 4
        elif self.varint_signature.upper() == "FF":
            next_index = 1
            current_index += 1
            self.signature_length = int(self.raw_data[current_index:current_index + 8 * 2], 16)
            next_index += 8
        else:
            self.signature_length = int(self.varint_signature,16)

        current_index += (next_index * 2)
        self.signature = raw_data[current_index:current_index + self.signature_length  * 2]

        current_index += self.signature_length  * 2

        self.varint_pubkey = self.raw_data[current_index:current_index + 2]
        next_index = 1
        if self.varint_pubkey.upper() == "FD":
            next_index = 1
            current_index += 1
            self.pubkey_length = int(self.raw_data[current_index:current_index + 2 * 2], 16)
            next_index += 2
        elif self.varint_pubkey.upper() == "FE":
            next_index = 1
            current_index += 1
            self.pubkey_length = int(self.raw_data[current_index:current_index + 4 * 2], 16)
            next_index += 4
        elif self.varint_pubkey.upper() == "FF":
            next_index = 1
            current_index += 1
            self.pubkey_length = int(self.raw_data[current_index:current_index + 8 * 2], 16)
            next_index += 8
        else:
            self.pubkey_length = int(self.varint_pubkey,16)

        current_index += (next_index * 2)
        self.pubkey = raw_data[current_index:current_index + self.pubkey_length  * 2]
        current_index += self.pubkey_length  * 2

        self.sequence = raw_data[current_index:]

    def gettransaction_id(self):
        return self.tx_id

    def getoutput_index(self):
        return self.outputindex

    def getsignature(self):
        return self.signature

    def getpublic_key(self):
        return self.pubkey

    def getsequence(self):
        return self.sequence

if __name__ == '__main__':

    input = '''941e985075825e09de53b08cdd346bb67075ef0ce5c94f98853292d4bf94c10d010000006b483045022100ab44ef4\
25e6d85c03cf301bc16465e3176b55bba9727706819eaf07cf84cf52d02203f7dc7ae9ab36bead14dd3c83c8c030bf8\
ce596e692021b66441b39b4b35e64e012102f63ae3eba460a8ed1be568b0c9a6c947abe9f079bcf861a7fdb2fd577ed\
48a81Feffffff'''


    # Le hash de la transaction passée où sont les bitcoins à dépenser (sur 32 octets)
    # L’index de la sortie (output) de cette transaction concernée (sur 4 octets)
    # ScriptSig
    # Séquence (sur 4 octets) --> 0xffffffff champ désactivé

    bi = BitcoinInput(input)
    print(f"Transaction ID : {bi.gettransaction_id()}")
    print(f"Output Index : {bi.getoutput_index()}")
    print(f"VARINT : {bi.scriptsig_length}")
    print(f"Signature Length : {bi.signature_length}")
    print(f"Signature : {bi.getsignature()}")
    print(f"Public key length : {bi.pubkey_length}")
    print(f"Public key : {bi.getpublic_key()}")
    print(f"Sequence : {bi.sequence}")