# -*- coding: utf8 -*-


"""
    https://ecole.alyra.fr/mod/book/view.php?id=53&chapterid=12
    Exercice 1.3.5 :Génèrer une adresse type bitcoin
    https://ecole.alyra.fr/mod/assign/view.php?id=60

    lib crypto : https://pypi.org/project/pycryptodome/

"""

import random
from Crypto.Hash import SHA256, RIPEMD160

mainnet_prfix = bytes([0])


def create_bitcoin_address(number):
    sha256 = SHA256.new()
    ripemd = RIPEMD160.new()

    sha256.update(bytes(number))

    ripemd.update(bytes(sha256.digest()))
    hash160  = ripemd.digest()

    # Ajout du préfix correspondant au type d'adresse
    h = mainnet_prfix + hash160

    # Ajout des 4 octets de conttrôle
    sha256.update(h)
    sha256.update(sha256.digest())
    ctrl = sha256.digest()[:4]

    h += ctrl

    # Conversion en base 58
    result=bytes()
    for b in h:
        base58_number = encode(b)
        result += bytes(base58_number.encode())

    return result


BASE58 = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXY"


def encode(num, alphabet=BASE58):
    if num == 0:
        return alphabet[0]
    arr = []
    base = len(alphabet)
    while num:
        num, rem = divmod(num, base)
        arr.append(alphabet[rem])
    arr.reverse()
    return ''.join(arr)


def decode(string, alphabet=BASE58):
    base = len(alphabet)
    strlen = len(string)
    num = 0

    idx = 0
    for char in string:
        power = (strlen - (idx + 1))
        num += alphabet.index(char) * (base ** power)
        idx += 1

    return num


if __name__ == '__main__':

    for i in range(10):
        random_number = random.randint(1, 100)
        print(create_bitcoin_address(random_number))