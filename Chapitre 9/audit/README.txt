Manque l'import de SafeMath
Correction du code pour compilation en 0.5.12 comme stipulé par le pragma

function Crowdsale(address _escrow) public{
	ne pas utiliser l'adresse mais l'interface
	
function() public {
	manque le modifer payable
	pas de contrôle du retour de send()
	pas de contre-mesure contre la ré-entrance
	
	
ajouter des evenements


entreprise.santeprevoyance@ag2rlamondiale.fr
Objet : n° siret
nom prenom
numéro de SS
référence du contrat



Code corrigé avant audit : 
pragma solidity ^0.5.12;

import "github.com/OpenZeppelin/openzeppelin-contracts/blob/release-v2.5.0/contracts/math/SafeMath.sol";

contract Crowdsale {
    
    using SafeMath for uint256;
    address public owner; // the owner of the contract
    address public escrow; // wallet to collect raised ETH
    uint256 public savedBalance = 0; // Total amount raised in ETH
    mapping (address => uint256) public balances; // Balances in incoming Ether
    
    // Initialization
    constructor(address _escrow) public{
        owner = tx.origin;
        // add address of the specific contract
        escrow = _escrow;
    }
    
    // fallback function to receive ETH
    function() external payable {
        address payable _to = address(uint160(escrow));
        balances[msg.sender] = balances[msg.sender].add(msg.value);
        savedBalance = savedBalance.add(msg.value);
        _to.send(msg.value);
    }
    
    // refund investisor	
    function withdrawPayments() public{
        address payee = msg.sender;
        address payable _to = address(uint160(payee));
        uint256 payment = balances[payee];
        _to.send(payment);
        savedBalance = savedBalance.sub(payment);
        balances[payee] = 0;
    }
}

Audit : 
Fonctionnellement, toute somme envoyée vers ce SC sera immédiatement renvoyée vers une adresse externe (utilisateur ou smart contrat) Les fonds disponibles sur ce SC seront donc toujours nuls.
Toute tentative d'un utilisateur voulant récupérer ses fonds via la fonction withdraw ne fera que lui faire consommer du gas. Le SC ne disposant d'aucun fonds, il n'y a rien à rembourser.
Si l'adresse "escrow" correspond à un smart contract il convient d'en tenir compte : 
	Si sa méthode de fallback est sur-consommatrice de gas elle pourrait empêcher systématiquement l'envoi de fonds sur le SC provoquant un déni de service.
Ce smart contract est donc une escroquerie.

Le constructor du contrat devrait tester l'adresse et vérifier qu'elle n'est pas égale à 0

Ligne 15 : Utilisation de owner = tx.origin;
Risque d'usurpation d'identité
Contre-mesure : Utiliser msg.sender à la place

La fonction de fallback consomme trop de gas (> 2300) 
Risque : Impossibilité de recevoir des ethers
Contre-mesure : Alléger le code de la fonction pour passer sous la limite des 2300 gas

Lignes 25 : Utilisation de la méthode send sans test de la valeur de retour
Risque : Réentrance avec consommation de tout le gas disponible
Contre-mesures possibles :
-à minima, tester la valeur de retour

La fonction withdraw() ne teste pas le retour de la méthode send() et met à jour l'état du contrat quoi qu'il advienne.
Si la méthode send échoue le contrat sera mis à jour alors qu'il ne le devrait pas.

Les fonctions de fallback et withdraw devraient déclencher un évènement pour rendre compte de ce qui se passe dans le contrat

Le code fournit "supprime" le détournement des fonds, même si ce n'est pas l'objet d'un audit.

