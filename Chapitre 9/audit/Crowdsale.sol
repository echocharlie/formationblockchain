pragma solidity ^0.5.12;

import "github.com/OpenZeppelin/openzeppelin-contracts/blob/release-v2.5.0/contracts/math/SafeMath.sol";

contract Crowdsale {
    
    using SafeMath for uint256;
    address public owner; // the owner of the contract
    
    uint256 public savedBalance = 0; // Total amount raised in ETH
    mapping (address => uint256) public balances; // Balances in incoming Ether
    
    event LogDepositReceived(address _from, uint256 amount);
    event LogWithdrawClaimed(address _to, uint256 amount);
    
    // Initialization
    constructor() public{
        owner = msg.sender;
    }
    
    // fallback function to receive ETH
    function() external payable {
        require(msg.data.length == 0); // pour vérifier que la fonction fallback n’est pas appelée par erreur
        
        balances[msg.sender] = balances[msg.sender].add(msg.value);
        savedBalance = savedBalance.add(msg.value);

        emit LogDepositReceived(msg.sender, msg.value);        
    }
    
    // refund investisor
    function withdrawPayments() public{
        address payee = msg.sender;
        
        uint256 payment = balances[payee];
        (bool success, bytes memory data) = payee.call.value(payment)("");
        require(success);
        savedBalance = savedBalance.sub(payment);
        balances[payee] = 0;
        emit LogWithdrawClaimed(payee, payment);        
    }
}
