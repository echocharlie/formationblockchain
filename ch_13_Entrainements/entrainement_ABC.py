# -*- coding: utf8 -*-


"""
    Entrainement : Chiffre et déchiffrer un message de Vigenere
    Entrainement A
    À partir de l'exercice 1.3.1 écrire la fonction (ou le programme) Vigenère qui prend en paramètre un mot clé et convertit un texte en sa version chiffrée.
    Entrainement B
    Écrire la fonction qui déchiffre un message en connaissant la clé

    (optionnel) en faire une option du programme précédent

    Entrainement C

    Écrire une fonction qui prend un texte en entrée et un nombre n, retire les espaces et le regroupe les caractères tous les n:

"""

#global
ORD_A = ord("A")

def regroupement(text, group):
    """

    :param text:
    :param group: taille du regroupement
    :return: [“MVIEAE”,EILSNS”,”SELTT”]
    """
    if not text or not key: return text

    r = []
    for i in range(group):
        r.append([])

    index = 0
    for c in text:
        if index == group: index = 0
        if not c.isalpha():
            continue
        r[index].append(c)

        index+=1

    return r

def vigenere(text, key, encrypt=True):

    if not text or not key: return text
    new_text = ""
    key_index = 0
    for c in text:
        if key_index == len(key): key_index = 0

        char_to_use = key[key_index]
        shift = ord(char_to_use) - ORD_A
        if encrypt:
            new_text += chr(ord(c) + shift)
        else:
            new_text += chr(ord(c) - shift)

        key_index+=1

    return new_text




if __name__ == '__main__':

    key = "GRA"

    text = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rhoncus tincidunt est, dictum vehicula nisl suscipit vitae. Praesent lectus arcu, semper ac nisl in, cursus viverra risus. Sed non nunc sit amet augue vestibulum posuere. Etiam a turpis orci. Aliquam augue justo, pulvinar vel posuere sed, fringilla id enim. Suspendisse dignissim ornare felis, vel iaculis metus feugiat consequat. Praesent feugiat, odio eu dapibus venenatis, nisi ex ullamcorper diam, ut accumsan diam lacus in tortor. Phasellus vel venenatis dui, vel ultricies metus. Proin pretium enim in rutrum laoreet.
Duis pellentesque dolor felis, eu lacinia lectus porta eu. Duis feugiat aliquam diam, ac molestie est suscipit sed. Quisque a nunc ut lacus commodo elementum porta non dolor. Curabitur sollicitudin dignissim congue. Praesent at velit condimentum, rutrum nisl ac, lacinia quam. Ut placerat augue quis leo vestibulum tincidunt. Nulla volutpat lacus eget sagittis gravida. Aliquam vitae venenatis libero. Curabitur aliquam semper purus, et bibendum ex sollicitudin a.
Ut in dapibus dui, quis gravida lorem. Aenean maximus rhoncus erat ut volutpat. Nunc et nulla vel quam maximus pharetra ut vel est. Duis tristique aliquam mauris, a porta orci ultricies eu. Etiam at porta eros. Proin euismod nibh arcu, ac commodo metus viverra viverra. Aenean id lectus nec orci tempus luctus. Nulla facilisi. Nam eleifend malesuada sem ut pretium. Aenean molestie dictum magna non dapibus."""
    text = text.upper()

    print(text)
    r = vigenere(text,key)
    r2 = vigenere(r,key,False)

    print("\n\n\n",r,"\n\n\n",r2,"\n\n\n")

    print("\n\n\n")
    print(regroupement(text,4))

    print(regroupement ("Mes vieilles tantes", 3))



